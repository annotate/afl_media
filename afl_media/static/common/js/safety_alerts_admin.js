// ********* Delete Attachment ********* //

function delete_attachment(attachment_id){
    var attachment_container = $("#attachment-" + attachment_id);
    var trash_icon = attachment_container.find('.icon-trash');
    var spinner = attachment_container.find('.spinner');

    trash_icon.hide();
    spinner.show();

    data = {
        'attachment_id': attachment_id
    };

    Dajaxice.safety_alerts.delete_attachment(delete_attachment_callback, data);

    function delete_attachment_callback(data){
        if(data.success){
            attachment_container.remove();
        }
        else {
            spinner.hide();
            trash_icon.show();
            alert("Could not remove this attachment. You may not have permission to modify this post.");
        }
    }
}
