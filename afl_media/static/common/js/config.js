/* ================================
** Turnaround time
** ================================ */


$(document).ready(function(){
    var container = $("#turnaround-time");
    var time = parseInt(container.html(), 10);

    _turnaround_time_last_changed = null;

    setInterval(update_turnaround_time, 250);
});


function update_turnaround_time(){
    if (!_turnaround_time_last_changed){
        return;
    }

    var container = $("#turnaround-time");
    var time = parseInt(container.html(), 10);

    var current_time = new Date();

    var time_since_last_changed = current_time.getTime() - _turnaround_time_last_changed.getTime();

    if (time_since_last_changed > 700){
        var data = {
            'new_value': time
        };

        Dajaxice.scheme_admin.set_turnaround_time(update_turnaround_time_callback, data);

        _turnaround_time_last_changed = null;
    }
}


function update_turnaround_time_callback(data){
    return;
}


function increase_turnaround_time(){
    var container = $("#turnaround-time");
    var suffix = $("#turnaround-time-suffix");
    var time = parseInt(container.html(), 10);
    var new_time = time + 1;

    container.html(new_time);

    if (new_time == 1) {
        suffix.html("Day");
    }
    else {
        suffix.html("Days");
    }

    _turnaround_time_last_changed = new Date();
}


function reduce_turnaround_time(){
    var container = $("#turnaround-time");
    var suffix = $("#turnaround-time-suffix");
    var time = parseInt(container.html(), 10);
    var new_time = time - 1;

    if (time > 0){
        container.html(new_time);
    }

    if (new_time == 1) {
        suffix.html("Day");
    }
    else {
        suffix.html("Days");
    }

    _turnaround_time_last_changed = new Date();
}


/* ================================
** Out of Office
** ================================ */

$("#id_out_of_office").change(function(){
    var val = $(this).is(":checked");
    var return_date_container = $("#return-date-container");
    var return_date = $("#return-date");
    
    var data = {
        'new_value': val
    };

    Dajaxice.scheme_admin.set_out_of_office(set_out_of_office_callback, data);

    if (!val){
        return_date_container.hide();
        return_date.html("<em>Not yet specified</em>");
    }
    else {
        return_date_container.show();
    }
});


function set_out_of_office_callback(){
    return;
}



/* ================================
** Return date
** ================================ */

$(document).ready(function(){
    $("#return-date-activator").datepicker({'autoclose': true}).on('changeDate', set_return_date);
});

function set_return_date(ev){
    var date_container = $("#return-date");
    var date = ev.date;
    
    var data = {
        'new_value': date.valueOf()
    };

    Dajaxice.scheme_admin.set_return_date(set_return_date_callback, data);

    formatted_date = moment(date).format('dddd MMMM D, YYYY');
    date_container.html(formatted_date);
}

function set_return_date_callback(){
    return;
}
