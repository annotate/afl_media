function check_completion()
{
    if ($("#emission_three"))
    {
        var application_level = $("#application_level").val();
        var benchmark_selected = $("#application_benchmark").val();
        var myArray = ['application_year'];


//        alert(benchmark_selected);
//        alert(application_level);

        var error_message = 'The Reduction calculations need to be completed before you can continue.';

        if (application_level == 1)
        {
            var error_message = 'The year of application (Year 0) data needs to be completed.';

        }

        if (application_level == 2)
        {
            myArray.push('year_1');
            myArray.push('year_average');
            var error_message = 'Levels 2+ must demonstrate a reduction and must do so against either Year -1 footprint, the average of Year -1 and -2, or the average of year -1, -2 and -3';
        }

        if ((application_level == 3) || (application_level == 4))
        {
            myArray.push('year_1');
            myArray.push('year_average');
            myArray.push('year_2');
            myArray.push('year_3');

            error_message = 'All of the Reduction calculations and Scope 3 Emissions need to be completed before you can continue.';
        }


    //        alert(myArray);

        var completed = true;
        var $currentElem;

        $.each( myArray, function( intValue, currentElement ) {
            currentElem = myArray[intValue];

            if ($("#id_scope_absolute_footprint_"+ currentElem).val() == "")
            {
                completed = false;
            }

            if ($("#id_absolute_footprint_"+ currentElem).val() == "")
            {
                completed = false;
            }

            if (benchmark_selected.toString() == "relative_footprint")
            {
                if ($("#id_scope_relative_footprint_"+ currentElem).val() == "")
                {
                    completed = false;
                }

                if ($("#id_scope_relative_measure_"+ currentElem).val() == "")
                {
                    completed = false;
                }

                if ($("#id_relative_footprint_"+ currentElem).val() == "")
                {
                    completed = false;
                }

                if ($("#id_relative_measure_"+ currentElem).val() == "")
                {
                    completed = false;
                }
            }

        });

        if (!(completed))
        {
            alert(error_message);
            $('#submit').removeClass('disabled');
            return;
        }
    }

    $('#submit').html('<i class="icon-save"></i> Saving');
    $('#question-form').submit();
}

//Reduction Calculations... the most inefficient way possible. I will return to this..

function check_update_average(show_deviation, section)
{
    var benchmark_selected = $("#application_benchmark").val();
    var total_year_count = 0;
    var total_count = 0;

    var ab_year_three = $("#id_"+section+"absolute_footprint_year_3").val();
    if (ab_year_three)
    {
        total_year_count++;
        total_count += parseFloat(ab_year_three);
    }

    var ab_year_two = $("#id_"+section+"absolute_footprint_year_2").val();
    if (ab_year_two)
    {
        total_year_count++;
        total_count += parseFloat(ab_year_two);
    }

    var ab_year_one = $("#id_"+section+"absolute_footprint_year_1").val();
    if (ab_year_one)
    {
        total_year_count++;
        total_count += parseFloat(ab_year_one);
    }

    var ab_average = (parseFloat(total_count/ total_year_count));

    if (isNaN(ab_average))
    {
        ab_average = $("#id_absolute_footprint_year_average").val();
    }

    var ab_application_year = $("#id_"+section+"absolute_footprint_application_year").val();


    var reduction_not_achieved = $("#reduction_not_achieved");

    if (ab_average == "" || ab_application_year == "")
    {
        reduction_not_achieved.hide();
        $("#"+section+"absolute_footprint_average").text('');
        $("#"+section+"relative_footprint_average").text('');
    }

    else
    {
         if(!isNaN(ab_average) && parseFloat(ab_application_year)) {

//           console.log(ab_average);
//           console.log(ab_application_year);
            if (parseFloat(ab_application_year) < parseFloat(ab_average))
            {
                reduction_not_achieved.hide();
                $("#id_"+section+"absolute_footprint_year_average").val(ab_average);
                $("#id_"+section+"absolute_footprint_less_than_average").attr('checked',true);
                $("#id_"+section+"absolute_footprint_less_than_average").attr("value", "True");
                $("#"+section+"absolute_footprint_average").text('Yes');
            }
            else
            {
                $("#id_"+section+"absolute_footprint_year_average").val(ab_average);
                $("#id_"+section+"absolute_footprint_less_than_average").removeAttr('checked');
                $("#id_"+section+"absolute_footprint_less_than_average").attr("value", "False");
                $("#"+section+"absolute_footprint_average").text('No');
            }
        }

    }



    var rm_year_three = $("#id_"+section+"relative_measure_year_3").val();

    total_count = 0;
    if (rm_year_three)
    {
        total_count += parseFloat(rm_year_three);
    }

    var rm_year_two = $("#id_"+section+"relative_measure_year_2").val();
    if (rm_year_two)
    {
        total_count += parseFloat(rm_year_two);
    }

    var rm_year_one = $("#id_"+section+"relative_measure_year_1").val();
    if (rm_year_one)
    {
        total_count += parseFloat(rm_year_one);
    }

    var rm_average = (parseFloat(total_count)/ total_year_count);
    var rm_application_year = $("#id_"+section+"relative_measure_application_year").val();


    if(parseFloat(rm_application_year) && !isNaN(rm_average)) {

        $("#id_"+section+"relative_measure_year_average").val(rm_average);

        //*here*//

        if (rm_average && ab_average && parseFloat(ab_average/rm_average))
        {
            var application_footprint_average = parseFloat(ab_average/rm_average);



            $("#id_"+section+"relative_footprint_year_average").val(application_footprint_average);
        }


        if (ab_application_year && rm_application_year && parseFloat(parseFloat(ab_application_year)/parseFloat(rm_application_year)))
        {
            $("#id_"+section+"relative_footprint_application_year").val(parseFloat(ab_application_year/rm_application_year));
        }
    }


    var rf_year_three = $("#id_"+section+"relative_footprint_year_3").val();
    total_count = 0;
    if (rf_year_three)
    {
        total_count += parseFloat(rf_year_three);
    }

    var rf_year_two = $("#id_"+section+"relative_footprint_year_2").val();
    if (rf_year_two)
    {
        total_count += parseFloat(rf_year_two);
    }
    var rf_year_one = $("#id_"+section+"relative_footprint_year_1").val();
    if (rf_year_one)
    {
        total_count += parseFloat(rf_year_one);
    }

    var rf_average = $("#id_"+section+"relative_footprint_year_average").val();
    var rf_application_year = $("#id_"+section+"relative_footprint_application_year").val();



    if (rf_average && !isNaN(rf_average))
    {
        if (parseFloat(rf_average) && parseFloat(rf_application_year) && ((parseFloat(rf_application_year) < parseFloat(rf_average))))
        {
            $("#id_"+section+"relative_footprint_less_than_average").attr('checked', true);
            $("#id_"+section+"relative_footprint_less_than_average").attr("value", "True");
            $("#"+section+"relative_footprint_average").text('Yes');
        }
        else
        {
            $("#id_"+section+"relative_footprint_less_than_average").removeAttr('checked');
            $("#id_"+section+"relative_footprint_less_than_average").attr('value', 'False');
            $("#"+section+"relative_footprint_average").text('No');
        }
    }

    if (rf_application_year == "" || rf_average == "" || ab_average == "" || ab_application_year == "")
    {
        $("#"+section+"relative_footprint_average").text('');
    }



    if (benchmark_selected.toString() == "relative_footprint")
    {
        if ((rf_average && rf_application_year) && (ab_average && ab_application_year))
        {
            if ((parseFloat(rf_average) < parseFloat(rf_application_year)) && (section != "scope_"))
            {
                reduction_not_achieved.show();

            }
            else
            {

               if (show_deviation && section != "scope_")
               {
                   reduction_not_achieved.hide();
                   alert('Congratulations! You have achieved a reduction in your Scope 1 & 2 CO2 emissions.');
               }

            }
        }
    }
    else
    {
        if (ab_average && !isNaN(ab_average) && ab_application_year && !isNaN(ab_application_year))
        {
            if ((parseFloat(ab_average) < parseFloat(ab_application_year)) && (section != "scope_"))
            {
                reduction_not_achieved.show();

            }
            else
            {


               if (show_deviation && section != "scope_")
               {
                   reduction_not_achieved.hide();
                   alert('Congratulations! You have achieved a reduction in your Scope 1 & 2 CO2 emissions.');
               }

            }
        }
    }

}


    $(".absolute_footprint_data").change(function() {
        check_update_average(false, "");
    });

    $(".relative_measure_data").change(function() {
        check_update_average(false, "");
    });


    $(".scope_relative_measure_data").change(function() {
        check_update_average(false, 'scope_');
    });


    $(".scope_absolute_footprint_data").change(function() {
        check_update_average(false, 'scope_');
    });


    $("#id_relative_measure_application_year").change(function() {
        check_update_average(true, "");
    });

    $("#id_absolute_footprint_application_year").change(function() {
        check_update_average(true, "");
    });

    $("#id_application_for_limited_deviation").change(function() {
       var value = $("#id_application_for_limited_deviation").val();
       if (value == 'True')
       {
           $("#deviation_yes_selected").show();
           $("#deviation_no_selected").hide();
       }

       if (value == 'False')
       {
           $("#deviation_no_selected").show();
           $("#deviation_yes_selected").hide();
       }

       if (value == 'None')
       {
           $("#deviation_no_selected").hide();
           $("#deviation_yes_selected").hide();
       }
    });






//        if ((rm_year_three && ab_year_three) && parseFloat(ab_year_three/rm_year_three))
//        {
//            $("#id_"+section+"relative_footprint_year_3").val(parseFloat(ab_year_three/rm_year_three));
//        }
//
//        if (rm_year_two && ab_year_two && parseFloat(ab_year_two/rm_year_two))
//        {
//            $("#id_"+section+"relative_footprint_year_2").val(parseFloat(ab_year_two/rm_year_two));
//        }
//
//        if (rm_year_one && ab_year_one && parseFloat(ab_year_one/rm_year_one))
//        {
//            $("#id_"+section+"relative_footprint_year_1").val(parseFloat(ab_year_one/rm_year_one));
//        }