function update_pseudo_checkboxes(){
    $(".pseudo-checkbox").each(function(){
        var actual_checkbox = $(this).parent().find('input');

        if (actual_checkbox.is(':checked')){
            $(this).addClass('checked');
        } else {
            $(this).removeClass('checked');
        }
    });
}

$(document).ready(update_pseudo_checkboxes);

$(".btn-checkbox").click(function(){
    var checkbox = $(this).find('input');

    if (checkbox.is(':checked')){
        checkbox.attr('checked', false);
    } else {
        checkbox.attr('checked', true);
    }

    checkbox.change();

    update_pseudo_checkboxes();
});

$("input[name='periods']").change(function(){
    var form_data = $("#report-filter-form").serialize();
    var updating_notification = $("#updating-notification");

    var data = {
        'form_data': form_data
    };

    updating_notification.show();

    Dajaxice.scheme_admin.get_kpi_report_data(get_kpi_report_data_callback, data);
});

function get_kpi_report_data_callback(data){
    var updating_notification = $("#updating-notification");

    updating_notification.hide();

    rows = eval("(" + data['rows'] + ")");
    draw_chartid_kpi_radar(rows, data['max_score']);
}

