window.onbeforeunload = function() {
    return "Dude, are you sure you want to leave? Think of the kittens!";
};


function get_folder_children(directory_id){

    var data = {
        'directory_id': directory_id
    };

    Dajaxice.catalogue_display.get_folder_children(get_folder_children_callback, data);

    function get_folder_children_callback(data){
        if (data.success){

            if (data.html)
            {
                var menu = $("#children-node-"+directory_id);

                if (!menu.html())
                {
                    menu.html('<div></div>');
                    $("#display-node-files").html('<div></div>');
                    var directory_name = data.directory_name;
                    $("#folder-name").html(directory_name);

                    $('#select-folder-options option:contains("add_folder")').text('Add Folder '+ d.directory_name+' to Bucket');
                    $('#select-folder-options option:contains("add_avid_folder")').text('Add Folder '+ d.directory_name+' as Avid Project to Bucket');
                    $('#select-folder-options option:contains("mark_complete")').text('Mark Folder '+ d.directory_name+' Complete');

                    menu.append(data.html);

                    $("#display-node-files").append(data.directory_html);
                }

                $("#children-node-"+directory_id).effect('highlight', {}, 100);

            }


        }
    }
}

