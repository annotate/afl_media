(function($){
    $.fn.serializeJSON=function() {
    var json = {};
    jQuery.map($(this).serializeArray(), function(n, i){
        json[n.name] = n.value;
    });

    return json;
};
})(jQuery);


function ef_init(object_id, update_callback) {
    window._ef_object_id = object_id;
    window._ef_update_callback = update_callback;
}


function ef_trigger(field) {
    var value_container = $("#ef-" + field + "-value");
    var form_container = $("#ef-" + field + "-form");
    var input = form_container.find(':input').filter(':visible:first');

    if (form_container.is(':visible')) {
        value_container.show();
        form_container.hide();
    }
    else {
        value_container.hide();
        form_container.show();
        input.focus();
    }
}


function ef_cancel(field) {
    var value_container = $("#ef-" + field + "-value");
    var form_container = $("#ef-" + field + "-form");

    value_container.show();
    form_container.hide();
}


function ef_save(field) {
    var value_container = $("#ef-" + field + "-value");
    var form_container = $("#ef-" + field + "-form");
    var form = form_container.find('form');
    var form_data = form.serializeJSON();
    var button = $("#ef-" + field + "-submit");

    button.button('loading');

    var data = {
        'object_id': window._ef_object_id,
        'field': field,
        'form_data': form_data
    };

    window._ef_update_callback(ef_save_callback, data);
}


function ef_save_callback(data) {
    var field = data.field;
    var value_container = $("#ef-" + field + "-value");
    var form_container = $("#ef-" + field + "-form");
    var form = form_container.find('form');
    var button = $("#ef-" + field + "-submit");

    button.button('reset');

    if (!data.success) {
        alert("Edit failed");
        $(window).trigger('ef:failure:' + field);
        return;
    }

    form_container.hide();
    value_container.show();

    console.log(field);
    console.log(data.updated_data);
    console.log(data.updated_data[field]);
    console.log("GOT HERE");

    value_container.html(data.updated_data[field]);
    value_container.effect('highlight', {}, 3000);

    $(window).trigger('ef:success:' + field, data.updated_data);
}
