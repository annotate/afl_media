function show_diff(log_entry_id) {
    var diff = $("#log-entry-diff-" + log_entry_id);
    var show_link = $("#log-entry-diff-" + log_entry_id + "-show");
    var hide_link = $("#log-entry-diff-" + log_entry_id + "-hide");

    show_link.hide();
    hide_link.show();
    diff.slideDown();
}

function hide_diff(log_entry_id) {
    var diff = $("#log-entry-diff-" + log_entry_id);
    var show_link = $("#log-entry-diff-" + log_entry_id + "-show");
    var hide_link = $("#log-entry-diff-" + log_entry_id + "-hide");

    hide_link.hide();
    show_link.show();
    diff.slideUp();
}