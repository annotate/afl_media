start_time = new Date();
maximum_comment_age = get_maximum_comment_age();


// ======================
// Timeouts
// ======================

setInterval(function(){
    // Periodically look at the time and remove the delete button from any comment
    // that has passed the deleteable time period
    var current_time = new Date();
    var page_age = (current_time - start_time) / 1000;

    $(".comment").each(function(index, comment){
        comment = $(comment);
        var delete_button = comment.find(".comment-delete-button");
        var comment_age = comment.data('age') + page_age;
        var always_deletable = comment.data('always-deletable');

        if (delete_button && delete_button.is(':visible') && comment_age > maximum_comment_age && !always_deletable) {
            delete_button.fadeOut();
        }
    });
}, 1000);


function get_maximum_comment_age(){
    // TODO: Make this dynamic
    return 15*60;
}


// ======================
// Adding comments
// ======================

function comment_init(object_id, callback){
    window._comment_object_id = object_id;
    window._comment_callback = callback;
}

function add_comment() {
    var comment = $("#comment-form").find('textarea').val();
    var button = $("#add-comment-button");

    if (!comment){
        return;
    }

    button.button('loading');

    data = {
        'object_id': window._comment_object_id,
        'comment': comment
    };

    window._comment_callback(add_comment_callback, data);
}

function add_comment_callback(data) {
    var comment_input = $("#comment-form").find('textarea');
    var button = $("#add-comment-button");
    var comments = $("#comments");
    button.button('reset');

    if (!data.success){
        alert("Your comment could not be entered. Please try again later.");
        return;
    }

    comment_input.val('');
    comments.append(data.html);

    $(".comment .comment-content").last().effect('highlight', {}, 3000);
}


// ======================
// Deleting comments
// ======================

function delete_comment(comment_id) {
    var comment = $("#comment-" + comment_id);
    var button = comment.find('.comment-delete-button');

    var data = {
        'comment_id': comment_id
    };

    if (confirm("Are you sure you want to delete this comment?")){
        button.button('loading');
        Dajaxice.comments.delete_comment(delete_comment_callback, data);
    }
}

function delete_comment_callback(data) {
    if (data.success) {
        var comment = $("#comment-" + data.deleted_comment_id);
        comment.hide();
    }

    else {
        alert("Delete failed");
    }
}
