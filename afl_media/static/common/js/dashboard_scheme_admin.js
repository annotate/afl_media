function stop_propagation(event){
    event.stopPropagation();
}

$(document).ready(function(){
    $("#start-date-activator").datepicker({'autoclose': true}).on('changeDate', set_start_date);
    $("#end-date-activator").datepicker({'autoclose': true}).on('changeDate', set_end_date);
});

function set_start_date(ev){
    var form_field = $("#id_filter-start_date");
    var date = ev.date;
    var date_str = $.datepicker.formatDate('dd/mm/yy', date);
    
    $("#start-date-activator").datepicker('hide');
    form_field.val(date_str);
    form_field.change();

    return;
}

function set_end_date(ev){
    var form_field = $("#id_filter-end_date");
    var date = ev.date;
    var date_str = $.datepicker.formatDate('dd/mm/yy', date);
    
    $("#end-date-activator").datepicker('hide');
    form_field.val(date_str);
    form_field.change();

    return;
}

$(".timeperiod-link").click(function(){
    var timeperiod_input = $("#timeperiod-input");
    var timeperiod_text = $(this).html();
    var num_years = $(this).data('numyears');

    $(".timeperiod-label").html(timeperiod_text);

    if(num_years >= 1 && num_years <= 3){
        timeperiod_input.val(num_years);
        timeperiod_input.change();
    }
});


function update_pseudo_checkboxes(){
    $(".pseudo-checkbox").each(function(){
        var actual_checkbox = $(this).parent().find('input');

        if (actual_checkbox.is(':checked')){
            $(this).addClass('checked');
        } else {
            $(this).removeClass('checked');
        }
    });
}

$(document).ready(update_pseudo_checkboxes);


$(".btn-checkbox").click(function(){
    var checkbox = $(this).find('input');

    if (checkbox.is(':checked')){
        checkbox.attr('checked', false);
    } else {
        checkbox.attr('checked', true);
    }

    checkbox.change();

    update_pseudo_checkboxes();
});


$("#sectors-filter a").click(function(event){
    var checkbox = $(this).find('input');
    event.stopPropagation();
    $(this).blur();

    if (checkbox.is(':checked')){
        checkbox.attr('checked', false);
    } else {
        checkbox.attr('checked', true);
    }

    checkbox.change();

    update_pseudo_checkboxes();

    update_sector_dropdown_text();
});


function update_sector_dropdown_text(){
    var sector_dropdown_text = $("#sector-dropdown-text");
    var new_text = construct_sector_dropdown_text();

    sector_dropdown_text.html(new_text);
}


function construct_sector_dropdown_text(){
    var wind_large = $("#wind-large-checkbox").is(":checked");
    var wind_medium = $("#wind-medium-checkbox").is(":checked");
    var wind_small = $("#wind-small-checkbox").is(":checked");
    var wave = $("#wave-checkbox").is(":checked");
    var tidal = $("#tidal-checkbox").is(":checked");

    var strings = [];

    if(wind_large && wind_medium && wind_small && wave && tidal){
        return "All Sectors";
    }

    if(!wind_large && !wind_medium && !wind_small && !wave && !tidal){
        return "No sectors";
    }

    if(wind_large || wind_medium || wind_small){
        var wind_strings = [];

        if (wind_large){
            wind_strings.push('l');
        }

        if (wind_medium){
            wind_strings.push("m");
        }

        if (wind_small){
            wind_strings.push("s");
        }

        strings.push("Wind (" + wind_strings.join() + ")");
    }

    if (wave){
        strings.push("Wave");
    }

    if (tidal){
        strings.push("Tidal");
    }

    return strings.join(" / ");
}


function update_charts(){
    $("#updating-notification").show();

    var num_years = $("#timeperiod-input").val();
    var wind_large = $("#wind-large-checkbox").is(":checked");
    var wind_medium = $("#wind-medium-checkbox").is(":checked");
    var wind_small = $("#wind-small-checkbox").is(":checked");
    var wave = $("#wave-checkbox").is(":checked");
    var tidal = $("#tidal-checkbox").is(":checked");
    var sectors_filter = $("#sectors-filter");
    var sectors = [];

    if (wind_large){ sectors.push("wind_large"); }
    if (wind_medium){ sectors.push("wind_medium"); }
    if (wind_small){ sectors.push("wind_small"); }
    if (wave){ sectors.push("wave"); }
    if (tidal){ sectors.push("tidal"); }

    var payload = {
        'num_years': num_years,
        'sectors': sectors
    };

    Dajaxice.scheme_admin.get_summary_report_data(update_charts_callback, payload);
}


$("#filters input").change(update_charts);


function update_charts_callback(data){
    var num_years = $("#timeperiod-input").val();
    var show_every = num_years * 2;

    $("#updating-notification").hide();

    if (data.success){
        window.chart_chartid_summary_incidents_over_time_options.hAxis.showTextEvery = show_every;

        setData_chartid_summary_incidents_over_time(data.incidents_over_time);
        setData_chartid_summary_incidents_by_activity_type(data.incidents_by_activity_type);
        setData_chartid_summary_incidents_by_hazard_type(data.incidents_by_hazard_type);
        setData_chartid_summary_incidents_by_operational_phase(data.incidents_by_operational_phase);
        setData_chartid_summary_incidents_by_actual_impact(data.incidents_by_actual_impact);
    }
}


function generate_summary_report_pdf(){
    var spinner = $("#export-modal-spinner");
    var link_container = $("#export-modal-download-link-container");
    var link = $("#export-modal-download-link");
    var error_message = $("#export-modal-error");

    spinner.show();
    link_container.hide();
    error_message.hide();
    
    $("#export-modal").modal();

    var num_years = $("#timeperiod-input").val();
    var wind_large = $("#wind-large-checkbox").is(":checked");
    var wind_medium = $("#wind-medium-checkbox").is(":checked");
    var wind_small = $("#wind-small-checkbox").is(":checked");
    var wave = $("#wave-checkbox").is(":checked");
    var tidal = $("#tidal-checkbox").is(":checked");
    var sectors_filter = $("#sectors-filter");
    var sectors = [];

    if (wind_large){ sectors.push("wind_large"); }
    if (wind_medium){ sectors.push("wind_medium"); }
    if (wind_small){ sectors.push("wind_small"); }
    if (wave){ sectors.push("wave"); }
    if (tidal){ sectors.push("tidal"); }

    var payload = {
        'num_years': num_years,
        'sectors': sectors
    };

    Dajaxice.scheme_admin.generate_summary_report_pdf(generate_summary_report_pdf_callback, payload);
}


function generate_summary_report_pdf_callback(data){
    var spinner = $("#export-modal-spinner");
    var link_container = $("#export-modal-download-link-container");
    var link = $("#export-modal-download-link");
    var error_message = $("#export-modal-error");

    spinner.hide();

    if (data.success){
        link.attr("href", data.download_url);
        link_container.show();
    } else {
        error_message.show();
    }
}
