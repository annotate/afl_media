// ********* Global ********* //

$('a[data-toggle="tab"]').on('shown', function (e) {
    var target_href = $(e.target).attr('href');
    var sidebar_summary = $('#incident-sidebar-summary');

    if(target_href == '#submission'){
        sidebar_summary.hide();
    } else {
        sidebar_summary.show();
    }
});

function save_incident(callback){
    var form_data = $("#incident-form").serialize();
    var saving_notification = $("#saving-notification");

    var data = {
        'incident_id': $("#id_saved_incident").val(),
        'form_data': form_data
    };

    saving_notification.show();

    Dajaxice.incidents.save_incident(function(returned_data){
        save_incident_callback(returned_data, callback);
    }, data);
}

function save_incident_callback(data, callback){
    var form = $("#incident-form");
    var control_groups = form.find('.control-group');
    var error_lists = form.find('.error-list');
    var sidebar_summary = $("#incident-sidebar-summary");
    var summary = $("#incident-summary");
    var error_html = "";
    var saving_notification = $("#saving-notification");
    var saved_notification = $("#saved-notification");
    var fail_notification = $("#fail-notification");
    var pdf_export_link = $("#pdf-export-link");

    control_groups.removeClass('error');
    error_lists.hide();
    error_lists.html("");

    saving_notification.hide();

    if (!data.success){
        fail_notification.show();
        setTimeout(function(){fail_notification.hide("fade", {}, 2000);}, 4000);

        if (data.errors){
            $.each(data.errors, function(index, errors){
                var control_group = $("#"+ index).closest('.control-group');
                var controls = control_group.find('.controls');
                var error_list_container = control_group;

                if(controls.length > 0){
                    error_list_container = controls;
                }

                var error_list = error_list_container.find('.error-list');

                if(error_list.length === 0){
                    error_list_container.append("<ul class='error-list'></ul>");
                    error_list = error_list_container.find('.error-list');
                }

                control_group.addClass('error');
                
                error_list_html = "";
                $.each(errors, function(error_index, error){
                    error_list_html = error_list_html + "<span class='help-inline'>" + error + '</span>';
                });

                error_list.html(error_list_html);
            });

            error_lists.show();
        }

        var scroll_target = $('.control-group.error').parents('.well');

        $(window)._scrollable().scrollTo(scroll_target, {'duration': '100', 'offset': -300});
        form.find('.error-list').effect('highlight', 2500);
        form.find('.error-list').find('.help-inline').effect('pulsate', {'times': 5}, 500);

        callback(data);
        return;
    }
    else {
        saved_notification.show();
        setTimeout(function(){saved_notification.hide("fade", {}, 2000);}, 4000);

        pdf_export_link.attr('href', data.pdf_download_url);
        pdf_export_link.attr('disabled', false);
    }

    sidebar_summary.html(data.sidebar_summary_html);
    summary.html(data.summary_html);

    if(data.details_is_complete){
        $("#button_submit").removeAttr('disabled');
    }
    else {
        $("#button_submit").prop('disabled', true);
    }

    $("#incident_id").val(data.incident_id);
    $("#id_saved_incident").val(data.incident_id);
    $("#id_pending_media").val('');

    $(".lodge-incident-header").html("Incident #" + data.incident_id);
    
    callback(data);
}


// ********* Base Save ********* //

function save_changes(){
    save_incident(save_changes_callback);
}

function save_changes_callback(){
    return;
}


// ********* Incident Details ********* //

function save_incident_details(){
    save_incident(save_incident_details_callback);
}

function save_incident_details_callback(data){
    if (data.success){
        $('#event-toggle').tab('show');
        $(window)._scrollable().scrollTo(0, 0);
    }
}


// ********* Incident Event ********* //

function save_incident_event(){
    save_incident(save_incident_event_callback);
}

function save_incident_event_callback(data){
    if (data.success){
        $('#cause-toggle').tab('show');
        $(window)._scrollable().scrollTo(0, 0);
    }
}


// ********* Incident Cause ********* //

function save_incident_cause(){
    save_incident(save_incident_cause_callback);
}

function save_incident_cause_callback(data){
    if (data.success){
        $('#learning-toggle').tab('show');
    }
}


// ********* Incident Learning and Improvement ********* //

function save_incident_learning_and_improvement(){
    save_incident(save_incident_learning_and_improvement_callback);
}

function save_incident_learning_and_improvement_callback(data){
    if (data.success){
        $('#submission-toggle').tab('show');
    }
}


// ********* Delete Attachment ********* //

function delete_attachment(attachment_id){
    var attachment_container = $("#incident-attachment-" + attachment_id);
    var trash_icon = attachment_container.find('.icon-trash');
    var spinner = attachment_container.find('.spinner');

    trash_icon.hide();
    spinner.show();

    data = {
        'attachment_id': attachment_id
    };

    Dajaxice.incidents.delete_attachment(delete_attachment_callback, data);

    function delete_attachment_callback(data){
        if(data.success){
            attachment_container.remove();
        }
        else {
            spinner.hide();
            trash_icon.show();
            alert("Could not remove this attachment. You may not have permission to modify this incident.");
        }
    }
}


// ********* Notificable Question Update ********* //

$('.actual_impact').change(function() {
    var id = $(this).attr("id");
    var impact_text = $('label[for='+id+']').text();

    if (impact_text.indexOf("notifiable") >= 0)
    {
        $("#id_notificable_0").attr('checked', 'checked');
    }
    else
    {
        $("#id_notificable_1").attr('checked', 'checked');
    }
});


// ********* "Other" text area field displays ********* //

$('.safety_implementation_shortfall_radio').change(function() {
    var id = $(this).attr("id");
    var impact_text = $('label[for='+id+']').text();

    if (impact_text.indexOf("Other") >= 0)
    {
        $("#safety_implementation_shortfall_other").removeAttr('style');
    }
    else
    {
        $("#safety_implementation_shortfall_other").attr('style', 'display: none;');
    }
});

$('.safety_organisation_shortfall_radio').change(function() {
    var id = $(this).attr("id");
    var impact_text = $('label[for='+id+']').text();

    if (impact_text.indexOf("Other") >= 0)
    {
        $("#safety_organisation_shortfall_other").removeAttr('style');
    }
    else
    {
        $("#safety_organisation_shortfall_other").attr('style', 'display: none;');
    }
});
