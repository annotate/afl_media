$("#select-folder-options").change(function() {
    if ($(this).val() == "add_folder")
    {
        add_folder_to_bucket();
    }

    if ($(this).val() == "add_avid_folder")
    {
        add_project_to_bucket();
    }

    if ($(this).val() == "mark_complete")
    {
        mark_folder_complete();
    }

    $(this).prop('selectedIndex', 0);
});

function mark_folder_complete() {
    if (confirm("Are you sure you wish to mark this folder and it's included folders done? "))
    {
        var folder_id = $("#selected-folder").val();
        window.location.href="/catalogue/completed/folder/"+folder_id+"/";
    }
    return false;
}

$("#check_all").click(function() {
    $(".check-magic-file").each(function() {
       if (! $(this).prop('checked'))
       {
          $(this).click();
       }
    });
    $("#check_all_hide").show();
    $("#check_all").hide();
});

$("#check_all_hide").click(function() {
    $(".check-magic-file").each(function() {
       if ( $(this).prop('checked'))
       {
          $(this).click();
       }
    });
    $("#check_all_hide").hide();
    $("#check_all").show();
});



$("#create-file").click(function() {
    $("#create-file").hide();
    $("#folder-status").hide();
    $("#file-button-group").hide();
    $("#folder-name").html('<img src="/static/common/img/loading.gif" style="height: 30px; width: 30px;">');
    $("#create-files").submit();
});

function add_folder_to_bucket() {
    $.post('/add_folder_to_bucket/',
        $("#create-files").serialize() ).done(function( data ) {
        if (data.success == true)
        {
            $("#bucket-container").html(data.bucket_html);
            alert('Folder and all contents added successfully.');
            $("#bucket-container").show();
        }
    });
}

function add_project_to_bucket() {
    $.post('/add_project_to_bucket/',
        $("#create-files").serialize() ).done(function( data ) {
        if (data.success == true)
        {
            $("#bucket-container").html(data.bucket_html);
            alert('Avid Project added successfully.');
            $("#bucket-container").show();
        }
    });
}

function add_files_to_bucket() {
    $.post('/add_files_to_bucket/',
        $("#create-files").serialize() ).done(function( data ) {
        if (data.success == true)
        {
            $("#bucket-container").html(data.bucket_html);
            alert('Files added successfully.');
            $("#bucket-container").show();
        }
    });
}

function remove_folder_from_bucket(folder_id) {
    $("#folder_"+folder_id).effect( "highlight" ).delay(2000);
    $.get('/remove_folder_from_bucket/' + folder_id + '/', function (data) {
        $("#bucket-container").html(data.bucket_html);
    });
}

function remove_project_from_bucket(folder_id) {
    $("#folder_"+folder_id).effect( "highlight" ).delay(2000);
    $.get('/remove_project_from_bucket/' + folder_id + '/', function (data) {
        $("#bucket-container").html(data.bucket_html);
    });
}

function remove_file_from_bucket(file_id) {
    $("#file_"+file_id).effect( "highlight").delay(2000);
    $.get('/remove_file_from_bucket/' + file_id + '/', function (data) {
        $("#bucket-container").html(data.bucket_html);
    });
}


function clear_bucket() {
    if (confirm("Are you sure you wish to remove all folders and files?"))
    {
        $.get('/remove_all_from_bucket/', function (data) {
            $("#bucket-container").html(data.bucket_html);
        });
    }

}

$("#create-dmp-file").click(function() {
    $("#loading-gif-container").html("<img src='/static/common/img/loading.gif' style='width:30px; height: 30px;'>");
    $("#create-file-container").hide();
});
