/* jQuery version of Autogrow script from
Mobile HTML5 Boilerplate - http://bit.ly/l4j1RZ
Adapted by Iraê http://bit.ly/kBfnaD */

$(document).delegate('textarea', 'keyup', set_textarea_height);
$(document).delegate('textarea', 'focus', set_textarea_height);

$('textarea').each(function(index, textarea){
	$(textarea).css('overflow', 'hidden');
});

function set_textarea_height(event){
	var self = $(this),
	textLineHeight = self.data('textLineHeight'),
	currentHeight = self.data('currentHeight'),
	newHeight = this.scrollHeight;

	if(!textLineHeight) { // init this particular textarea
		textLineHeight = parseInt(self.css('line-height'),10);
		currentHeight = self.height();
		self.css('overflow','hidden');
	}

	if (newHeight > currentHeight) {
		newHeight = newHeight + 3 * textLineHeight;
		self.height(newHeight);
		self.data('currentHeight',newHeight);
	}
}