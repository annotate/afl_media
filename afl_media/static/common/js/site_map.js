function generate_site_map(map_id, width, height, scale, x, y, site_locations){
    var centered;

    var projection = d3.geo.mercator()
        .scale(scale)
        .translate([x, y]);

    var path = d3.geo.path()
        .projection(projection);

    var svg = d3.select(map_id).append("svg")
        .attr("width", width)
        .attr("height", height);

    svg.append("rect")
        .attr("class", "background")
        .attr("width", width)
        .attr("height", height)
        .on("click", click_path);

    var regions = svg.append("g")
        .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")")
        .append("g")
        .attr("id", "regions");

    var offshore_wind_leases = svg.append("g")
        .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")")
        .append("g")
        .attr("id", "offshore_wind_leases");

    var offshore_wave_leases = svg.append("g")
        .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")")
        .append("g")
        .attr("id", "offshore_wave_leases");

    var site_markers = svg.append("g")
        .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")")
        .append("g")
        .attr("id", "site_markers");

    $.each(site_locations, function(index, location){
        var latitude = location['latitude'];
        var longitude = location['longitude'];
        var id = "site" + location['id'];
        var location_xy = projection([longitude, latitude]);

        site_markers.append("svg:circle")
            .attr("cx", location_xy[0])
            .attr("cy", location_xy[1])
            .attr("r", 5)
            .attr("id", id)
            .on("click", click_site_marker);
        
    });

    d3.json(window.static_url + "common/geo/uk_regions.json", function(json) {
      regions.selectAll("path")
          .data(json.features)
          .enter().append("path")
          .attr("d", path)
          .on("click", click_region);
    });

    d3.json(window.static_url + "common/geo/uk_offshore_wind.json", function(json) {
      offshore_wind_leases.selectAll("path")
          .data(json.features)
          .enter().append("path")
          .attr("d", path)
          .on("click", click_offshore_wind_lease);
    });

    d3.json(window.static_url + "common/geo/uk_wave.json", function(json) {
      offshore_wave_leases.selectAll("path")
          .data(json.features)
          .enter().append("path")
          .attr("d", path)
          .on("click", click_offshore_wave_lease);
    });

    function click_region(d){
        return click_path(d, regions);
    }

    function click_offshore_wind_lease(d){
        return click_path(d, offshore_wind_leases);
    }

    function click_offshore_wave_lease(d){
        return click_path(d, offshore_wave_leases);
    }

    function click_path(d, layer) {
        if (!layer){
            layer = regions;
        }

        var x = 0,
          y = 0,
          k = 1;

        if (d && centered !== d) {
            var centroid = path.centroid(d);
            x = -centroid[0];
            y = -centroid[1];
            k = 4;
            centered = d;
        } else {
            centered = null;
        }

        layer.selectAll("path")
            .classed("active", centered && function(d) { return d === centered; });

        rescale(x, y, k);
    }

    function click_site_marker() {
        marker = d3.select(this);
        var site_id = '__site_marker' + this.id;

        var x = 0,
          y = 0,
          k = 1;

        if (centered !== site_id) {
            x = -marker.attr('cx');
            y = -marker.attr('cy');

            k = 4;
            centered = site_id;
        } else {
            centered = null;
        }

        var all_markers = site_markers.selectAll("circle");

        all_markers.each(function(){
            var marker = d3.select(this);
            var marker_id = '__site_marker' + this.id;
            
            if (marker_id === centered){
                marker.attr('fill-opacity', 1);
            }
            else {
                marker.attr('fill-opacity', 0.3);
            }
        });

        rescale(x, y, k);
    }

    function rescale(x, y, k){
        regions.transition()
            .duration(1000)
            .attr("transform", "scale(" + k + ")translate(" + x + "," + y + ")")
            .style("stroke-width", 1.5 / k + "px");

        offshore_wind_leases.transition()
            .duration(1000)
            .attr("transform", "scale(" + k + ")translate(" + x + "," + y + ")")
            .style("stroke-width", 0.5 / k + "px");

        offshore_wave_leases.transition()
            .duration(1000)
            .attr("transform", "scale(" + k + ")translate(" + x + "," + y + ")")
            .style("stroke-width", 0.5 / k + "px");

        site_markers.transition()
            .duration(1000)
            .attr("transform", "scale(" + k + ")translate(" + x + "," + y + ")")
            .style("stroke-width", 0.5 / k + "px");

        site_markers.selectAll("circle").transition()
            .duration(1000)
            .attr("r", 5.0 / k);
    }

}


function generate_legacy_site_map(selector){
    $(selector).html("Map view is not currently supported on your browser. Please upgrade to a current version of Internet Explorer 9, Firefox, Chrome or Safari.");
}
