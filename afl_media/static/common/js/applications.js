// Save Accreditation Level

function new_save_accreditation_level(callback)
{
    alert('ol one');
    var form_data = $("#applications-form").serialize();

    var data = {
        'application_id': $("#id_saved_application").val(),
        'form_data': form_data
    };

    Dajaxice.applications.save_application(function(returned_data){
        save_accreditation_level_callback(returned_data, callback);
    }, data);

}

function check_completed(data)
{
    if (data.is_airport_user_completed)
    {
        $("#submit_button_completed").show();
        $("#text_not_completed").hide();
    }
    else
    {
        $("#submit_button_completed").hide();
        $("#text_not_completed").show();
    }
}

function save_accreditation_level_callback(data, callback){

    if (data.success){
        $('#organisational-boundary-toggle').tab('show');
        $("#operational-boundary-toggle").show();
        $("#absolute-footprint-toggle").show();
        $("#carbon-footprint-toggle").show();
        $("#footprint-offsetting-toggle").show();
        $("#footprint-summary-toggle").show();

        $("#verification-toggle").show();
        $("#carbon-management-toggle").show();
        $("#carbon-summary-toggle").show();
        $('#governance-toggle').show();
        $("#carbon-stakeholder-toggle").show();
        $('html,body').scrollTop(0);
    }

    check_completed(data);

}

function save_accreditation_level_view(){

    $('#organisational-boundary-toggle').tab('show');
    $("#operational-boundary-toggle").show();
    $("#absolute-footprint-toggle").show();
    $("#carbon-footprint-toggle").show();
    $("#footprint-offsetting-toggle").show();
    $("#footprint-summary-toggle").show();

    $("#verification-toggle").show();
    $("#carbon-management-toggle").show();
    $("#carbon-summary-toggle").show();
    $('#governance-toggle').show();
    $("#carbon-stakeholder-toggle").show();
    $('html,body').scrollTop(0);
}


function save_account_details()
{
    $('#carbon-reduction-toggle').tab('show');
    $('html,body').scrollTop(0);
}

function save_carbon_reduction(callback)
{
    update_levels();

    var form_data = $("#applications-form").serialize();

    var data = {
        'application_id': $("#id_saved_application").val(),
        'form_data': form_data
    };

    Dajaxice.applications.save_application(function(returned_data){
        save_carbon_reduction_callback(returned_data, callback);
    }, data);


}

function save_carbon_reduction_callback(data, callback)
{
    if (data.success){

        var level = $("#id_accreditation_level").val();
        if (level == "level_1")
        {
            $('#submit-application-toggle').tab('show');
        }
        else
        {
            $('#governance-toggle').tab('show');
        }

        $('html,body').scrollTop(0);
    }

    check_completed(data);
}

function save_carbon_reduction_view()
{

    var level = $("#id_accreditation_level").val();
    if (level == "level_1")
    {
        $('#submit-application-toggle').tab('show');
    }
    else
    {
        $('#governance-toggle').tab('show');
    }

    $('html,body').scrollTop(0);

    update_levels();
}

function save_governance(callback)
{
    update_levels();

    var form_data = $("#applications-form").serialize();

    var data = {
        'application_id': $("#id_saved_application").val(),
        'form_data': form_data
    };

    Dajaxice.applications.save_application(function(returned_data){
        save_governance_callback(returned_data, callback);
    }, data);

}

function save_governance_callback(data, callback)
{
    if (data.success){
        $('#verification-toggle').tab('show');
        $('html,body').scrollTop(0);
    }
    check_completed(data);
}

function save_governance_view()
{

    $('#verification-toggle').tab('show');
    $('html,body').scrollTop(0);

}

function save_carbon_verification(callback)
{
    update_levels();

    var form_data = $("#applications-form").serialize();

    var data = {
        'application_id': $("#id_saved_application").val(),
        'form_data': form_data
    };

    Dajaxice.applications.save_application(function(returned_data){
        save_carbon_verification_callback(returned_data, callback);
    }, data);

}

function save_carbon_verification_callback(data, callback)
{
    if (data.success){
        $('#carbon-management-toggle').tab('show');
        $('html,body').scrollTop(0);
    }
    check_completed(data);
}

function save_carbon_verification_view()
{

    $('#carbon-management-toggle').tab('show');
    $('html,body').scrollTop(0);

}


function save_carbon_management(callback)
{
    update_levels();

    var form_data = $("#applications-form").serialize();

    var data = {
        'application_id': $("#id_saved_application").val(),
        'form_data': form_data
    };

    Dajaxice.applications.save_application(function(returned_data){
        save_carbon_management_callback(returned_data, callback);
    }, data);
}

function save_carbon_management_callback(data, callback)
{
    if (data.success){
        var level = $("#id_accreditation_level").val();
        if (level == "level_1" || level == "level_2")
        {
            $('#submit-application-toggle').tab('show');
        }
        else if (level == "level_3" || level == "level_3_plus")
        {
            $('#carbon-stakeholder-toggle').tab('show');
        }

        $('html,body').scrollTop(0);

    }
    check_completed(data);

}

function save_carbon_management_view()
{

        var level = $("#id_accreditation_level").val();
        if (level == "level_1" || level == "level_2")
        {
            $('#submit-application-toggle').tab('show');
        }
        else if (level == "level_3" || level == "level_3_plus")
        {
            $('#carbon-stakeholder-toggle').tab('show');
        }

        $('html,body').scrollTop(0);


}

function save_carbon_stakeholder(callback)
{
    var form_data = $("#applications-form").serialize();

    var data = {
        'application_id': $("#id_saved_application").val(),
        'form_data': form_data
    };

    Dajaxice.applications.save_application(function(returned_data){
        save_carbon_stakeholder_callback(returned_data, callback);
    }, data);
}


function save_carbon_stakeholder_callback(data, callback)
{
    if (data.success){
        $('#submit-application-toggle').tab('show');
        $('html,body').scrollTop(0);
    }
    check_completed(data);
}

function save_carbon_stakeholder_view()
{

    $('#submit-application-toggle').tab('show');
    $('html,body').scrollTop(0);

    update_levels();
}

function save_emissions()
{
    $('#footprint-toggle').tab('show');
    $('html,body').scrollTop(0);
}


function save_footprint_organisational_boundary(callback)
{
    var form_data = $("#applications-form").serialize();

    var data = {
        'application_id': $("#id_saved_application").val(),
        'form_data': form_data
    };

    Dajaxice.applications.save_application(function(returned_data){
        save_footprint_organisational_boundary_callback(returned_data, callback);
    }, data);

}

function save_footprint_organisational_boundary_callback(data, callback){

    if (data.success){
        $('#operational-boundary-toggle').tab('show');
        $('html,body').scrollTop(0);
    }
    check_completed(data);
}

function save_footprint_organisational_boundary_view()
{

    $('#operational-boundary-toggle').tab('show');
    $('html,body').scrollTop(0);

}



function save_footprint_operational_boundary(callback)
{
    var form_data = $("#applications-form").serialize();

    var data = {
        'application_id': $("#id_saved_application").val(),
        'form_data': form_data
    };

    Dajaxice.applications.save_application(function(returned_data){
        save_footprint_operational_boundary_callback(returned_data, callback);
    }, data);
}


function save_footprint_operational_boundary_callback(data, callback){

    if (data.success)
    {
        $('#absolute-footprint-toggle').tab('show');
        $('html,body').scrollTop(0);
    }
    check_completed(data);

}

function save_footprint_operational_boundary_view(){


    $('#absolute-footprint-toggle').tab('show');
    $('html,body').scrollTop(0);

}


function save_footprint_absolute(callback)
{
    var form_data = $("#applications-form").serialize();

    var data = {
        'application_id': $("#id_saved_application").val(),
        'form_data': form_data
    };

    Dajaxice.applications.save_application(function(returned_data){
        save_footprint_absolute_callback(returned_data, callback);
    }, data);

}

function save_footprint_absolute_callback(data, callback)
{

    if (data.success)
    {
        var level = $("#id_accreditation_level").val();

        if (level == "level_3" || level == "level_2")
        {
            $('#carbon-reduction-toggle2').tab('show');
            $('#verification-toggle').show();
            $('#carbon-management-toggle').show();
            $('#carbon-stakeholder-toggle').show();
        }
        else if (level == "level_3_plus")
        {
            $('#footprint-offsetting-toggle').tab('show');
        }
        else
        {
            $('#carbon-footprint-toggle').tab('show');
        }

        $('html,body').scrollTop(0);
    }

    check_completed(data);
}

function save_footprint_absolute_view()
{

    var level = $("#id_accreditation_level").val();

    if (level == "level_3" || level == "level_2")
    {
        $('#carbon-reduction-toggle2').tab('show');
        $('#verification-toggle').show();
        $('#carbon-management-toggle').show();
        $('#carbon-stakeholder-toggle').show();
    }
    else if (level == "level_3_plus")
    {
        $('#footprint-offsetting-toggle').tab('show');
    }
    else
    {
        $('#carbon-footprint-toggle').tab('show');
    }

    $('html,body').scrollTop(0);

}

function save_footprint_benchmark()
{
    $('#carbon-footprint-toggle').tab('show');
    $('html,body').scrollTop(0);
}

function save_footprint_carbon(callback)
{
    var form_data = $("#applications-form").serialize();

    var data = {
        'application_id': $("#id_saved_application").val(),
        'form_data': form_data
    };

    Dajaxice.applications.save_application(function(returned_data){
        save_footprint_carbon_callback(returned_data, callback);
    }, data);

}

function save_footprint_carbon_callback(data, callback)
{
    if (data.success)
    {
        var level = $("#id_accreditation_level").val();
        if (level == "level_1")
        {
            $('#carbon-reduction-toggle').tab('show');
        }
        else
        {
            $('#footprint-offsetting-toggle').tab('show');
        }


        $('html,body').scrollTop(0);
    }
    check_completed(data);

}

function save_footprint_carbon_view()
{
   var level = $("#id_accreditation_level").val();
    if (level == "level_1")
    {
        $('#carbon-reduction-toggle').tab('show');
    }
    else
    {
        $('#footprint-offsetting-toggle').tab('show');
    }

    $('html,body').scrollTop(0);

    update_levels();
}


function save_footprint_carbon_offsetting(callback)
{
    var form_data = $("#applications-form").serialize();

    var data = {
        'application_id': $("#id_saved_application").val(),
        'form_data': form_data
    };

    Dajaxice.applications.save_application(function(returned_data){
        save_footprint_carbon_offsetting_callback(returned_data, callback);
    }, data);

}

function save_footprint_carbon_offsetting_callback(data, callback)
{
    if (data.success)
    {
        $('#footprint-summary-toggle').tab('show');
        $('html,body').scrollTop(0);
    }

    check_completed(data);

}

function save_footprint_carbon_offsetting_view()
{
    $('#carbon-reduction-toggle2').tab('show');
    $('#verification-toggle').show();
    $('#carbon-management-toggle').show();
    $('#carbon-stakeholder-toggle').show();
    $('html,body').scrollTop(0);
}



function verification_next(page)
{
   $(page).tab('show');
   $('html,body').scrollTop(0);
}

function go_to_submit()
{
    $('#submit-application-toggle').tab('show');
    $('html,body').scrollTop(0);
}


function update_levels()
{
    var level = $("#id_accreditation_level").val();

    if (level == "level_1")
    {
        $(".well").each(function() {

            $(this).show();
            if ($(this).attr('level1') != "True")
            {
                $(this).hide();
            }
        });

        $(".app_menu").each(function() {

            $(this).show();
            if ($(this).attr('level1') != "True")
            {
                $(this).hide();
            }
        });

    }

    else if (level == "level_2")
    {
        $(".well").each(function() {

            $(this).show();
            if ($(this).attr('level2') != "True")
            {
                $(this).hide();
            }
        });
        $(".app_menu").each(function() {

            $(this).show();
            if ($(this).attr('level2') != "True")
            {
                $(this).hide();
            }
        });
    }
    else if (level == "level_3")
    {
        $(".well").each(function() {

            $(this).show();
            if ($(this).attr('level3') != "True")
            {
                $(this).hide();
            }
        });

        $(".app_menu").each(function() {

            $(this).show();
            if ($(this).attr('level3') != "True")
            {
                $(this).hide();
            }
        });

    }
    else if (level == "level_3_plus")
    {
        $(".well").each(function() {

            $(this).show();
            if ($(this).attr('level4') != "True")
            {
                $(this).hide();
            }
        });

        $(".app_menu").each(function() {

            $(this).show();
            if ($(this).attr('level4') != "True")
            {
                $(this).hide();
            }
        });
    }

}