
$(function() {
    var hash = window.location.hash;
    $(hash).tab('show');

    $('html,body').scrollTop(0);

    var accreditation_level_id = $("#id_accreditation_level").val();
    if (accreditation_level_id)
    {
        $("#level_"+accreditation_level_id).click();
    }
    else
    {
        $(".btn-level-one").click();
    }

});


function next_page_with_scroll(page)
{
   $(page).tab('show');
   $('html,body').scrollTop(0);
}


function update_level(level, can_edit)
{
    if (can_edit == "True"){
        $("#id_accreditation_level").val(level);
    }
}

// Save Accreditation Level

function save_accreditation_level(callback)
{

    var form_data = $("#form-accreditation-level").serialize();

    var data = {
        'application_id': $("#application_id_accreditation_level").val(),
        'form_data': form_data
    };

    Dajaxice.applications.save_accreditation_level(function(returned_data){
        save_accreditation_level_callback(returned_data, callback);
    }, data);
}


function save_accreditation_level_callback(data)
{
    if (data.success)
    {
        var application_id = data.application_id;
        window.location.href="/application/"+application_id+"/first/";
        $('html,body').scrollTop(0);
    }

}


// Save question form

function save_question_form(subsection, callback)
{
    var form_data = $("#question-form").serialize();

    var data = {
        'application_id': $("#application_id_accreditation_level").val(),
        'form_data': form_data,
        'subsection_id':subsection
    };

    Dajaxice.applications.save_subsection(function(returned_data){
        save_subsection_callback(returned_data, callback);
    }, data);

}


function save_subsection_callback(data)
{
    if (data.success)
    {
        var application_id = data.application_id;
        var next_subsection = data.next_section_id;

        if (next_subsection)
        {
            window.location.href="/application/"+application_id+"/subsection/"+next_subsection+"/";
            $('html,body').scrollTop(0);
        }
        else
        {
            window.location.href="/applications/"+application_id+"/#application-submit-toggle";
            $('html,body').scrollTop(0);
        }

    }
}

function submit_application(application_id)
{
    var can_submit = $("#user_can_submit").val();
    var requires_verification = $("#id_requires_verification").is(":checked");

    $('#submit-application-button').addClass('disabled');
    $('#submit-application-button').html('<i class="icon-save"></i> Saving');


    var verification = false;
    if (requires_verification)
    {
        verification = true;
    }

    if (can_submit == "False")
    {
        alert("You cannot submit this application until all mandatory questions have been completed.");

        $('#submit-application-button').removeClass('disabled');
        $('#submit-application-button').html('<i class="icon-star"></i> Submit');

    }
    else
    {
       window.location.href="/application/"+application_id+"/"+verification+"/submit/";
    }

}


// ********* Delete Attachment ********* //

function delete_attachment(attachment_id){
    var attachment_container = $("#application-attachment-" + attachment_id);
    var trash_icon = attachment_container.find('.icon-trash');
    var spinner = attachment_container.find('.spinner');

    trash_icon.hide();
    spinner.show();

    var data = {
        'attachment_id': attachment_id
    };

    Dajaxice.applications.delete_attachment(delete_attachment_callback, data);

    function delete_attachment_callback(data){
        if(data.success){
            attachment_container.remove();
        }
        else {
            spinner.hide();
            trash_icon.show();
            alert("You do not have permission to remove this attachment.");
        }
    }
}


// **************** Comments *************** //

function new_comment_button_callback() {
    $("#new-comment-button").hide();
    $("#new-comment-form").show();
}

function new_comment_cancel_callback() {
    $("#new-comment-form").hide();
    $("#new-comment-button").show();
    $("#id_comment").val('');
}

function new_comment_create(application_id, subsection_id) {

    $("#new-comment-form-create-button").button('loading');

    var comment = $("#id_comment").val();

    var data = {
        'application_id': application_id,
        'subsection_id': subsection_id,
        'comment': comment
    };

    Dajaxice.applications.save_application_comment(new_comment_create_callback, data);
}


function new_comment_create_callback(data) {
    $("#new-comment-form-create-button").button('reset');

    if (data.success) {
        $("#id_comment").val('');
        $("#new-comment-form").hide();
        $("#new-comment-button").show();
        var comment_id = data.comment_id;
    }

    var comments = $("#new-comment-list");
    comments.append(data.html);

    $("#comment-user-"+comment_id).last().effect('highlight', {}, 2000);

    $('html,body').scrollTop(2000);
}

function delete_comment(comment_id)
{
    var data = {
        'comment_id': comment_id
    };

    Dajaxice.applications.delete_comment(delete_comment_callback, data);
}

function delete_comment_callback(data)
{
    if (data.success) {
        var comment = $("#comment-" + data.deleted_comment_id);
        comment.hide();
    }

    else {
        alert("Delete failed");
    }
}