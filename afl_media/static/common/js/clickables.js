$(document).ready(function() {
    $('.clickable-rows tr').click(function(event) {
        if (!event.target.href && !$(event.target).parent().attr('href')){
            var href = $(this).find("a").not(".ignore-for-clickable").attr("href");
            if(href) {
                window.location = href;
            }
        }
    });

    $('.clickable').click(function(event) {
        if (!event.target.href && !$(event.target).parent().attr('href')){
            var href = $(this).find("a").not(".ignore-for-clickable").attr("href");
            if(href) {
                window.location = href;
            }
        }
    });

    $('.clickable-rows tr').hover(
        function() {
            $(this).addClass('clickable-rows-hover');
        },
        function() {
            $(this).removeClass('clickable-rows-hover');
        }
    );
});
