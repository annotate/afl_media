import re
from random import randint

from users.models import User


def get_max_length(model, field_name):
    field = model._meta.get_field_by_name(field_name)[0]
    return field.max_length


def generate_username(first_name, last_name):
    """
    Generates a username in the format '{firstname}{lastname}[{counter}]
    """
    # Remove all non-alphanumeric chars from the first and last names
    pattern = re.compile('[\W_]+')
    clean_firstname = pattern.sub('', first_name).lower()
    clean_lastname = pattern.sub('', last_name).lower()

    # Put first and last names together, limiting the length to some thing that
    # will fit in the username field (minus 10 chars or so to make room for _{counter})
    clean_fullname = (u'%s%s' % (clean_firstname, clean_lastname))[:get_max_length(User, 'username')-10]

    found_username = False
    i = 0
    while not found_username:
        if i == 0:
            username_to_try = clean_fullname
        else:
            username_to_try = u'%s%s' % (clean_fullname, i)

        try:
            User.objects.get(username=username_to_try)
            i += 1
        except User.DoesNotExist, e:
            found_username = True
            username = username_to_try

    return username


def generate_password(num_syllables=6):
    syllable_pool = [
        'ba', 'be', 'bi', 'bo', 'bu', 'by', 'da', 'de', 'di', 'do', 'du',
        'dy', 'fa', 'fe', 'fi', 'fo', 'fu', 'fy', 'ga', 'ge', 'gi', 'go', 'gu',
        'gy', 'ha', 'he', 'hi', 'ho', 'hu', 'hy', 'ja', 'je', 'ji', 'jo', 'ju',
        'jy', 'ka', 'ke', 'ki', 'ko', 'ku', 'ky', 'la', 'le', 'li', 'lo', 'lu',
        'ly', 'ma', 'me', 'mi', 'mo', 'mu', 'my', 'na', 'ne', 'ni', 'no', 'nu',
        'ny', 'pa', 'pe', 'pi', 'po', 'pu', 'py', 'ra', 're', 'ri', 'ro', 'ru',
        'ry', 'sa', 'se', 'si', 'so', 'su', 'sy', 'ta', 'te', 'ti', 'to', 'tu',
        'ty', 'va', 've', 'vi', 'vo', 'vu', 'vy', 'bra', 'bre', 'bri', 'bro', 'bru',
        'bry', 'dra', 'dre', 'dri', 'dro', 'dru', 'dry', 'fra', 'fre', 'fri', 'fro',
        'fru', 'fry', 'gra', 'gre', 'gri', 'gro', 'gru', 'gry', 'pra', 'pre', 'pri',
        'pro', 'pru', 'pry', 'sta', 'ste', 'sti', 'sto', 'stu', 'sty', 'tra', 'tre'
    ]

    syllables = []

    for idx in range(0, num_syllables):
        syllable = syllable_pool[randint(0, len(syllable_pool) - 1)]
        syllables.append(syllable)

    return ''.join(syllables)