from django.contrib.auth.decorators import user_passes_test
from django.shortcuts import redirect
from django.contrib import messages


#@user_admin_required
def user_admin_required(function=None):
    """Requires the user to be an administrator"""
    def is_administrator(u):
        return u.is_administrator

    actual_decorator = user_passes_test(is_administrator)

    if function:
        return actual_decorator(function)

    return actual_decorator


decorator_with_arguments = lambda decorator: lambda *args, **kwargs: lambda func: decorator(func, *args, **kwargs)

#@terms_and_conditions_required('terms_and_conditions_accepted')
@decorator_with_arguments
def terms_and_conditions_required(function, perm):

    def _function(request, *args, **kwargs):
        profile = request.user.get_profile()
        if profile.terms_and_conditions_accepted:
            return function(request, *args, **kwargs)
        else:
            messages.error(request, "The terms and conditions have been updated. You will need to read and accept the conditions to continue.")
            return redirect('terms-and-conditions')
    return _function

