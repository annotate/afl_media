from django.db import models
from django.contrib.auth.models import User
from django.conf import settings
import os
from mptt.models import MPTTModel, TreeForeignKey
from .workflows import create_in_magic_evs_file
import xml.etree.ElementTree as ET


class Directories(MPTTModel):

    ROOT_PATH = 'top_level'
    SDNA_FOLDER = 'sdna_folder'
    EVS_LOG_PATH = 'evs_folder'
    AVID_PATH = 'avid_path'

    PATH_CHOICES = (
        (ROOT_PATH, 'Top level'),
        (SDNA_FOLDER, 'SDNA Folder'),
        (EVS_LOG_PATH, 'EVS log Path'),
        (AVID_PATH, 'Avid Path')
    )

    name = models.CharField(max_length=255)
    parent = TreeForeignKey('self', null=True, blank=True, related_name='children', db_index=True)
    full_path_text = models.TextField(null=True, blank=True)

    folder_and_children_done = models.BooleanField(default=False)
    folder_section = models.CharField(max_length=50, choices=PATH_CHOICES, default=SDNA_FOLDER)

    modified = models.DateTimeField(verbose_name="Date last modified", auto_now=True, null=True, help_text="The date and time at which the file was last modified")

    class MPTTMeta:
        order_insertion_by = ['name']

    def __unicode__(self):
        return self.name

    def get_file_count(self):
        return len(self.directory_files.all())

    def get_all_files(self):
        return self.directory_files.all().order_by('-status')

    def get_all_new_files(self):
        return self.directory_files.all()

    def count_archived_files(self):
        return len(self.directory_files.filter(status=FilesInCatalogue.ARCHIVED_FILE))

    def get_active_node(self):
        return 'btn-default'

    def get_tree_display(self):
        if self.children.all():
            folders = self.children.filter(folder_and_children_done=False).values("name").distinct().count()

            if self.folder_and_children_done:
                return '<i class="jstree-icon green"></i><span style="color: #059616 !important; font-weight: bold;">%s</span>' % self.name
            else:
                return '<span>%s  -  [%s folders]</span>' % (self.name, folders)

        #CF1D1D

        if self.folder_and_children_done:
            return '<i class="jstree-icon green"></i><span style="color: #059616 !important; font-weight: bold;">%s</span>' % self.name
        else:
            return '<span>%s  -  [%s folders]</span>' % (self.name, len(self.children.filter(folder_and_children_done=False)))

    def mark_complete(self):
        self.folder_and_children_done = True
        self.save()

    def check_for_new_files(self):

        if os.path.isdir(self.full_path_text):
            for fn in os.listdir(self.full_path_text):
                if os.path.isfile(os.path.join(self.full_path_text, fn)):
                    if fn not in FilesInCatalogue.ignore_files:
                        file_path = os.path.join(self.full_path_text, fn)

                        if os.path.isfile(file_path):
                            files = FilesInCatalogue.objects.filter(name=fn,
                                                                            directory=self,
                                                                            full_path_text=self.full_path_text)

                            if not files:
                                clip_id = self.get_file_clip_id(file_path)

                                file = FilesInCatalogue.objects.create(name=fn,
                                                                directory=self,
                                                                full_path_text=self.full_path_text,
                                                                status=FilesInCatalogue.NEW_FILE)

                                if clip_id:
                                    file.clip_id = self.get_file_clip_id(file_path)
                                else:
                                    check_clip = self.get_file_clip_via_source(file_path)
                                    file.clip_id = check_clip

                                file.tape_id = self.get_file_tape_id(file_path)
                                file.save()


                            else:
                                file = files[0]
                                duplicates = FilesInCatalogue.objects.filter(name=fn,
                                                                directory=self,
                                                                full_path_text=self.full_path_text).exclude(pk=file.pk)

                                duplicates.delete()


    def check_for_new_folders(self):
        if os.path.isdir(self.full_path_text):

            folders_in_path = []
            for fn in os.listdir(self.full_path_text):
                if os.path.isdir(os.path.join(self.full_path_text, fn)):

                    # folders_in_path.append({'name': fn, 'path': os.path.join(self.full_path_text, fn)})
                    folders_in_path.append(fn)

                    if fn not in FilesInCatalogue.ignore_files:
                        file_path = os.path.join(self.full_path_text, fn)

                        if os.path.isdir(file_path):
                            if not self.folder_section == self.EVS_LOG_PATH and os.path.isdir(file_path):

                                if fn not in FilesInCatalogue.ignore_files:
                                    folders = Directories.objects.filter(name=fn,
                                                                         parent=self,
                                                                         full_path_text=file_path)

                                    if not folders:
                                        Directories.objects.create(name=fn,
                                                                   parent=self,
                                                                   full_path_text=file_path)

                                    else:
                                        directory = folders[0]
                                        duplicates = Directories.objects.filter(name=fn,
                                                                                parent=self).exclude(pk=directory.pk)

                                        duplicates.delete()

            # folders = Directories.objects.filter(parent=self).exclude(parent__isnull=True)
            #
            # for folder in folders:
            #     if folder.name not in folders_in_path:
            #         folder.delete()

    def get_folder_or_file_section(self, path):
        section = None
        if "/1/" in path:
            return FilesInCatalogue.AVID_PATH
        if not section:
            if "/1" in path:
                return FilesInCatalogue.AVID_PATH

        return self.folder_section

    def get_file_clip_id(self, file_path):
        try:
            tree = ET.parse(file_path)
            root = tree.getroot()
        except:
            return None

        clip_text_id = None
        for child in root.iter('clip'):
            clip_text_id = child.attrib['clipid']
            return clip_text_id

        return clip_text_id

    def get_file_clip_via_source(self, file_path):
        try:
            tree = ET.parse(file_path)
            root = tree.getroot()
        except:
            return None

        clip_text_id = None
        for child in root:
            if child.tag == "sourcemobid":
                if child.text:
                    clip_text_id = child.text

        if clip_text_id:
            return clip_text_id


    def get_file_tape_id(self, file_path):

        try:
            tree = ET.parse(file_path)
            root = tree.getroot()
        except:
            return None

        tape_id = None
        if root.tag == "file":
            for child in root.iter("action"):
                try:
                    tape_id = child.attrib['tapeid']
                except:
                    pass

        if not tape_id:
            for action in root.iter('files'):
                for child in action:
                    try:
                        tape_id = child.attrib['tapeserial']
                    except:
                        pass

        return tape_id


class FileCatalogueManager(models.Manager):

    def check_evs_home_folder(self):
        evs_path = settings.EVS_LOG_WALK

        duplicates = Directories.objects.filter(full_path_text=evs_path,
                                                folder_section=Directories.EVS_LOG_PATH,
                                                parent__isnull=True)

        if len(duplicates) == 0:
            directory = Directories.objects.create(full_path_text=evs_path,
                                                   folder_section=Directories.EVS_LOG_PATH,
                                                   parent=None)

        elif len(duplicates) == 1:
            directory = Directories.objects.filter(full_path_text=evs_path,
                                                    folder_section=Directories.EVS_LOG_PATH,
                                                    parent__isnull=True)[0]

            directory.name = 'EVS LOGS'
            directory.save()
        else:
            directory = Directories.objects.filter(full_path_text=evs_path,
                                                   folder_section=Directories.EVS_LOG_PATH,
                                                   name='EVS Logs',
                                                   parent__isnull=True).order_by('pk')[0]

            duplicates = Directories.objects.filter(full_path_text=evs_path,
                                        folder_section=Directories.EVS_LOG_PATH,
                                        parent__isnull=True).exclude(pk=directory.pk)

            duplicates.delete()

        directory = Directories.objects.filter(full_path_text=evs_path,
                                        folder_section=Directories.EVS_LOG_PATH,
                                        parent__isnull=True)[0]


        for fn in os.listdir(settings.EVS_LOG_WALK):
            # print fn
            if fn not in FilesInCatalogue.ignore_files and os.path.isfile(os.path.join(settings.EVS_LOG_WALK, fn)):
                file_path = os.path.join(settings.EVS_LOG_WALK, fn)

                file, created = FilesInCatalogue.objects.get_or_create(name=fn,
                                                                       directory=directory,
                                                                       full_path_text=settings.EVS_LOG_WALK,
                                                                       folder_section=FilesInCatalogue.EVS_FOLDER)
                if created:
                    file.clip_id = self.get_file_clip_id(file_path)
                    file.tape_id = self.get_file_tape_id(file_path)
                    file.status = FilesInCatalogue.NEW_FILE
                    file.save()

    @staticmethod
    def get_file_clip_id(file_path):
        try:
            tree = ET.parse(file_path)
            root = tree.getroot()
        except:
            return None

        clip_text_id = None
        for child in root.iter('clip'):
            clip_text_id = child.attrib['clipid']
            return clip_text_id

        if not clip_text_id:
            pass

    @staticmethod
    def get_file_tape_id(file_path):

        try:
            tree = ET.parse(file_path)
            root = tree.getroot()
        except:
            return None

        tape_id = None
        if root.tag == "file":
            for child in root.iter("action"):
                try:
                    tape_id = child.attrib['tapeid']
                except:
                    pass

        if not tape_id:
            for action in root.iter('files'):
                for child in action:
                    try:
                        tape_id = child.attrib['tapeserial']
                    except:
                        pass

        return tape_id


class FilesInCatalogue(models.Model):

    objects = FileCatalogueManager()

    ignore_files = ['.DS_Store', '.git', '.idea', '.gitignore']

    SDNA_FOLDER = 'sdna_folder'
    AVID_PATH = 'avid_path'
    EVS_FOLDER = 'evs_folder'

    FOLDER_OPTIONS = (
        (SDNA_FOLDER, 'SDNA folder'),
        (AVID_PATH, 'Avid Path'),
        (EVS_FOLDER, 'EVS folder')
    )


    NEW_FILE = 'new_file'
    ARCHIVED_FILE = 'archived_file'
    IGNORED_FILE = 'ignored_file'
    ERROR_FILE = 'error_file'
    IS_FOLDER = 'is_folder'

    STATUS_OPTIONS = (
        (NEW_FILE, 'New'),
        (ARCHIVED_FILE, 'Archived'),
        (IGNORED_FILE, 'Ignored'),
        (ERROR_FILE, 'Error'),
        (IS_FOLDER, 'Is Folder')
    )

    ARCHIVE_NOT_BEGUN = 'no_attempt_made'
    ARCHIVE_SUCCESS = 'created_archive'
    ARCHIVE_FAIL = 'archive_failed_to_create'
    FILE_IGNORED = 'no_matching_files_found'

    ARCHIVE_STATUS_OPTIONS = (
        (ARCHIVE_NOT_BEGUN, 'File has not been processed yet'),
        (ARCHIVE_SUCCESS, 'Successfully created InMagic File'),
        (ARCHIVE_FAIL, 'InMagic File could not be created - Error'),
        (FILE_IGNORED, 'No matching files found to create InMagic file')
    )

    created = models.DateTimeField(verbose_name="Date created", auto_now_add=True, help_text="The date and time at which the file was entered into the system")
    modified = models.DateTimeField(verbose_name="Date last modified", auto_now=True, help_text="The date and time at which the file was last modified")

    modified_by = models.ForeignKey(User, null=True, blank=True)

    name = models.CharField(max_length=255, null=True, blank=True, help_text="The file name")
    full_path_text = models.TextField(null=True, blank=True, help_text="The full path of the file")
    directory = models.ForeignKey(Directories, null=True, blank=True, related_name="directory_files")

    clip_id = models.CharField(max_length=255, null=True, blank=True, help_text="The clip ID of the file")

    matched_log_file = models.ForeignKey('FileCatalogueLog', null=True, blank=True)

    status = models.CharField(max_length=255, choices=STATUS_OPTIONS, default=NEW_FILE)
    archive_file_status = models.CharField(max_length=255, choices=ARCHIVE_STATUS_OPTIONS, default=ARCHIVE_NOT_BEGUN)

    broadcast = models.BooleanField(default=False)

    folder_section = models.CharField(max_length=255, choices=FOLDER_OPTIONS, default=SDNA_FOLDER)
    folder_link = models.CharField(max_length=255, null=True, blank=True)
    parent_link = models.CharField(max_length=255, null=True, blank=True)

    archive_file = models.FileField(upload_to="archive_files", null=True, blank=True, help_text="The created archive file")

    archiving_notes = models.TextField(null=True, blank=True, help_text="If any errors come through they will be stored here.")

    tape_id = models.CharField(max_length=255, null=True, blank=True)

    ignore_file = models.BooleanField(default=False)

    umpires_list = models.ManyToManyField("EVSUmpires", null=True, blank=True)
    weather_list = models.ManyToManyField("EVSWeather", null=True, blank=True)
    commentators_list = models.ManyToManyField("EVSCommentators", null=True, blank=True)
    ready_for_processing = models.BooleanField(default=False)

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name_plural = "Files in Catalogue"

    @classmethod
    def create_new_file(cls, name, path, type=None, clip_id=None):
        book = cls(name=name, path=path, clip_id=clip_id, status=FilesInCatalogue.NEW_FILE,
                   file_type=os.path.splitext(name)[-1].lower().replace('.', ''))
        try:
            book.save()
        except:
            book = FilesInCatalogue.objects.get(name=name, path=path)
            ext = os.path.splitext(name)[-1].lower().replace('.', '')
            if not ext:
                book.file_type = FilesInCatalogue.NO_EXTENSION
            else:
                book.file_type = ext
            book.save()

        return book.status

    def has_matched_log_file(self):
        return self.matched_log_file is not None
    has_matched_log_file.allow_tags=True

    def is_log_file(self):
        return self.folder_section == self.EVS_FOLDER

    def get_log_files(self):
        log_files = FilesInCatalogue.objects.filter(name__iexact=self.name,
                                                 folder_section=self.EVS_FOLDER,
                                                 status=self.NEW_FILE)
        if log_files:
            return log_files[0]

    def get_football_file(self):
        football_file = FilesInCatalogue.objects.filter(name__iexact=self.name, tape_id__isnull=False).exclude(folder_section=self.EVS_FOLDER)
        if football_file:
            return football_file[0]
        else:
            football_file = FilesInCatalogue.objects.filter(name__iexact=self.name).exclude(folder_section=self.EVS_FOLDER)
            if football_file:
                return football_file[0]

    def has_matched_clip_ids(self):
        return False

    def get_file_searching_name(self):
        ext = '.%s' % self.file_type
        return self.name.replace(ext, '')

    def has_matching_xml(self):
        xml_files = FilesInCatalogue.objects.filter(name__icontains=self.get_file_searching_name(),file_type=self.XML_FILE).exclude(pk=self.pk)
        return xml_files is not None

    def has_matching_aiff(self):
        xml_files = FilesInCatalogue.objects.filter(name__icontains=self.get_file_searching_name(), file_type=self.XML_FILE).exclude(pk=self.pk)
        return xml_files is not None

    def has_matching_m2v(self):
        xml_files = FilesInCatalogue.objects.filter(name__icontains=self.get_file_searching_name(), file_type=self.M2V_FILE).exclude(pk=self.pk)
        return xml_files is not None

    def get_total_files_in_folder(self):
        total = FilesInCatalogue.objects.filter(full_path_text=self.full_path_text).exclude(status=self.IS_FOLDER)
        return len(total)

    def get_children_folders(self):
        return FilesInCatalogue.objects.filter(parent_link=self.folder_link).order_by('pk')

    def get_file_colour(self):
        if self.status == self.NEW_FILE:
            return 'btn-default'
        elif self.status == self.ARCHIVED_FILE:
            return 'btn-success'
        elif self.status == self.ERROR_FILE:
            return 'btn-danger'
        elif self.status == self.IGNORED_FILE:
            return 'btn-info'
        else:
            return None

    def has_matching_evs_log(self):
        try:
            log = FilesInCatalogue.objects.get(name=self.name, folder_section=self.EVS_FOLDER)
            if log:
                return True
        except:
            pass

    def get_log_file(self):
        try:
            log = FilesInCatalogue.objects.get(name=self.name, folder_section=self.EVS_FOLDER)
            if log:
                return log
        except:
            pass

    def get_xml_file(self):
        try:
            logs = FilesInCatalogue.objects.filter(name=self.name, folder_section=self.SDNA_FOLDER)
            if logs:
                return logs
        except:
            pass

    def get_matching_clip_files(self):
        if self.status == self.NEW_FILE and self.folder_section == self.EVS_FOLDER:
            try:
                files = FilesInCatalogue.objects.filter(name__iexact=file.name).exclude(pk=self.pk)
                return files
            except:
                return []

    def update_log_file(self):
        log = self.get_log_file()
        if log:
            log.status = self.status
            log.save()

    def update_clip_files(self):
        files = self.get_matching_clip_files()
        for log in files:
            log.status = self.status
            log.save()

    def get_file_update_form(self):
        from .forms import EVSLogEditForm
        form = EVSLogEditForm(instance=self)

        return form



class FileCatalogueLog(models.Model):

    NEW_FILE = 'new_file'
    ARCHIVED_FILE = 'archived_file'
    IGNORED_FILE = 'ignored_file'
    ERROR_FILE = 'error_file'

    STATUS_OPTIONS = (
        (NEW_FILE, 'New'),
        (ARCHIVED_FILE, 'Archived'),
        (IGNORED_FILE, 'Ignored'),
        (ERROR_FILE, 'Error')
    )

    created = models.DateTimeField(verbose_name="Date created", auto_now_add=True, help_text="The date and time at which the file was entered into the system")
    modified = models.DateTimeField(verbose_name="Date last modified", auto_now=True, help_text="The date and time at which the file was last modified")

    status = models.CharField(max_length=255, choices=STATUS_OPTIONS, default=NEW_FILE)
    file_logs = models.ManyToManyField(FilesInCatalogue, blank=True, help_text="The files saved to this dmp file")
    folders = models.ManyToManyField(Directories, blank=True, help_text="The folders selected to be saved to this dmp file")
    avid_projects = models.ManyToManyField(Directories, related_name="avid_projects", blank=True, help_text="The folders selected to be saved to this dmp file")

    class Meta:
        verbose_name = 'Bucket'
        verbose_name_plural = 'Bucket'

    def __unicode__(self):
        return '%s' % self.pk

    def push_out_magic_file(self):
        file_count = create_in_magic_evs_file(self)

        if file_count:
            if not len(self.folders.all()) and not len(self.file_logs.all()):
                self.status = self.ARCHIVED_FILE
                self.save()

            return file_count

        return False


class BucketDateList(models.Model):

    item_name = models.CharField(max_length=255, null=True, blank=True)
    date_file_added = models.DateField(auto_now_add=True, null=True, blank=True)




class FileCatalogueSet(models.Model):
    umpires = models.CharField(max_length=255, null=True, blank=True)
    weather = models.CharField(max_length=255, null=True, blank=True)
    commentators = models.CharField(max_length=255, null=True, blank=True)

    class Meta:
        verbose_name_plural = 'Umpires, weather and commentators'

    def __unicode__(self):
        return 'Umpires, weather and commentators'


class Players(models.Model):

    number = models.CharField(max_length=50, null=True, blank=True)
    initial = models.CharField(max_length=50, null=True, blank=True)
    last_name = models.CharField(max_length=50, null=True, blank=True)
    first_name = models.CharField(max_length=50, null=True, blank=True)
    team  = models.CharField(max_length=50, null=True, blank=True)

    class Meta:
        verbose_name_plural = 'Players'

    def __unicode__(self):
        return '%s' % self.last_name


class HighlightSyntax(models.Model):

    highlight = models.CharField(max_length=255, null=True, blank=True)
    syntax = models.CharField(max_length=255, null=True, blank=True)

    class Meta:
        verbose_name_plural = 'Highlight Syntax'

    def __unicode__(self):
        return '%s' % self.highlight


class NameCorrections(models.Model):

    evs_name = models.CharField(max_length=255, null=True, blank=True)
    corrected_name = models.CharField(max_length=255, null=True, blank=True)

    class Meta:
        verbose_name_plural = 'Name Corrections'

    def __unicode__(self):
        return '%s - %s' % (self.evs_name, self.corrected_name)



class EVSUmpires(models.Model):

    umpires = models.CharField(max_length=400, null=True, blank=True)

    class Meta:
        verbose_name_plural = 'Umpires'

    def __unicode__(self):
        return '%s' % self.umpires


class EVSWeather(models.Model):

    weather = models.CharField(max_length=400, null=True, blank=True)

    class Meta:
        verbose_name_plural = 'Weather'

    def __unicode__(self):
        return '%s' % self.weather


class EVSCommentators(models.Model):

    commentators = models.CharField(max_length=400, null=True, blank=True)

    class Meta:
        verbose_name_plural = 'Commentators'

    def __unicode__(self):
        return '%s' % self.commentators