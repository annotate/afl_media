from django import forms
from .models import FilesInCatalogue, EVSCommentators, EVSWeather, EVSUmpires


class EVSLogEditForm(forms.ModelForm):

    class Meta:
        model = FilesInCatalogue
        fields = ('commentators_list', 'umpires_list', 'weather_list', 'ready_for_processing')


    def __init__(self, *args, **kwargs):
        super(EVSLogEditForm, self).__init__(*args, **kwargs)

        if self.instance:
            id = 'id_commentators_%s' % self.instance.pk
            self.fields['commentators_list'].widget=forms.SelectMultiple(attrs={'id': id})
            self.fields['commentators_list'].queryset=EVSCommentators.objects.all().order_by('commentators')

            id = 'id_umpires_%s' % self.instance.pk
            self.fields['umpires_list'].widget=forms.SelectMultiple(attrs={'id': id})
            self.fields['umpires_list'].queryset=EVSUmpires.objects.all().order_by('umpires')

            id = 'id_weather_%s' % self.instance.pk
            self.fields['weather_list'].widget=forms.SelectMultiple(attrs={'id': id})
            self.fields['weather_list'].queryset=EVSWeather.objects.all().order_by('weather')

    def save(self, commit=True):
        super(EVSLogEditForm, self).save(commit=True)

        cleaned_data = self.cleaned_data
        commentators_list = cleaned_data.get('commentators_list', None)
        umpires_list = cleaned_data.get('umpires_list', None)
        weather_list = cleaned_data.get('weather_list', None)

        if self.instance:
            if commentators_list:
                for commentator in commentators_list:
                    self.instance.commentators_list.add(commentator)

            if umpires_list:
                for commentator in umpires_list:
                    self.instance.umpires_list.add(commentator)

            if weather_list:
                for commentator in weather_list:
                    self.instance.weather_list.add(commentator)

            self.instance.save()