from django import template
register = template.Library()

import os

@register.filter(name='show_file_contents')
def show_file_contents(obj, file):
    file = os.path.join(file.full_path_text, file.name)

    try:
        f = open(file, 'r')
        file_contents = str(f.read())
    except:
        file_contents = ''

    return file_contents


@register.filter(name='cutting')
def cutting(value, arg):
    return value.replace(arg, '')

@register.filter(name='can_be_deleted_by')
def can_be_deleted_by(obj, user):
    """
    Tests to see if a user can delete an object

    Argument: The user to test
    """
    return obj.can_be_deleted_by(user)