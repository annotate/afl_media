import sys
import os
import datetime
import logging
import logging.handlers
import xml.etree.ElementTree as ET
from django.conf import settings
from django.core.files.base import ContentFile, File
import shutil
from django.contrib import messages
import requests


def create_in_magic_evs_file(bucket):
    file_status = True

    if bucket.avid_projects.all():
        print 'logging avid project'
        file_status = write_folder(bucket, avid=True)

    if bucket.folders.all():
        print 'logging bucket folder'
        file_status = write_folder(bucket, avid=False)

    if bucket.file_logs.all():
        print 'logging bucket files'
        file_status = write(bucket)

    return file_status


def write_folder(bucket, avid=False):
    completed_folders = []

    if avid:
        avid_projects = bucket.avid_projects.all()
        archiving_file_name = 'catalogue_display/logs/%s_avid.dmp' % bucket.pk
    
        file = open(archiving_file_name,'w')
        for folder in avid_projects:
            success = write_folder_file(file, None, folder, avid=avid)
            if success:
                completed_folders.append(folder)
                bucket.avid_projects.remove(folder)

        file.close()

        if os.path.isfile(archiving_file_name):
            try:
                shutil.move(archiving_file_name, settings.EVS_LOG_WALK_DONE)
            except:
                print 'Error moving AVID file: ', archiving_file_name

    else:
        non_avid_projects = bucket.folders.all()
        archiving_file_name = 'catalogue_display/logs/%s_folders.dmp' % bucket.pk

        file = open(archiving_file_name,'w')
        for folder in non_avid_projects:
            success = write_folder_file(file, None, folder, avid=avid)
            if success:
                completed_folders.append(folder)
                bucket.folders.remove(folder)

        file.close()

        if os.path.isfile(archiving_file_name):
            shutil.move(archiving_file_name, settings.EVS_LOG_WALK_DONE)
            try:
                shutil.move(archiving_file_name, settings.EVS_LOG_WALK_DONE)
            except:
                print 'Error moving Non Avid file: ', archiving_file_name

    if completed_folders:
        for folder in completed_folders:
            folder.folder_and_children_done=True
            folder.save()

        return True


def write_folder_file(file, archiving_file, folder, avid):
    import itertools
    from .models import FilesInCatalogue, Directories

    project_size = 0
    project_duration = datetime.datetime(2015, 9, 01, 0, 0, 0)
    completed_clips = []
    tape_id = '0'

    files_to_ignore = ["mixdown", ".copy", "_dissolve", "FPS", "FF"]

    files_in_project = []
    if archiving_file:
        files_in_project.append(archiving_file)

    files_in_project_ids = []

    if folder:
        directory = Directories.objects.get(pk=folder.pk)

        files_in_directory = FilesInCatalogue.objects.filter(directory=directory)

        for file_in_directory in files_in_directory:
            files_in_project.append(file_in_directory)
            files_in_project_ids.append(file_in_directory.pk)

        children = directory.get_children()
        children = children.filter(folder_section=directory.folder_section)

        if children:
            for child in children:

                files_in_child = FilesInCatalogue.objects.filter(directory=child)
                for file_in_directory in files_in_child:
                    files_in_project.append(file_in_directory)
                    files_in_project_ids.append(file_in_directory.pk)

                for children_folder in child.get_children():
                    files_in_children = FilesInCatalogue.objects.filter(directory=children_folder)
                    for file_in_directory in files_in_children:
                        files_in_project.append(file_in_directory)
                        files_in_project_ids.append(file_in_directory.pk)

                    for cc in children_folder.get_children():
                        files_in_cc = FilesInCatalogue.objects.filter(directory=cc)
                        for file_in_directory in files_in_cc:
                            files_in_project.append(file_in_directory)
                            files_in_project_ids.append(file_in_directory.pk)

    # print 'files_in_project', files_in_project
    for ffile in files_in_project:

        file_name = str(ffile.name)

        try:
            if ffile and ffile.tape_id:
                tape_id = ffile.tape_id
        except:
            tape_id = '0'

        #Only add the clip if it does not have the words "mixdown", ".copy", "_dissolve", "FPS" or "FF" in the name
        if not any(itertools.imap(file_name.__contains__, files_to_ignore)):
            #Log the individual clip
            directory_title = ffile.directory.name
            testing_clip_id = ffile.clip_id

            # print 'testing_clip_id', testing_clip_id

            path = str(ffile.directory.full_path_text).replace("/home/user/MediaSort/file_mounts/sdna_to_inmagic/FILE_CATALOGUE_DATA/", "")
            path = path.replace("/Users/martajaniak/Sites/django/annotate/afl_media/documents/SDNA/SDNA_CATALOGUES/", "")

            date = '-'
            duration = '-'

            try:
                try:
                    tree = ET.parse(os.path.join(ffile.full_path_text, ffile.name))
                except:
                    tree = None

                if tree:
                    root = tree.getroot()

                    for child in root:
                        for log_chunk in child:
                            if str(log_chunk.attrib) == "{'name': 'clip-duration'}":
                                duration = str(log_chunk.text)[-12:-1]
                            if str(log_chunk.attrib) == "{'name': 'Duration'}":
                                duration = str(log_chunk.text)
                            if str(log_chunk.attrib) == "{'name': 'clip-created'}":
                                date = str(log_chunk.text)
            except:
                duration = '-'
                date = '-'

            try:
                archive_name = path.split('/')[0]
            except:
                archive_name = directory_title

            title = get_title(os.path.join(ffile.full_path_text, file_name), ffile)
            type = get_type(os.path.join(ffile.full_path_text, file_name))
            server_time_code = ''

            try:
                if 'broadcast' in str(archiving_file.name).lower():
                    type = 'broadcast'
                elif 'iso' in str(archiving_file.name).lower():
                    type = 'ISO'
            except:
                pass

            if not avid:
                details = get_size_and_tapeid_folder(os.path.join(ffile.full_path_text, file_name))
                tape_id = details['tapeid']
                item_size = details['size']
            else:
                matching_files = FilesInCatalogue.objects.filter(clip_id=ffile.clip_id, folder_section=FilesInCatalogue.AVID_PATH)

                clip_size = 0
                clip_tape_id = ffile.tape_id

                for mfile in matching_files:

                    file_to_check = os.path.join(mfile.full_path_text, mfile.name)
                    matching_file_details = get_avid_size_and_tapeid_folder(file_to_check)

                    if matching_file_details:
                        clip_size += matching_file_details['size']
                        clip_tape_id = matching_file_details['tapeid']
                        server_time_code = matching_file_details['server_time_code']

                tape_id = clip_tape_id
                item_size = clip_size

                details = get_avid_matching_clip_ids_and_file_size(ffile.full_path_text, ffile.clip_id, ffile.name)
                if "size" in details and details['size']:
                    item_size = details['size']

            try:
                size_in_gb = "%.2f" % ((item_size / 1024.0))
                if str(size_in_gb) == "0.00":
                    size_in_gb = "0.01"
            except:
                size_in_gb = item_size

            project_size = project_size + item_size

            if duration and not duration == '-':

                duration_split = duration.split(":")
                # print duration_split, duration

                hours = 0
                minutes = 0
                seconds = 0

                if duration_split and len(duration_split) > 1:
                    hours = int(duration_split[0])
                    minutes = int(duration_split[1])
                    seconds = int(duration_split[2])

                # print project_duration, datetime.timedelta(hours=hours)

                project_duration = project_duration + datetime.timedelta(hours=hours) + datetime.timedelta(minutes=minutes) + datetime.timedelta(seconds=seconds)

            date_of_entry = datetime.datetime.now().strftime("%d-%b-%y")
            description = '-'

            try:
                file.write("'Item Title'\t "+str(title)+"\r\n")
                file.write("Path\t "+str(path)+"\r\n")
                file.write("'Archive Name'\t "+str(archive_name)+"\r\n")
                file.write("'Item Type'\t "+str(type)+"\r\n")
                file.write("'Tape Number'\t "+str(tape_id)+"\r\n")
                file.write("'Tape Format'\t LTO\r\n")
                file.write('Date\t'+date+'\r\n')
                file.write("'Date of Entry'\t"+str(date_of_entry)+'\r\n')
                if not avid:
                    file.write('Description\t'+description+'\r\n')
                else:
                    file.write("Duration\t"+duration+"\r\n")

                file.write("Size\t "+str(size_in_gb)+"\r\n") #Size - listed as name - listed as bytes under size in avid path version
                # file.write("'Clip ID'\t "+str(ffile.clip_id)+"\r\n")

                file.write("$\r\n")

                if ffile.clip_id not in completed_clips:
                    completed_clips.append(ffile.clip_id)

            except IOError as e:
                error = "I/O error({0}): {1}".format(e.errno, e.strerror)
                ffile.archiving_notes = error
                ffile.save()
                print error

                return False
            except ValueError:
                error = "Could not convert data to an integer."
                ffile.archiving_notes = error
                ffile.save()
                print error
                return False
            except:
                error = "Unexpected error:", sys.exc_info()[0]
                ffile.archiving_notes = error
                ffile.save()
                print error
                return False

    if folder:
        #Log the avid project itself
        directory_title = directory.name
        path = str(directory.full_path_text).replace("/home/user/MediaSort/file_mounts/sdna_to_inmagic/FILE_CATALOGUE_DATA/", "")
        path = path.replace("/Users/martajaniak/Sites/django/annotate/afl_media/documents/SDNA/SDNA_CATALOGUES/", "")
        date_of_entry = datetime.datetime.now().strftime("%d-%b-%y")
        description = '-'

        try:
            archive_name = path.split('/')[0]
        except:
            archive_name = directory_title

        try:
            size_in_gb = "%.2f" % ((project_size / 1024.0) / 1024.0)
            if str(size_in_gb) == "0.00":
                size_in_gb = "0.01"
        except:
            size_in_gb = project_size

        if avid:
            project_duration = format(project_duration, '%H:%M:%S')
            file.write("'Project Title'\t "+str(directory_title)+"\r\n")
            file.write("Duration\t"+str(project_duration)+"\r\n")
        else:
            file.write("'Item Title'\t "+str(directory_title)+"\r\n")

        file.write("Path\t "+path+"\r\n")
        file.write("'Archive Name'\t"+archive_name+"\r\n") #Archive - media path

        if avid:
            file.write("'Item Type'\tProject\r\n")
        else:
            file.write("'Item Type'\tFolder\r\n")
            file.write('Description\t'+str(description)+'\r\n')

        file.write("'Tape Number'\t"+str(tape_id)+"\r\n")
        file.write("'Date of Entry'\t"+str(date_of_entry)+'\r\n')
        file.write("'Tape Format'\t LTO\r\n") #Tape Format (LTO)

        file.write("Size\t "+str(size_in_gb)+"\r\n") #Size - listed as name - listed as bytes under size in avid path version
        file.write("$\r\n")

    #we should be finished here:
    try:
        if files_in_project_ids:
            FilesInCatalogue.objects.filter(pk__in=files_in_project_ids).update(status=FilesInCatalogue.ARCHIVED_FILE)
    except:
        pass

    if archiving_file:
        archiving_file.status = FilesInCatalogue.ARCHIVED_FILE
        archiving_file.save()

    return True


def get_type(file_path_location):

    try:
        tree = ET.parse(file_path_location)
    except:
        tree = None

    if tree:
        root = tree.getroot()

        if root.tag == "clip":
            return 'Clip'

        if root.tag == "file":
            return 'File'

    return 'Project'


def get_avid_matching_clip_ids_and_file_size(file_path_location, file_clip_id, file):

    # print 'file_path_location', file_path_location
    # print 'clip_id', file_clip_id
    # print 'file', file
    # print '---------------------------------'

    size = 0
    tape_id = '0'
    server_time_code = ''
    clip_id = ''

    from .models import FilesInCatalogue

    #First check if in AVID section:
    matching_clip_id = False
    print 'Checking for avid files with matching clip IDs: ', file_clip_id
    check_files = FilesInCatalogue.objects.filter(clip_id=file_clip_id, folder_section=FilesInCatalogue.AVID_PATH)
    print 'check_files', check_files

    if check_files:
        print 'have avid matching clip ID files'
        for avid_file in check_files:
            if os.path.isfile(os.path.join(avid_file.full_path_text, avid_file.name)):
                try:
                    tree = ET.parse(os.path.join(os.path.join(avid_file.full_path_text, avid_file.name)))
                    root = tree.getroot()
                    if root.tag == "file":
                        for child in root.iter("action"):
                            print child.tag, child.attrib

                            try:
                                tape_id = child.attrib['tapeid']
                            except:
                                pass

                            try:
                                size = int(child.attrib['size'])
                            except:
                                pass

                    if root.tag == "clip":
                        for child in root.iter("data"):
                            if "name" in child.attrib and child.attrib['name'] == "File Size":
                                try:
                                    if "MB" in child.text:
                                        size = str(child.text).replace("MB", "")
                                        size = int(size)
                                    else:
                                        size = int(child.text)
                                except:
                                    size = 0

                            if not clip_id:
                                if "material-package-uid" in child.attrib and child.attrib['material-package-uid']:
                                    try:
                                        clip_id = child.attrib['material-package-uid']
                                    except:
                                        pass

                        if "clipid" in root.attrib and root.attrib['clipid']:
                            clip_id = root.attrib['clipid']

                        for row in root.iter("files"):
                            for child in row:
                                try:
                                    clip_id = child.attrib['clipid']
                                except:
                                    pass

                                if not clip_id:
                                    try:
                                        clip_id = child.attrib['material-package-uid']
                                    except:
                                        pass

                                try:
                                    tape_id = child.attrib['tapeserial']
                                except:
                                    pass

                                for file_child in child:
                                    try:
                                        tape_id = file_child.attrib['tapeserial']
                                    except:
                                        pass

                    if clip_id and str(file_clip_id).replace('-', '') == str(clip_id).replace('-', ''):
                        matching_clip_id = True
                        return {'size': size, 'tape_id': tape_id}

                except:
                    pass

    # if not matching_clip_id:
    #     print 'stepping through avid locations'
    #     locations = [file_path_location.replace("/metadata/", "/1/"), file_path_location.replace("/metadata/Projects", "/metadata/1/avidisis/")]
    #
    #     for location in locations:
    #         location_split = location.split("/")
    #         new_location = ''
    #         for item in location_split:
    #             if not item == "1":
    #                 new_location += str(item) +'/'
    #             else:
    #                 break

            # if os.path.isdir(new_location):
            #     for subdir, dirs, files in os.walk(new_location):
            #         for avid_file in files:
            #
            #             try:
            #                 tree = ET.parse(os.path.join(subdir, avid_file))
            #                 root = tree.getroot()
            #
            #                 if root.tag == "file":
            #                     for child in root.iter("action"):
            #                         print child.tag, child.attrib
            #
            #                         try:
            #                             tape_id = child.attrib['tapeid']
            #                         except:
            #                             pass
            #
            #                         try:
            #                             size = int(child.attrib['size'])
            #                         except:
            #                             pass
            #
            #
            #                 if root.tag == "clip":
            #                     for child in root.iter("data"):
            #                         if "name" in child.attrib and child.attrib['name'] == "File Size":
            #                             try:
            #                                 if "MB" in child.text:
            #                                     size = str(child.text).replace("MB", "")
            #                                     size = int(size)
            #                                 else:
            #                                     size = int(child.text)
            #                             except:
            #                                 size = 0
            #
            #                         if not clip_id:
            #                             if "material-package-uid" in child.attrib and child.attrib['material-package-uid']:
            #                                 try:
            #                                     clip_id = child.attrib['material-package-uid']
            #                                 except:
            #                                     pass
            #
            #                     if "clipid" in root.attrib and root.attrib['clipid']:
            #                         clip_id = root.attrib['clipid']
            #
            #                     for row in root.iter("files"):
            #                         for child in row:
            #                             try:
            #                                 clip_id = child.attrib['clipid']
            #                             except:
            #                                 pass
            #
            #                             if not clip_id:
            #                                 try:
            #                                     clip_id = child.attrib['material-package-uid']
            #                                 except:
            #                                     pass
            #
            #                             try:
            #                                 tape_id = child.attrib['tapeserial']
            #                             except:
            #                                 pass
            #
            #                             for file_child in child:
            #                                 try:
            #                                     tape_id = file_child.attrib['tapeserial']
            #                                 except:
            #                                     pass
            #
            #                 if clip_id and str(file_clip_id).replace('-', '') == str(clip_id).replace('-', ''):
            #                     return {'size': size, 'tape_id': tape_id}
            #
            #             except:
            #                 pass

    return {'size': size, 'tape_id': tape_id}


def get_avid_size_and_tapeid_folder(file_path_location, clip_id=None):

    size = 0
    tape_id = '0'
    server_time_code = ''




    if os.path.isfile(file_path_location):
        try:
            tree = ET.parse(file_path_location)
        except:
            tree = None

        if tree:
            root = tree.getroot()

            if root.tag == "file":
                for child in root.iter("action"):
                    try:
                        tape_id = child.attrib['tapeid']
                    except:
                        pass

                    try:
                        size = int(child.attrib['size'])
                    except:
                        pass

                    try:
                        server_time_code = child.attrib['File Access Date/Time']
                    except:
                        pass

            if root.tag == "clip":
                for child in root.iter("files"):
                    try:
                        tape_id = child.attrib['tapeserial']
                    except:
                        pass

                    for file_child in child:
                        try:
                            tape_id = file_child.attrib['tapeserial']
                        except:
                            pass

                for child in root.iter("data"):

                    try:
                        server_time_code = child.attrib['File Access Date/Time']
                    except:
                        pass



    return {'size': size, 'tapeid': tape_id, 'server_time_code': server_time_code}


def get_size_and_tapeid_folder(file_path_location):

    size = 0
    tape_id = '0'
    server_time_code = ''
    clip_id = '-'

    avid_check_file_path_location = file_path_location.replace("/metadata", "/1").replace(".XML", '.evs.xml')
    avid_check_file_path_location_two = file_path_location.replace("/metadata/Projects", "/metadata/Volumes")

    print 'avid_check_file_path_location_two', avid_check_file_path_location_two

    if os.path.isfile(avid_check_file_path_location):
        try:
            tree = ET.parse(avid_check_file_path_location)
            root = tree.getroot()

            if root.tag == "file":
                for child in root.iter("action"):
                    try:
                        tape_id = child.attrib['tapeid']
                    except:
                        pass

                    try:
                        size = int(child.attrib['size'])
                    except:
                        pass

                    try:
                        server_time_code = int(child.attrib['write-time'])
                    except:
                        pass
        except:
            pass

    elif size == '-' and os.path.isfile(avid_check_file_path_location_two):
        try:
            tree = ET.parse(avid_check_file_path_location_two)
            root = tree.getroot()

            if root.tag == "file":
                for child in root.iter("action"):
                    try:
                        tape_id = child.attrib['tapeid']
                    except:
                        pass

                    try:
                        size = int(child.attrib['size'])
                    except:
                        pass

                    try:
                        server_time_code = int(child.attrib['write-time'])
                    except:
                        pass
        except:
            pass

    else:
        if os.path.isfile(file_path_location):
            tree = ET.parse(file_path_location)
            root = tree.getroot()

            if root.tag == "file":
                for child in root.iter("action"):
                    try:
                        tape_id = child.attrib['tapeid']
                    except:
                        pass

                    try:
                        size = int(child.attrib['size'])

                    except:
                        pass

                    try:
                        server_time_code = int(child.attrib['write-time'])
                    except:
                        pass

            if root.tag == "clip":
                for child in root.iter("files"):
                    try:
                        clip_id = child.attrib['material-package-uid']
                    except:
                        pass

                    try:
                        tape_id = child.attrib['tapeserial']
                    except:
                        pass

                    for file_child in child:
                        try:
                            tape_id = file_child.attrib['tapeserial']
                        except:
                            pass

                for child in root.iter("data"):
                    if "name" in child.attrib and child.attrib['name'] == "File Size":
                        try:
                            if "MB" in child.text:
                                size = str(child.text).replace("MB", "")
                                size = int(size)
                            else:
                                size = int(child.text)
                        except:
                            size = 0

    return {'size': size, 'tapeid': tape_id, 'server_time_code': server_time_code, 'new_clip_id': clip_id}


def get_title(file_path_location, file_name):

    try:
        tree = ET.parse(file_path_location)
    except:
        tree = None

    title = ''

    if tree:
        root = tree.getroot()

        title = file_name
        if root.tag == "clip":
            try:
                title = root.attrib['name']

            except:
                pass

        if root.tag == "file":
            try:
                title = root.attrib['name']
            except:
                pass

    return title


def write(bucket):
    from .models import FilesInCatalogue

    folders = bucket.folders.all()
    files = bucket.file_logs.all()
    is_team = False

    is_football = False
    iso_files = []
    non_iso_files = []
    project_files = []
    completed_files = []
    completed_file_ids = []
    potential_game_files = []

    for file in files:

        if file.folder_section == FilesInCatalogue.SDNA_FOLDER:
            evs_file = FilesInCatalogue.objects.filter(name=file.name, folder_section=FilesInCatalogue.EVS_FOLDER)
            if evs_file:
                for efile in evs_file:
                    efile.tape_id = file.tape_id
                    efile.save()

                    football_log = get_team_names(os.path.join(efile.full_path_text, efile.name))
                    football_log = football_log['football_match']

                    if not football_log:
                        project_files.append(efile)
                    else:
                        is_football = True
                        if "_ISO" in str(efile.name) and efile not in iso_files:
                            is_football = True
                            iso_files.append(efile)
                        else:
                            non_iso_files.append(efile)
            else:
                try:
                    name_element = file.name
                    names = name_element.split("_")
                    check_team_one = names[0]
                    check_team_two = team_name_list(check_team_one)
                    if not str(check_team_one) == str(check_team_two):
                        is_football = True
                        potential_game_files.append(file.pk)
                except:
                    pass


        football_log = get_team_names(os.path.join(file.full_path_text, file.name))
        football_log = football_log['football_match']

        if not football_log:
            project_files.append(file)
        else:
            if "_ISO" in str(file.name):
                is_football = True
                iso_files.append(file)
            else:
                non_iso_files.append(file)


    for folder in folders:
        print 'Logging out folder: ', folder
        files = FilesInCatalogue.objects.filter(directory=folder)
        for file in files:
            if file.folder_section == FilesInCatalogue.SDNA_FOLDER:
                evs_file = FilesInCatalogue.objects.filter(name=file.name, folder_section=FilesInCatalogue.EVS_FOLDER, ready_for_processing=True)
                if evs_file:
                    for efile in evs_file:
                        football_log = get_team_names(os.path.join(efile.full_path_text, efile.name))
                        football_log = football_log['football_match']

                        if not football_log:
                            project_files.append(efile)
                        else:
                            if "_ISO" in str(efile.name) and efile not in iso_files:
                                iso_files.append(efile)
                            else:
                                non_iso_files.append(efile)

            football_log = get_team_names(os.path.join(file.full_path_text, file.name))
            football_log = football_log['football_match']

            if not football_log:
                project_files.append(file)
            else:
                if "_ISO" in str(file.name):
                    iso_files.append(file)
                else:
                    non_iso_files.append(file)

    if non_iso_files:
        print "Non ISO Files to dmp"
        archiving_file_name = 'catalogue_display/logs/%s_broadcast.dmp' % bucket.pk
        new_file_name = 'MATCH RECORDING'

        file = open(archiving_file_name,'w')
        for archiving_file in non_iso_files:
            success = write_file(file, archiving_file, FilesInCatalogue, new_file_name)
            if success:
                completed_files.append(archiving_file)
                completed_file_ids.append(archiving_file.pk)
            else:
                file_not_completed = FilesInCatalogue.objects.get(pk=archiving_file.pk)
                file_not_completed.status = FilesInCatalogue.ERROR_FILE
                file_not_completed.save()

        file.close()

        if os.path.isfile(archiving_file_name):
            try:
                shutil.move(archiving_file_name, settings.EVS_LOG_WALK_DONE)
            except:
                print 'Error moving Non ISO file: ', archiving_file_name

    #ISO Files to dmp
    if iso_files:
        print "ISO Files to dmp"
        archiving_file_name = 'catalogue_display/logs/%s_iso.dmp' % bucket.pk
        new_file_name = 'Iso'
        file = open(archiving_file_name,'w')

        for archiving_file in iso_files:
            success = write_file(file, archiving_file, FilesInCatalogue, new_file_name)
            if success:
                completed_files.append(archiving_file)
                completed_file_ids.append(archiving_file.pk)
            else:
                try:
                    file_not_completed = FilesInCatalogue.objects.get(pk=archiving_file.pk)
                    file_not_completed.status = FilesInCatalogue.IGNORED_FILE
                    file_not_completed.save()
                except:
                    pass

        file.close()
        if os.path.isfile(archiving_file_name):
            try:
                shutil.move(archiving_file_name, settings.EVS_LOG_WALK_DONE)
            except:
                print 'Error moving ISO file: ', archiving_file_name


    if project_files:
        if is_football:
            print "Normal project Files to dmp"
            archiving_file_name = 'catalogue_display/logs/%s_football.dmp' % bucket.pk
        else:
            print "Normal project Files to dmp"
            archiving_file_name = 'catalogue_display/logs/%s_library.dmp' % bucket.pk

        new_file_name = 'Files'
        file = open(archiving_file_name,'w')

        for archiving_file in project_files:
            # file, folder, avid
            success = write_folder_file(file, archiving_file, None, False)
            if success:
                completed_files.append(archiving_file)
                completed_file_ids.append(archiving_file.pk)
            else:
                try:
                    file_not_completed = FilesInCatalogue.objects.get(pk=archiving_file.pk)
                    file_not_completed.status = FilesInCatalogue.IGNORED_FILE
                    file_not_completed.save()
                except:
                    pass

        file.close()
        if os.path.isfile(archiving_file_name):
            try:
                shutil.move(archiving_file_name, settings.EVS_LOG_WALK_DONE)
            except:
                print 'Error moving Library file: ', archiving_file_name

    if completed_files:
        FilesInCatalogue.objects.filter(pk__in=completed_file_ids).update(status=FilesInCatalogue.ARCHIVED_FILE)

        for file in completed_files:
            matching_clip_ids = FilesInCatalogue.objects.filter(clip_id__isnull=False).filter(clip_id=file.clip_id).exclude(folder_section=FilesInCatalogue.EVS_FOLDER)
            if matching_clip_ids:
                for efile in matching_clip_ids:
                    efile.status = FilesInCatalogue.ARCHIVED_FILE
                    efile.save()

            bucket.file_logs.remove(file)
            bucket.save()


        FilesInCatalogue.objects.filter(pk__in=potential_game_files).update(archiving_notes="Marking as ignored: Potential Football Log however EVS Log Not Found. File is logged out to Library DMP file.", status=FilesInCatalogue.IGNORED_FILE)

        return len(completed_files)
    else:
        return False
        
        
def write_file(file, archiving_file, FilesInCatalogue, new_file_name):

    ffile = None
    if not archiving_file.is_log_file():
        log_file = archiving_file.get_log_file()
        if log_file:
            archiving_file = log_file
        else:
            archiving_file.archiving_notes = "Did not find EVS matching log file - Logging in Library DMP"
            archiving_file.save()

    try:
        ffile = archiving_file.get_football_file()
    except:
        pass

    initial_file_location = os.path.join(archiving_file.full_path_text, archiving_file.name)

    initial_football_file_location = None
    if ffile:
        initial_football_file_location = os.path.join(ffile.full_path_text, ffile.name)

    if os.path.isfile(initial_file_location):
        team_names = get_team_names(initial_file_location)
        home_team = team_names['home_team']
        away_team = team_names['away_team']
        venue = team_names['venue']
        round = team_names['round']
        football_match = team_names['football_match']

        tree = ET.parse(initial_file_location)
        root = tree.getroot()

        duration = ''
        date = ''

        if ffile:
            archive_path = str(ffile.full_path_text).replace("/home/user/MediaSort/file_mounts/sdna_to_inmagic/FILE_CATALOGUE_DATA/", "")
            archive_path = archive_path.replace("/Users/martajaniak/Sites/django/annotate/afl_media/documents/SDNA/SDNA_CATALOGUES/", "")
            try:
                ff_tree = ET.parse(initial_football_file_location)
                ff_root = ff_tree.getroot()

                for child in ff_root:
                    for log_chunk in child:
                        if str(log_chunk.attrib) == "{'name': 'clip-duration'}":
                            duration = str(log_chunk.text)[-12:-1]
                        if str(log_chunk.attrib) == "{'name': 'Duration'}":
                                duration = str(log_chunk.text)
                        if str(log_chunk.attrib) == "{'name': 'clip-created'}":
                            date = str(log_chunk.text)

            except:
                duration = '-'
                date = '-'
        else:

            archive_path = str(archiving_file.full_path_text).replace("/home/user/MediaSort/file_mounts/sdna_to_inmagic/FILE_CATALOGUE_DATA/", "")
            archive_path = archive_path.replace("/Users/martajaniak/Sites/django/annotate/afl_media/documents/SDNA/SDNA_CATALOGUES/", "")

            try:
                for child in root:
                    for log_chunk in child:
                        if str(log_chunk.attrib) == "{'name': 'clip-duration'}":
                            duration = str(log_chunk.text)[-12:-1]
                        if str(log_chunk.attrib) == "{'name': 'Duration'}":
                                duration = str(log_chunk.text)
                        if str(log_chunk.attrib) == "{'name': 'clip-created'}":
                            date = str(log_chunk.text)

            except:
                duration = '-'
                date = '-'


        title = str(archiving_file.name)


        details = get_size_and_tapeid_folder(initial_file_location)
        item_size = details['size']
        server_time_code = details['server_time_code']

        try:
            size_in_gb = "%.2f" % ((item_size / 1024.0))
            if str(size_in_gb) == "0.00":
                size_in_gb = "0.01"
        except:
            size_in_gb = item_size

        if size_in_gb == '0.01':
            if initial_football_file_location:
                football_details = get_size_and_tapeid_folder(initial_football_file_location)
                fitem_size = football_details['size']
                fserver_time_code = football_details['server_time_code']
                if not server_time_code:
                    server_time_code = fserver_time_code
                try:
                    size_in_gb = "%.2f" % (float(fitem_size) / 1024.0)
                    if str(size_in_gb) == "0.00":
                        size_in_gb = "0.01"
                except:
                    size_in_gb = fitem_size


        try:
            type = get_type(initial_file_location)
        except:
            type = '-'

        try:
            if 'broadcast' in str(file.name).lower():
                type = 'broadcast'
            elif 'iso' in str(file.name).lower():
                type = 'ISO'
        except:
            pass

        try:

            for child in root.findall('Logs'):
                for log_chunk in child:

                    if not date:
                        date = log_chunk.find('DateUser').text
                    date_of_entry = datetime.datetime.now().strftime("%d-%b-%y")
                    description = str(log_chunk.find('Description').text)

                    if not description:
                        description = ''

                    description += ' %s' % str(server_time_code)

                    if not football_match:
                        file.write('Date\t'+date+'\r\n')
                        file.write("'Date of Entry'\t"+str(date_of_entry)+'\r\n')
                        file.write("Client\t\r\n")
                        file.write("Contact\t\r\n")
                        file.write('Description\t'+description+'\r\n')
                        file.write("Project Title\t\r\n")
                        file.write('Item Title\t'+archiving_file.name+'\r\n')
                        file.write('Duration\t'+duration+'\r\n')
                        file.write('$\r\n')

                    else:
                        timecode = log_chunk.find("TC").text.replace(":", ".")[:8]

                        server_timecode = log_chunk.find("TCUser").text.replace(":", ".")[:8]

                        if ffile and not archiving_file.tape_id:
                            tape_id = ffile.tape_id

                            if not tape_id:
                                if tape_id in ['0', None]:
                                    details = get_size_and_tapeid_folder(os.path.join(ffile.full_path_text, ffile.name))
                                    tape_id = details['tape_id']
                        else:
                            tape_id = '0'

                        if tape_id in ['0', None] and archiving_file and archiving_file.tape_id:
                            tape_id = archiving_file.tape_id

                        highlights = get_highlights(log_chunk)

                        name = get_participants(log_chunk)

                        try:
                            umpire_list = []
                            for u in archiving_file.umpires_list.all():
                                umpire_list.append(str(u))

                            umpire = umpire_list[0]
                            for h in umpire_list[1:]:
                                if not h == '':
                                    umpire += '\r\n; '+ str(h)

                            if not umpire or umpire == 'None':
                                umpire = '-'

                            commentators_list = []
                            for u in archiving_file.commentators_list.all():
                                commentators_list.append(str(u))

                            commentators = commentators_list[0]
                            for h in commentators_list[1:]:
                                if not h == '':
                                    commentators += '\r\n; '+ str(h)

                            if not commentators or commentators == 'None':
                                commentators = '-'

                            weather_list = []
                            for u in archiving_file.weather_list.all():
                                weather_list.append(str(u))

                            weather = weather_list[0]
                            for h in weather_list[1:]:
                                if not h == '':
                                    weather += '\r\n; '+ str(h)

                            if not weather or weather == 'None':
                                weather = '-'
                        except:
                            umpire = '-'
                            commentators = '-'
                            weather = '-'

                        tape_format = get_tape_format()
                        description += ' %s' % str(server_timecode)

                        file.write("'Record ID'\t1"+'\r\n')
                        file.write("'Tape Number'\t"+str(tape_id)+'\r\n')
                        file.write("'Tape Title'\t"+str(new_file_name)+'\r\n')
                        file.write("'Tape Format'\t"+str(tape_format)+"\r\n")
                        file.write("'Home Team'\t" +str(home_team)+'\r\n')
                        file.write("'Away Team'\t"+str(away_team)+'\r\n')
                        file.write('Round\t'+str(round)+'\r\n')
                        file.write('Date\t'+str(date)+'\r\n')
                        file.write('Venue\t'+str(venue)+'\r\n')

                        if type == "broadcast":
                            file.write('Umpires\t'+str(umpire)+'\r\n')
                            file.write('Weather\t'+str(weather)+'\r\n')
                            file.write('Quarter\t-\r\n')

                        file.write('Commentators\t'+str(commentators)+'\r\n')
                        file.write('Night-Day\t'+get_night_day(server_timecode)+'\r\n')

                        file.write("'Time Code'\t"+str(timecode)+"\r\n")
                        file.write("'Server TimeCode'\t"+str(server_timecode)+"\r\n")
                        file.write('Name\t'+str(name)+'\r\n')
                        file.write('Highlight\t'+str(highlights)+'\r\n')
                        file.write('Description\t'+str(description)+'\r\n')
                        file.write("'Date of Entry'\t"+str(date_of_entry)+'\r\n')

                        file.write('Siren1\t\r\n')
                        file.write('Siren2\t\r\n')
                        file.write("Operator\t\r\n")
                        file.write("'Clip Name'\t"+title+'\r\n')
                        file.write("Path\t"+archive_path+"\r\n")
                        file.write("'Item Type'\t"+str(type)+"\r\n")
                        file.write("Size\t "+str(size_in_gb)+"\r\n")
                        file.write('$\r\n')
        
        except IOError as e:
            error = "I/O error({0}): {1}".format(e.errno, e.strerror)
            archiving_file.archiving_notes = error
            archiving_file.save()
            print error

            return False
        except ValueError:
            error = "Could not convert data to an integer."
            archiving_file.archiving_notes = error
            archiving_file.save()
            print error
            return False
        except:
            error = "Unexpected error:", sys.exc_info()[0]
            archiving_file.archiving_notes = error
            archiving_file.save()
            print error
            return False

    return True


def get_tape_format():
    return 'LTO'


def get_highlights(log_chunk):
    ignore_keys = ["MOD" ,"ROD" ,"GOD" ,"TOD" ,"COD" ,"COM"]

    highlights = ''
    testing_running = ''
    highlighted = set()

    for keywords in log_chunk.iter('Keywords'):
        for keyword in keywords:
            if keyword.attrib == {'Type': 'Keyword'}:
                if keyword.text and keyword.text not in ignore_keys and keyword.text not in highlighted:
                    testing_running += keyword.text
                    highlighted.add(get_highlight_syntax(keyword.text))

    highlighted = list(highlighted)

    if highlighted and len(highlighted) > 1:
        highlight_string = str(highlighted[0])
        for h in highlighted[1:]:
            if not h == '':
                highlight_string += '\r\n; '+ h

        return highlight_string
    else:
        return 'General highlights'



def get_highlight_syntax(highlight):
    from .models import HighlightSyntax
    highlight_split = highlight.split(" ")
    highlighted = []

    for highlight in highlight_split:
        highlight = str(highlight).lower()
        set = HighlightSyntax.objects.filter(highlight__iexact=highlight)
        if set:
            set = set[0]
            highlighted.append(set.syntax)

    if highlighted and len(highlighted) > 1:
        highlight_string = str(highlighted[0])
        for h in highlighted[1:]:
            highlight_string += '\r\n; '+ h
    else:
        highlight_string = ''
        if highlighted:
            highlight_string = highlighted[0]

    # print 'highlight_string', highlight_string
    return highlight_string


def get_participants(log_chunk):

    highlighted = []

    participants = ''
    for keywords in log_chunk.iter('Keywords'):
        for keyword in keywords:
            if keyword.attrib == {'Type': 'Participant'}:
                correction = str(get_player_names(keyword.text))
                if correction and not correction in ["None", None, "", " "]:
                    highlighted.append(correction)

    if highlighted and len(highlighted) > 1:
        participants = str(highlighted[0])
        for h in highlighted[1:]:
            participants += '\r\n; '+ h

    elif highlighted and len(highlighted) == 1:
        if not highlighted[0]:
            participants = ''
        else:
            participants = highlighted[0]

    return participants


def get_sirens(log_chunk):
    siren = ''
    for keywords in log_chunk.iter('Keywords'):
        for keyword in keywords:
            if keyword.attrib == {'Type': 'SIREN'}:
                siren = log_chunk.find("TC").text.replace(":", ".")[:8]

    return siren





def is_football_log(file_path_location):
    tree = ET.parse(file_path_location)
    root = tree.getroot()

    football_log = True
    try:
        name_element = root[0][0].text
        names = name_element.split("_")
        # print 'names', names

        home_team = names[0]
        away_team = names[2]
    except:
        try:
            name_element = root[0][0].text
            names = name_element.split(" ")
            # print 'names', names

            home_team = names[0]
            away_team = names[2]
        except:
            football_log = False

    return football_log

def get_team_names(file_path_location):

    try:
        tree = ET.parse(file_path_location)
    except:
        tree = None

    names = None
    home_team = '-'
    away_team = '-'
    venue = '-'
    round = '0'


    if tree:
        root = tree.getroot()

        try:
            name_element = root[0][0].text
            names = name_element.split("_")

            home_team = names[0]
            away_team = names[2]
        except:
            try:
                name_element = root[0][0].text
                names = name_element.split(" ")
                print 'names', names

                home_team = names[0]
                away_team = names[2]
            except:
                pass

        if names:
            try:
                for check in names:
                    if "RD" in str(check):
                        round = str(check).replace('RD', '')
            except:
                round = '0'



        for child in root.iter('UserFields'):
            for field in child:
                if field.attrib:
                    try:
                        if field.attrib['Header']:
                            if str(field.attrib['Header']) == "HOME TEAM":
                                home_team = str(field.text)
                                if home_team == 'None':
                                    home_team = '-'
                            if str(field.attrib['Header']) == "AWAY TEAM":
                                away_team = str(field.text)
                                if away_team  == 'None':
                                    away_team = '-'
                                    round = '0'
                            if str(field.attrib['Header']) == "STADIUM":
                                venue = get_stadium_correction(field.text)
                                if venue in ["None", None]:
                                    venue = '-'
                    except:
                       pass

    football_match = True
    if home_team == "-" and away_team == "-" and venue == "-" and round == '0':
        football_match = False

    # print {'home_team': team_name_list(home_team), 'away_team': team_name_list(away_team), 'venue': venue, 'round': round, 'football_match': football_match}
    return {'home_team': team_name_list(home_team), 'away_team': team_name_list(away_team), 'venue': venue, 'round': round, 'football_match': football_match}


def get_night_day(time):
    try:
        time =  int(time[:2])
        if time < 16:
            return 'Day'
        elif time > 16 and time < 19:
            return 'Twilight'
        else:
            return 'Night'
    except:
        return '-'


def get_weather():
    from .models import FileCatalogueSet
    set = FileCatalogueSet.objects.all()
    if set:
        set = set[0]
        if not set.weather:
            return '-'

        return set.weather
    return '-'


def get_commentators():
    from .models import FileCatalogueSet
    set = FileCatalogueSet.objects.all()
    if set:
        set = set[0]
        if not set.commentators:
            return '-'
        return set.commentators
    return '-'


def get_umpires():
    from .models import FileCatalogueSet
    set = FileCatalogueSet.objects.all()
    if set:
        set = set[0]

        if not set.umpires:
            return '-'

        return set.umpires
    return '-'


def get_player_names(name):
    from .models import Players, NameCorrections

    name_split = name.split(" ")
    try:
        number = name_split[0]
        initial = name_split[1]
        last_name = name_split[2]

        set = Players.objects.filter(number=number, initial=initial, last_name=last_name)
        if set:
            set = set[0]
            name = '%s, %s' % (set.last_name, str(set.first_name).lstrip().rstrip())

            check_corrected_name = NameCorrections.objects.filter(evs_name=name)
            if check_corrected_name:
                name = '%s' % (check_corrected_name[0].corrected_name).lstrip().rstrip()

            return name
    except:
        return ' '


def team_name_list(team_name):

    team_dict = [{"ADEL": "Adelaide"},
                 {"ADE": "Adelaide"},
                 {"BRIS": "Brisbane"},
                 {"BRI": "Brisbane"},
                 {"CARL": "Carlton"},
                 {"CAR": "Carlton"},
                 {"COLL": "Collingwood"},
                 {"COL": "Collingwood"},
                 {"ESS": "Essendon"},
                 {"FREM": "Fremantle"},
                 {"FRE": "Fremantle"},
                 {"GEEL": "Geelong"},
                 {"GEE": "Geelong"},
                 {"GCS": "Gold Coast"},
                 {"GWS": "GWS Giants"},
                 {"HAW": "Hawthorn"},
                 {"MELB": "Melbourne"},
                 {"NTH": "North Melbourne"},
                 {"PORT": "Port Adelaide"},
                 {"POR": "Port Adelaide"},
                 {"RICH": "Richmond"},
                 {"RIC": "Richmond"},
                 {"STK": "St Kilda"},
                 {"SYD": "Sydney"},
                 {"WCE": "West Coast"},
                 {"WBD": "Western Bulldogs"}]


    for team in team_dict:
        try:
            team_name = team[team_name]
        except:
            team_name = team_name

    if team_name:
        return team_name
    return ''



def get_stadium_correction(stadium):
    if stadium:
        if stadium == "AAMI":
            return "AAMI Stadium"
        if stadium == "MANUKA OVAL":
            return "Manuka Oval Canberra"
        if stadium == "MELBOURNE CRICKET GROUND":
            return "MCG"
        if stadium == "SYDNEY CRICKET GROUND":
            return "SCG"
        if stadium == "TIO STADIUM TIO":
            return "Stadium, Darwin"
        if stadium == "GABBA":
            return "GABBA"
    return stadium

