from django.conf.urls import patterns, url


urlpatterns = patterns('',
    url(r'^display/$', 'catalogue_display.views.index', name='catalogue-display'),
    url(r'^display/sdna/$', 'catalogue_display.views.sdna_index', name='catalogue-display-sdna'),
    url(r'^completed/folder/(?P<node_id>.*)/$', 'catalogue_display.views.folder_complete', name='catalogue-complete-folder'),
    url(r'^completed/dmp/$', 'catalogue_display.views.completed_dmp_files', name='catalogue-completed-dmp'),
    url(r'^evs/display/$', 'catalogue_display.views.evs_logs_index', name='catalogue-display-evs'),
    url(r'^evs/display/(?P<node_id>.*)/$', 'catalogue_display.views.evs_logs_folder_display', name='catalogue-display-folder-evs'),

)