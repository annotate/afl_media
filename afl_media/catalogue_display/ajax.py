from django.shortcuts import render, get_object_or_404
from .models import *
from catalogue_display.models import *
from django.http import HttpResponse
import json
from django.http import QueryDict
from dajaxice.decorators import dajaxice_register
from django.template.loader import render_to_string


from mptt.templatetags.mptt_tags import cache_tree_children
import json
from .forms import EVSLogEditForm

def get_evs_tree_structure(request):
    if request.is_ajax():
        root_nodes = cache_tree_children(Directories.objects.filter(folder_section=Directories.EVS_LOG_PATH,
                                                                    parent__isnull=True))
        dicts = []
        for n in root_nodes:
            dicts.append({
                'id': n.pk,
                'text': n.get_tree_display(),
                'complete': n.folder_and_children_done,
                'children': get_node_children(request, n.pk)
            })

        data = json.dumps(dicts, indent=4)

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)



def get_root_structure(request):
    if request.is_ajax():
        root_nodes = cache_tree_children(Directories.objects.filter(folder_section=Directories.ROOT_PATH, parent__isnull=True))

        dicts = []
        for n in root_nodes:
            dicts.append({
                'id': n.pk,
                'text': n.get_tree_display(),
                'complete': n.folder_and_children_done,
                'children': get_node_children(request, n.pk)
            })

        data = json.dumps(dicts, indent=4)

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def get_node_children(request, directory_id):
    node = get_object_or_404(Directories, pk=directory_id)
    node.check_for_new_folders()
    node.check_for_new_files()
    children = node.get_children()
    # children = children.filter(folder_and_children_done=False)

    check_dict = []
    for child in children:
        has_children = False
        if len(child.get_children()):
            has_children = True

        result = {
            'id': child.pk,
            'text': child.get_tree_display(),
            'children': has_children
        }
        check_dict.append(result)

    return check_dict


def get_node_children_structure(request, directory_id):
    node = get_object_or_404(Directories, pk=directory_id)
    children = node.get_children()
    node.check_for_new_folders()
    # children = children.filter(folder_and_children_done=False)

    check_dict = []
    check_dict_name = []
    for child in children:
        if child.name not in check_dict_name:
            check_dict_name.append(child.name)
            child.check_for_new_folders()
            has_children = False
            if len(child.get_children()):
                has_children = True

            result = {
                'id': child.pk,
                'text': child.get_tree_display(),
                'children': has_children
            }

            check_dict.append(result)

    data = json.dumps(check_dict, indent=4)
    mimetype = 'application/json'
    return HttpResponse(data, mimetype)


def save_evs_log(request, file_id):

    if request.is_ajax():
        file = get_object_or_404(FilesInCatalogue, pk=file_id)

        if request.GET:
            commentators_list = []
            umpires_list = []
            weather_list = []
            for key, value in request.GET.iteritems():
                if key == 'commentators':
                    commentators_list = value.split(",")
                if key == 'umpires':
                    umpires_list = value.split(",")
                if key == 'weather':
                    weather_list = value.split(",")

            data = {
                'commentators_list':commentators_list,
                'umpires_list': umpires_list,
                'weather_list': weather_list
            }

            form = EVSLogEditForm(data=data, instance=file)
            if form.is_valid():
                form.save()
            else:
                print form.errors

        context = {
            'success': True
        }

        return HttpResponse(context)

def get_directory_files(request, directory_id):

    if request.is_ajax():

        directory = get_object_or_404(Directories, pk=directory_id)
        directory.check_for_new_folders()
        directory.check_for_new_files()
        all_node_files = directory.get_all_new_files()

        evs_logs = False
        if directory.folder_section == Directories.EVS_LOG_PATH:
            evs_logs = True

        template = 'catalogue_display/includes/directory_files.html'
        context = {'selected_node': directory,
                   'all_node_files': all_node_files,
                   'evs_logs': evs_logs,
                   'folder_form': EVSLogEditForm()
                   }
        directory_html = render_to_string(template, context)

        dicts = {
            'html': directory_html,
            'directory_name': directory.name,
            'file_count': len(all_node_files),
            'id': directory.pk
        }

        data = json.dumps(dicts, indent=4)

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def add_folder_and_children_to_bucket(request, bucket, directory):

    bucket.folders.add(directory)
    for children_folders in directory.get_children():
        add_folder_and_children_to_bucket(request, bucket, children_folders)


def add_folder_to_bucket(request):
    if request.is_ajax():

        folder_id = request.POST['select-folder']
        directory = Directories.objects.get(pk=folder_id)
        latest_folder, created = FileCatalogueLog.objects.get_or_create(status=FileCatalogueLog.NEW_FILE)

        latest_folder.folders.add(directory)

        for children_folders in directory.get_children():
            add_folder_and_children_to_bucket(request, latest_folder, children_folders)

        latest_folder.save()
        BucketDateList.objects.create(item_name=directory.name)

        context = {'bucket': latest_folder }
        template = "catalogue_display/includes/bucket.html"
        bucket_html = render_to_string(template, context)

        data = json.dumps({'success': True, 'bucket_html': bucket_html})
        mimetype = 'application/json'

        return HttpResponse(data, mimetype)


def add_folder_and_children_as_project(request, bucket, directory):

    bucket.avid_projects.add(directory)
    for children_folders in directory.get_children():
        add_folder_and_children_as_project(request, bucket, children_folders)


def add_project_to_bucket(request):
    if request.is_ajax():

        folder_id = request.POST['select-folder']
        directory = Directories.objects.get(pk=folder_id)
        latest_folder, created = FileCatalogueLog.objects.get_or_create(status=FileCatalogueLog.NEW_FILE)

        latest_folder.avid_projects.add(directory)
        latest_folder.save()
        BucketDateList.objects.create(item_name=directory.name)

        for children_folders in directory.get_children():
            add_folder_and_children_as_project(request, latest_folder, children_folders)

        context = {'bucket': latest_folder }
        template = "catalogue_display/includes/bucket.html"
        bucket_html = render_to_string(template, context)

        data = json.dumps({'success': True, 'bucket_html': bucket_html})
        mimetype = 'application/json'

        return HttpResponse(data, mimetype)



def add_files_to_bucket(request):
    if request.is_ajax():
        files = request.POST.getlist('_selected_file')
        latest_folder, created = FileCatalogueLog.objects.get_or_create(status=FileCatalogueLog.NEW_FILE)

        for file in files:
            selected_file = FilesInCatalogue.objects.get(pk=file)
            latest_folder.file_logs.add(selected_file)
            latest_folder.save()
            BucketDateList.objects.create(item_name=selected_file.name)

        context = {'bucket': latest_folder }
        template = "catalogue_display/includes/bucket.html"
        bucket_html = render_to_string(template, context)

        data = json.dumps({'success': True, 'bucket_html': bucket_html})
        mimetype = 'application/json'

        return HttpResponse(data, mimetype)



def remove_folder_from_bucket(request, folder_id):
    if request.is_ajax():
        folder = Directories.objects.get(pk=folder_id)
        latest_folder, created = FileCatalogueLog.objects.get_or_create(status=FileCatalogueLog.NEW_FILE)

        latest_folder.folders.remove(folder)
        latest_folder.save()

        BucketDateList.objects.filter(item_name=folder.name).delete()


        context = {'bucket': latest_folder }
        template = "catalogue_display/includes/bucket.html"
        bucket_html = render_to_string(template, context)

        data = json.dumps({'success': True, 'bucket_html': bucket_html})
        mimetype = 'application/json'

        return HttpResponse(data, mimetype)


def remove_project_from_bucket(request, folder_id):
    if request.is_ajax():
        folder = Directories.objects.get(pk=folder_id)
        latest_folder, created = FileCatalogueLog.objects.get_or_create(status=FileCatalogueLog.NEW_FILE)

        latest_folder.avid_projects.remove(folder)
        latest_folder.save()

        BucketDateList.objects.filter(item_name=folder.name).delete()

        context = {'bucket': latest_folder }
        template = "catalogue_display/includes/bucket.html"
        bucket_html = render_to_string(template, context)

        data = json.dumps({'success': True, 'bucket_html': bucket_html})
        mimetype = 'application/json'

        return HttpResponse(data, mimetype)


def remove_file_from_bucket(request, file_id):
    if request.is_ajax():
        file = FilesInCatalogue.objects.get(pk=file_id)
        latest_folder, created = FileCatalogueLog.objects.get_or_create(status=FileCatalogueLog.NEW_FILE)

        latest_folder.file_logs.remove(file)
        latest_folder.save()

        BucketDateList.objects.filter(item_name=file.name).delete()

        context = {'bucket': latest_folder }
        template = "catalogue_display/includes/bucket.html"
        bucket_html = render_to_string(template, context)

        data = json.dumps({'success': True, 'bucket_html': bucket_html})
        mimetype = 'application/json'

        return HttpResponse(data, mimetype)


def remove_all_from_bucket(request):
    if request.is_ajax():
        latest_folder, created = FileCatalogueLog.objects.get_or_create(status=FileCatalogueLog.NEW_FILE)

        for folder in latest_folder.folders.all():
            latest_folder.folders.remove(folder)

        for file in latest_folder.file_logs.all():
            latest_folder.file_logs.remove(file)

        latest_folder.save()

        BucketDateList.objects.all().delete()

        context = {'bucket': latest_folder }
        template = "catalogue_display/includes/bucket.html"
        bucket_html = render_to_string(template, context)

        data = json.dumps({'success': True, 'bucket_html': bucket_html})
        mimetype = 'application/json'

        return HttpResponse(data, mimetype)



def get_file_clip_id(file_path):
    try:
        tree = ET.parse(file_path)
        root = tree.getroot()
    except:
        return None

    clip_text_id = None
    for child in root.iter('clip'):
        clip_text_id = child.attrib['clipid']
        return clip_text_id

    if not clip_text_id:
        pass


def get_file_tape_id(file_path):

    try:
        tree = ET.parse(file_path)
        root = tree.getroot()
    except:
        return None

    tape_id = None
    for action in root.iter('actions'):
        for child in action:
            try:
                tape_id = child.attrib['tapeid']
            except:
                pass

    if not tape_id:
        for action in root.iter('files'):
            for child in action:
                try:
                    tape_id = child.attrib['tapeserial']
                except:
                    pass

    return tape_id
