import os
from django.conf import settings
from afl_media.generic_views import DetailLoggedInView, TemplateView
from django.shortcuts import render, get_object_or_404
from django.shortcuts import render
from django.template import loader
from .models import FilesInCatalogue, Directories, FileCatalogueLog
from django.shortcuts import redirect
from django.contrib import messages


def index(request):
    template = "catalogue_display/index.html"
    context = {}
    return render(request, template, context)


def sdna_index(request):
    template = "catalogue_display/home.html"
    latest_folder, created = FileCatalogueLog.objects.get_or_create(status=FileCatalogueLog.NEW_FILE)

    if request.POST:
        success = latest_folder.push_out_magic_file()
        latest_folder = create_new_file_for_new_id(request, latest_folder)

        if not success:
            messages.info(request, 'One or more files could not be catalogued.')
        else:
            messages.success(request, 'File(s) catalogued successfully.')

        return redirect('catalogue-display-sdna')

    context = {
        'nodes': Directories.objects.filter(folder_section=Directories.SDNA_FOLDER, parent__isnull=True),
        'selected_node': None,
        'bucket': latest_folder,
    }

    return render(request, template, context)


def mark_folder_and_children_to_bucket(request, bucket, directory):

    directory.mark_complete()
    for children_folders in directory.get_children():
        FilesInCatalogue.objects.filter(parent=children_folders).update(status=FilesInCatalogue.ARCHIVED_FILE)
        mark_folder_and_children_to_bucket(request, bucket, children_folders)


def folder_complete(request, node_id):
    selected_node = get_object_or_404(Directories, pk=node_id)
    selected_node.mark_complete()
    bucket, created = FileCatalogueLog.objects.get_or_create(status=FileCatalogueLog.NEW_FILE)

    for directory in Directories.objects.filter(parent=selected_node):
         mark_folder_and_children_to_bucket(request, bucket, directory)

    return redirect('catalogue-display-sdna')


def evs_logs_index(request):
    template = "catalogue_display/evs_home.html"
    latest_folder, created = FileCatalogueLog.objects.get_or_create(status=FileCatalogueLog.NEW_FILE)
    FilesInCatalogue.objects.check_evs_home_folder()

    if request.POST:
        success = latest_folder.push_out_magic_file()
        latest_folder = create_new_file_for_new_id(request, latest_folder)

        if not success:
            messages.info(request, 'One or more files could not be catalogued.')
        else:
            messages.success(request, 'File(s) catalogued successfully.')

        return redirect('catalogue-display-evs')

    context = {
        'nodes': Directories.objects.filter(folder_section=Directories.EVS_LOG_PATH)[0],
        'selected_node': None,
        'bucket': latest_folder,
        'evs_logs': True
    }

    return render(request, template, context)


def evs_logs_folder_display(request, node_id):
    template = "catalogue_display/evs_home.html"
    latest_folder, created = FileCatalogueLog.objects.get_or_create(status=FileCatalogueLog.NEW_FILE)

    if request.POST:
        files_to_create = request.POST.getlist('_selected_file')
        success = False
        for id in files_to_create:
            file = FilesInCatalogue.objects.get(id=id)
            success = file.push_out_magic_file()
            latest_folder = create_new_file_for_new_id(request, latest_folder)

        if not success:
            messages.info(request, 'One or more files failed to be catalogued.')
        else:
            messages.success(request, 'File(s) catalogued successfully.')

        return redirect('catalogue-display-folder-evs', node_id)

    context = {
        'node_id': int(node_id),
        'nodes': Directories.objects.filter(folder_section=Directories.EVS_LOG_PATH),
        'selected_node': Directories.objects.get(pk=node_id),
        'bucket': latest_folder
    }

    return render(request, template, context)


def completed_dmp_files(request):
    template = "catalogue_display/completed_files.html"
    files = os.listdir(os.path.join(settings.EVS_LOG_WALK, 'Done'))

    context = {
        'files': files,
        'file_path': os.path.join(settings.EVS_LOG_WALK, 'Done')
    }

    return render(request, template, context)



def create_new_file_for_new_id(request, latest_folder):
    new_folder = FileCatalogueLog.objects.create(status=FileCatalogueLog.NEW_FILE)

    if len(latest_folder.folders.all()) or len(latest_folder.file_logs.all()):
        for folder in latest_folder.folders.all():
            new_folder.folders.add(folder)

        for file in latest_folder.file_logs.all():
            new_folder.file_logs.add(file)

    latest_folder.status = FileCatalogueLog.ARCHIVED_FILE
    latest_folder.save()

    return new_folder


