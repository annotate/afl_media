# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalogue_display', '0006_auto_20150511_0623'),
    ]

    operations = [
        migrations.AddField(
            model_name='filecatalogue',
            name='file_extension',
            field=models.CharField(help_text=b'The type of file it is.', max_length=255, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='filecatalogue',
            name='file_type',
            field=models.CharField(blank=True, max_length=255, null=True, choices=[(b'avid_file', b'Avid file'), (b'xml_file', b'XML File'), (b'log_file', b'Log File')]),
            preserve_default=True,
        ),
    ]
