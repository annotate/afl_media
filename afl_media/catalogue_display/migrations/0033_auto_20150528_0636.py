# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalogue_display', '0032_filecatalogue_matched_clip_id_files'),
    ]

    operations = [
        migrations.CreateModel(
            name='FileCatalogueSet',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('umpires', models.CharField(max_length=255, null=True, blank=True)),
                ('weather', models.CharField(max_length=255, null=True, blank=True)),
                ('commentators', models.CharField(max_length=255, null=True, blank=True)),
            ],
        ),
        migrations.AlterField(
            model_name='directory',
            name='folder_section',
            field=models.CharField(default=b'SDNA_FOLDER', max_length=50, choices=[(b'top_level', b'Top level'), (b'SDNA_FOLDER', b'SDNA Metadata Path'), (b'evs_logs', b'EVS log Path'), (b'avid_path', b'Avid Path')]),
        ),
    ]
