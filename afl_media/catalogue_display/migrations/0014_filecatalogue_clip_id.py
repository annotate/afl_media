# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalogue_display', '0013_filecatalogue_path_text'),
    ]

    operations = [
        migrations.AddField(
            model_name='filecatalogue',
            name='clip_id',
            field=models.CharField(help_text=b'The clip ID of the file', max_length=255, null=True, blank=True),
            preserve_default=True,
        ),
    ]
