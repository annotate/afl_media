# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalogue_display', '0053_auto_20150927_1220'),
    ]

    operations = [
        migrations.CreateModel(
            name='EVSCommentators',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('commentators', models.CharField(max_length=400, null=True, blank=True)),
            ],
            options={
                'verbose_name_plural': 'Commentators',
            },
        ),
        migrations.CreateModel(
            name='EVSUmpires',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('umpires', models.CharField(max_length=400, null=True, blank=True)),
            ],
            options={
                'verbose_name_plural': 'Umpires',
            },
        ),
        migrations.CreateModel(
            name='EVSWeather',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('weather', models.CharField(max_length=400, null=True, blank=True)),
            ],
            options={
                'verbose_name_plural': 'Weather',
            },
        ),
        # migrations.AddField(
        #     model_name='directories',
        #     name='modified',
        #     field=models.DateTimeField(auto_now=True, help_text=b'The date and time at which the file was last modified', null=True, verbose_name=b'Date last modified'),
        # ),
    ]
