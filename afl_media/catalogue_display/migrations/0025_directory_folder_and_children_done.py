# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalogue_display', '0024_filecatalogue_directory'),
    ]

    operations = [
        migrations.AddField(
            model_name='directory',
            name='folder_and_children_done',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
    ]
