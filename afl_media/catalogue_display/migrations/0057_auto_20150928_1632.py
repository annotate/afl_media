# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalogue_display', '0056_auto_20150928_1434'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='filesincatalogue',
            name='commentators',
        ),
        migrations.RemoveField(
            model_name='filesincatalogue',
            name='evs_umpires',
        ),
        migrations.RemoveField(
            model_name='filesincatalogue',
            name='evs_weather',
        ),
        migrations.RemoveField(
            model_name='filesincatalogue',
            name='umpires',
        ),
        migrations.RemoveField(
            model_name='filesincatalogue',
            name='weather',
        ),
        migrations.AddField(
            model_name='directories',
            name='modified',
            field=models.DateTimeField(auto_now=True, help_text=b'The date and time at which the file was last modified', null=True, verbose_name=b'Date last modified'),
        ),
    ]
