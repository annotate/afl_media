# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalogue_display', '0031_filecatalogue_tape_id'),
    ]

    operations = [
        migrations.AddField(
            model_name='filecatalogue',
            name='matched_clip_id_files',
            field=models.ManyToManyField(help_text=b'All files with matching clip IDs', to='catalogue_display.FileCatalogue', null=True, blank=True),
            preserve_default=True,
        ),
    ]
