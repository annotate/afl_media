# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalogue_display', '0017_auto_20150518_0448'),
    ]

    operations = [
        migrations.AddField(
            model_name='filecatalogue',
            name='folder_link',
            field=models.CharField(max_length=255, null=True, blank=True),
            preserve_default=True,
        ),
    ]
