# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import mptt.fields
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('catalogue_display', '0047_auto_20150618_1828'),
    ]

    operations = [
        migrations.CreateModel(
            name='Directories',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
                ('full_path_text', models.TextField(null=True, blank=True)),
                ('folder_and_children_done', models.BooleanField(default=False)),
                ('folder_section', models.CharField(default=b'sdna_folder', max_length=50, choices=[(b'top_level', b'Top level'), (b'sdna_folder', b'SDNA Folder'), (b'evs_logs', b'EVS log Path'), (b'avid_path', b'Avid Path')])),
                ('lft', models.PositiveIntegerField(editable=False, db_index=True)),
                ('rght', models.PositiveIntegerField(editable=False, db_index=True)),
                ('tree_id', models.PositiveIntegerField(editable=False, db_index=True)),
                ('level', models.PositiveIntegerField(editable=False, db_index=True)),
                ('parent', mptt.fields.TreeForeignKey(related_name='children', blank=True, to='catalogue_display.Directories', null=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='FilesInCatalogue',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(help_text=b'The date and time at which the file was entered into the system', verbose_name=b'Date created', auto_now_add=True)),
                ('modified', models.DateTimeField(help_text=b'The date and time at which the file was last modified', verbose_name=b'Date last modified', auto_now=True)),
                ('name', models.CharField(help_text=b'The file name', max_length=255, null=True, blank=True)),
                ('full_path_text', models.TextField(help_text=b'The full path of the file', null=True, blank=True)),
                ('clip_id', models.CharField(help_text=b'The clip ID of the file', max_length=255, null=True, blank=True)),
                ('status', models.CharField(default=b'new_file', max_length=255, choices=[(b'new_file', b'New'), (b'archived_file', b'Archived'), (b'ignored_file', b'Ignored'), (b'error_file', b'Error'), (b'is_folder', b'Is Folder')])),
                ('archive_file_status', models.CharField(default=b'no_attempt_made', max_length=255, choices=[(b'no_attempt_made', b'File has not been processed yet'), (b'created_archive', b'Successfully created InMagic File'), (b'archive_failed_to_create', b'InMagic File could not be created - Error'), (b'no_matching_files_found', b'No matching files found to create InMagic file')])),
                ('broadcast', models.BooleanField(default=False)),
                ('folder_section', models.CharField(default=b'sdna_folder', max_length=255, choices=[(b'sdna_folder', b'SDNA folder'), (b'avid_path', b'Avid Path'), (b'evs_folder', b'EVS folder')])),
                ('folder_link', models.CharField(max_length=255, null=True, blank=True)),
                ('parent_link', models.CharField(max_length=255, null=True, blank=True)),
                ('archive_file', models.FileField(help_text=b'The created archive file', null=True, upload_to=b'archive_files', blank=True)),
                ('archiving_notes', models.TextField(help_text=b'If any errors come through they will be stored here.', null=True, blank=True)),
                ('tape_id', models.CharField(max_length=255, null=True, blank=True)),
                ('directory', models.ForeignKey(related_name='directory_files', blank=True, to='catalogue_display.Directories', null=True)),
            ],
            options={
                'verbose_name_plural': 'Files in Catalogue',
            },
        ),
        migrations.RemoveField(
            model_name='directory',
            name='parent',
        ),
        migrations.RemoveField(
            model_name='filecatalogue',
            name='directory',
        ),
        migrations.RemoveField(
            model_name='filecatalogue',
            name='matched_log_file',
        ),
        migrations.RemoveField(
            model_name='filecatalogue',
            name='modified_by',
        ),
        migrations.AlterField(
            model_name='filecataloguelog',
            name='file_logs',
            field=models.ManyToManyField(help_text=b'The files saved to this dmp file', to='catalogue_display.FilesInCatalogue', blank=True),
        ),
        migrations.AlterField(
            model_name='filecataloguelog',
            name='folders',
            field=models.ManyToManyField(help_text=b'The folders selected to be saved to this dmp file', to='catalogue_display.Directories', blank=True),
        ),
        migrations.DeleteModel(
            name='Directory',
        ),
        migrations.DeleteModel(
            name='FileCatalogue',
        ),
        migrations.AddField(
            model_name='filesincatalogue',
            name='matched_log_file',
            field=models.ForeignKey(blank=True, to='catalogue_display.FileCatalogueLog', null=True),
        ),
        migrations.AddField(
            model_name='filesincatalogue',
            name='modified_by',
            field=models.ForeignKey(blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
    ]
