# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalogue_display', '0038_auto_20150602_1203'),
    ]

    operations = [
        migrations.AddField(
            model_name='filecatalogue',
            name='archiving_notes',
            field=models.TextField(help_text=b'If any errors come through they will be stored here.', null=True, blank=True),
        ),
    ]
