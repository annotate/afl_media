# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalogue_display', '0049_auto_20150625_1649'),
    ]

    operations = [
        migrations.AddField(
            model_name='filecataloguelog',
            name='date_file_added',
            field=models.DateField(auto_now_add=True, null=True),
        ),
    ]
