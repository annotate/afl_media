# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalogue_display', '0048_auto_20150618_1856'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='filecatalogueset',
            options={'verbose_name_plural': 'Umpires, weather and commentators'},
        ),
        migrations.AlterModelOptions(
            name='highlightsyntax',
            options={'verbose_name_plural': 'Highlight Syntax'},
        ),
        migrations.AlterModelOptions(
            name='namecorrections',
            options={'verbose_name_plural': 'Name Corrections'},
        ),
        migrations.AlterModelOptions(
            name='players',
            options={'verbose_name_plural': 'Players'},
        ),
        migrations.AddField(
            model_name='filecataloguelog',
            name='avid_projects',
            field=models.ManyToManyField(help_text=b'The folders selected to be saved to this dmp file', related_name='avid_projects', to='catalogue_display.Directories', blank=True),
        ),
        migrations.AlterField(
            model_name='directories',
            name='folder_section',
            field=models.CharField(default=b'sdna_folder', max_length=50, choices=[(b'top_level', b'Top level'), (b'sdna_folder', b'SDNA Folder'), (b'evs_folder', b'EVS log Path'), (b'avid_path', b'Avid Path')]),
        ),
    ]
