# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalogue_display', '0023_auto_20150518_0704'),
    ]

    operations = [
        migrations.AddField(
            model_name='filecatalogue',
            name='directory',
            field=models.ForeignKey(related_name='directory_files', blank=True, to='catalogue_display.Directory', null=True),
            preserve_default=True,
        ),
    ]
