# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalogue_display', '0046_namecorrections'),
    ]

    operations = [
        migrations.AlterField(
            model_name='filecataloguelog',
            name='file_logs',
            field=models.ManyToManyField(help_text=b'The files saved to this dmp file', to='catalogue_display.FileCatalogue', blank=True),
        ),
        migrations.AlterField(
            model_name='filecataloguelog',
            name='folders',
            field=models.ManyToManyField(help_text=b'The folders selected to be saved to this dmp file', to='catalogue_display.Directory', blank=True),
        ),
    ]
