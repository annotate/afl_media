# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalogue_display', '0029_directory_folder_section'),
    ]

    operations = [
        migrations.AddField(
            model_name='directory',
            name='full_path_text',
            field=models.TextField(null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='directory',
            name='folder_section',
            field=models.CharField(default=b'SDNA_FOLDER', max_length=50, choices=[(b'SDNA_FOLDER', b'SDNA Metadata Path'), (b'evs_logs', b'EVS log Path'), (b'SDNA_FOLDER', b'Avid Path')]),
            preserve_default=True,
        ),
    ]
