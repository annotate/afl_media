# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('catalogue_display', '0007_auto_20150511_0638'),
    ]

    operations = [
        migrations.CreateModel(
            name='FileCatalogueLog',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(help_text=b'The date and time at which the file was entered into the system', verbose_name=b'Date created', auto_now_add=True)),
                ('modified', models.DateTimeField(help_text=b'The date and time at which the file was last modified', verbose_name=b'Date last modified', auto_now=True)),
                ('name', models.CharField(help_text=b'The file name', max_length=255, null=True, blank=True)),
                ('path', models.CharField(help_text=b'The path of the file', max_length=255, null=True, blank=True)),
                ('file_type', models.CharField(blank=True, max_length=255, null=True, choices=[(b'avid_file', b'Avid file'), (b'xml_file', b'XML File'), (b'log_file', b'Log File')])),
                ('file_extension', models.CharField(help_text=b'The type of file it is.', max_length=255, null=True, blank=True)),
                ('status', models.CharField(default=b'new_file', max_length=255, choices=[(b'new_file', b'New'), (b'archived_file', b'Archived'), (b'ignored_file', b'Ignored'), (b'error_file', b'Error')])),
                ('broadcast', models.BooleanField(default=False)),
                ('modified_by', models.ForeignKey(blank=True, to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
                'verbose_name_plural': 'Files in Catalogue',
            },
            bases=(models.Model,),
        ),
        migrations.AlterUniqueTogether(
            name='filecataloguelog',
            unique_together=set([('name', 'path')]),
        ),
    ]
