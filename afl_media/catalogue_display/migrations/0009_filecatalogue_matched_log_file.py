# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalogue_display', '0008_auto_20150511_0645'),
    ]

    operations = [
        migrations.AddField(
            model_name='filecatalogue',
            name='matched_log_file',
            field=models.ForeignKey(blank=True, to='catalogue_display.FileCatalogueLog', null=True),
            preserve_default=True,
        ),
    ]
