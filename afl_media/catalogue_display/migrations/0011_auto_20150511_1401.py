# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalogue_display', '0010_auto_20150511_0705'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='filecataloguelog',
            options={'verbose_name_plural': 'Log Files'},
        ),
        migrations.AlterField(
            model_name='filecatalogue',
            name='file_type',
            field=models.CharField(blank=True, max_length=255, null=True, choices=[(b'aiff', b'AIFF FILE'), (b'avid', b'Avid file'), (b'xml', b'XML File'), (b'log', b'Log File'), (b'm2v', b'M2V File'), (b'no_extension', b'No extension'), (b'mxf', b'MXF File'), (b'par', b'PAR File')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='filecataloguelog',
            name='file_type',
            field=models.CharField(blank=True, max_length=255, null=True, choices=[(b'aiff', b'AIFF FILE'), (b'avid', b'Avid file'), (b'xml', b'XML File'), (b'log', b'Log File'), (b'm2v', b'M2V File'), (b'no_extension', b'No extension'), (b'mxf', b'MXF File'), (b'par', b'PAR File')]),
            preserve_default=True,
        ),
    ]
