# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalogue_display', '0015_auto_20150518_0441'),
    ]

    operations = [
        migrations.AddField(
            model_name='filecatalogue',
            name='folder_section',
            field=models.CharField(default=b'metadata_folder', max_length=255, choices=[(b'metadata_folder', b'Metadata folder'), (b'one_folder', b'1 folder')]),
            preserve_default=True,
        ),
    ]
