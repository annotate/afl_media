# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalogue_display', '0014_filecatalogue_clip_id'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='filecatalogue',
            unique_together=set([]),
        ),
    ]
