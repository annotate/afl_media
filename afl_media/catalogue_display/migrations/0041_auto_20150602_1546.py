# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalogue_display', '0040_auto_20150602_1533'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='filecataloguelog',
            options={},
        ),
        migrations.AlterUniqueTogether(
            name='filecataloguelog',
            unique_together=set([]),
        ),
        migrations.RemoveField(
            model_name='filecataloguelog',
            name='broadcast',
        ),
        migrations.RemoveField(
            model_name='filecataloguelog',
            name='modified_by',
        ),
        migrations.RemoveField(
            model_name='filecataloguelog',
            name='name',
        ),
        migrations.RemoveField(
            model_name='filecataloguelog',
            name='path',
        ),
    ]
