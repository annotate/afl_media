# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalogue_display', '0052_filesincatalogue_ignore_file'),
    ]

    operations = [
        migrations.AddField(
            model_name='filesincatalogue',
            name='commentators',
            field=models.CharField(max_length=400, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='filesincatalogue',
            name='ready_for_processing',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='filesincatalogue',
            name='umpires',
            field=models.CharField(max_length=400, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='filesincatalogue',
            name='weather',
            field=models.CharField(max_length=400, null=True, blank=True),
        ),
    ]
