# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalogue_display', '0043_players'),
    ]

    operations = [
        migrations.AddField(
            model_name='players',
            name='team',
            field=models.CharField(max_length=50, null=True, blank=True),
        ),
    ]
