# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalogue_display', '0026_auto_20150521_1003'),
    ]

    operations = [
        migrations.AddField(
            model_name='filecatalogue',
            name='archive_file',
            field=models.FileField(help_text=b'The created archive file', null=True, upload_to=b'archive_files', blank=True),
            preserve_default=True,
        ),
    ]
