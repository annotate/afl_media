# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalogue_display', '0054_auto_20150928_1128'),
    ]

    operations = [
        # migrations.RemoveField(
        #     model_name='filesincatalogue',
        #     name='commentators',
        # ),
        # migrations.RemoveField(
        #     model_name='filesincatalogue',
        #     name='umpires',
        # ),
        # migrations.RemoveField(
        #     model_name='filesincatalogue',
        #     name='weather',
        # ),
        # migrations.AddField(
        #     model_name='filesincatalogue',
        #     name='evs_commentators',
        #     field=models.ForeignKey(blank=True, to='catalogue_display.EVSCommentators', null=True),
        # ),
        migrations.AddField(
            model_name='filesincatalogue',
            name='evs_umpires',
            field=models.ForeignKey(blank=True, to='catalogue_display.EVSUmpires', null=True),
        ),
        migrations.AddField(
            model_name='filesincatalogue',
            name='evs_weather',
            field=models.ForeignKey(blank=True, to='catalogue_display.EVSWeather', null=True),
        ),
    ]
