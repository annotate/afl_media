# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalogue_display', '0033_auto_20150528_0636'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='directory',
            name='folder_link',
        ),
    ]
