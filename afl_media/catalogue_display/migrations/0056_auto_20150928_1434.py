# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalogue_display', '0055_auto_20150928_1133'),
    ]

    operations = [
        #
        # migrations.RemoveField(
        #     model_name='filesincatalogue',
        #     name='evs_umpires',
        # ),
        # migrations.RemoveField(
        #     model_name='filesincatalogue',
        #     name='evs_commentators',
        # ),
        # migrations.RemoveField(
        #     model_name='filesincatalogue',
        #     name='evs_weather',
        # ),

        migrations.AddField(
            model_name='filesincatalogue',
            name='commentators_list',
            field=models.ManyToManyField(to='catalogue_display.EVSCommentators', null=True, blank=True),
        ),
        migrations.AddField(
            model_name='filesincatalogue',
            name='umpires_list',
            field=models.ManyToManyField(to='catalogue_display.EVSUmpires', null=True, blank=True),
        ),
        migrations.AddField(
            model_name='filesincatalogue',
            name='weather_list',
            field=models.ManyToManyField(to='catalogue_display.EVSWeather', null=True, blank=True),
        ),
    ]
