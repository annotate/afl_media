# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalogue_display', '0034_remove_directory_folder_link'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='filecatalogue',
            name='matched_clip_id_files',
        ),
    ]
