# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalogue_display', '0003_auto_20150508_1602'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='filecatalogue',
            unique_together=set([('name', 'path')]),
        ),
    ]
