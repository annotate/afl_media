# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalogue_display', '0022_auto_20150518_0700'),
    ]

    operations = [
        migrations.AlterField(
            model_name='directory',
            name='folder_link',
            field=models.CharField(max_length=400),
            preserve_default=True,
        ),
    ]
