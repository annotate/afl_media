# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalogue_display', '0027_filecatalogue_archive_file'),
    ]

    operations = [
        migrations.AddField(
            model_name='filecatalogue',
            name='archive_file_status',
            field=models.CharField(default=b'no_attempt_made', max_length=255, choices=[(b'no_attempt_made', b'File has not been processed yet'), (b'created_archive', b'Successfully created InMagic File'), (b'archive_failed_to_create', b'InMagic File could not be created - Error'), (b'no_matching_files_found', b'No matching files found to create InMagic file')]),
            preserve_default=True,
        ),
    ]
