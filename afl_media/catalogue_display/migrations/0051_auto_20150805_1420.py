# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalogue_display', '0050_filecataloguelog_date_file_added'),
    ]

    operations = [
        migrations.CreateModel(
            name='BucketDateList',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('item_name', models.CharField(max_length=255, null=True, blank=True)),
                ('date_file_added', models.DateField(auto_now_add=True, null=True)),
            ],
        ),
        migrations.AlterModelOptions(
            name='filecataloguelog',
            options={'verbose_name': 'Bucket', 'verbose_name_plural': 'Bucket'},
        ),
        migrations.RemoveField(
            model_name='filecataloguelog',
            name='date_file_added',
        ),
    ]
