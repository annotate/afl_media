# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalogue_display', '0004_auto_20150508_1602'),
    ]

    operations = [
        migrations.AddField(
            model_name='filecatalogue',
            name='broadcast',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='filecatalogue',
            name='status',
            field=models.CharField(default=b'new_file', max_length=255, choices=[(b'avid_file', b'Avid File'), (b'log_file', b'EVS Log File'), (b'new_file', b'New'), (b'archived_file', b'Archived'), (b'ignored_file', b'Ignored'), (b'error_file', b'Error')]),
            preserve_default=True,
        ),
    ]
