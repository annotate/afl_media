# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalogue_display', '0036_auto_20150528_1555'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='filecatalogue',
            name='file_type',
        ),
    ]
