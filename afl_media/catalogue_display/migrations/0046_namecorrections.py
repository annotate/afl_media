# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalogue_display', '0045_highlightsyntax'),
    ]

    operations = [
        migrations.CreateModel(
            name='NameCorrections',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('evs_name', models.CharField(max_length=255, null=True, blank=True)),
                ('corrected_name', models.CharField(max_length=255, null=True, blank=True)),
            ],
        ),
    ]
