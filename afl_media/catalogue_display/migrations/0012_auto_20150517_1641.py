# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalogue_display', '0011_auto_20150511_1401'),
    ]

    operations = [
        migrations.AlterField(
            model_name='filecatalogue',
            name='file_type',
            field=models.CharField(blank=True, max_length=255, null=True, choices=[(b'aiff', b'AIFF FILE'), (b'avid', b'Avid file'), (b'xml', b'XML File'), (b'log', b'Log File'), (b'm2v', b'M2V File'), (b'no_extension', b'No extension'), (b'mxf', b'MXF File'), (b'par', b'PAR File'), (b'folder', b'FOLDER')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='filecatalogue',
            name='status',
            field=models.CharField(default=b'new_file', max_length=255, choices=[(b'new_file', b'New'), (b'archived_file', b'Archived'), (b'ignored_file', b'Ignored'), (b'error_file', b'Error'), (b'is_folder', b'Is Folder')]),
            preserve_default=True,
        ),
    ]
