# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalogue_display', '0039_filecatalogue_archiving_notes'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='filecataloguelog',
            name='file_extension',
        ),
        migrations.RemoveField(
            model_name='filecataloguelog',
            name='file_type',
        ),
        migrations.AddField(
            model_name='filecataloguelog',
            name='file_logs',
            field=models.ManyToManyField(help_text=b'The files saved to this dmp file', to='catalogue_display.FileCatalogue', null=True, blank=True),
        ),
    ]
