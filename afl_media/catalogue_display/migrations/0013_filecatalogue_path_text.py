# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalogue_display', '0012_auto_20150517_1641'),
    ]

    operations = [
        migrations.AddField(
            model_name='filecatalogue',
            name='path_text',
            field=models.TextField(help_text=b'The full path of the file', null=True, blank=True),
            preserve_default=True,
        ),
    ]
