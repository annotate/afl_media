# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalogue_display', '0041_auto_20150602_1546'),
    ]

    operations = [
        migrations.AddField(
            model_name='filecataloguelog',
            name='folders',
            field=models.ManyToManyField(help_text=b'The folders selected to be saved to this dmp file', to='catalogue_display.Directory', null=True, blank=True),
        ),
    ]
