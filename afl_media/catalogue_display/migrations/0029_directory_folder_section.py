# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalogue_display', '0028_filecatalogue_archive_file_status'),
    ]

    operations = [
        migrations.AddField(
            model_name='directory',
            name='folder_section',
            field=models.CharField(default=b'SDNA_FOLDER', max_length=50, choices=[(b'SDNA_FOLDER', b'SDNA Catalogue Path'), (b'evs_logs', b'EVS log Path')]),
            preserve_default=True,
        ),
    ]
