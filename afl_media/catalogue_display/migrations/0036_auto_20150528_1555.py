# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalogue_display', '0035_remove_filecatalogue_matched_clip_id_files'),
    ]

    operations = [
        migrations.RenameField(
            model_name='filecatalogue',
            old_name='path_text',
            new_name='full_path_text',
        ),
        migrations.RemoveField(
            model_name='directory',
            name='path',
        ),
        migrations.RemoveField(
            model_name='filecatalogue',
            name='path',
        ),
    ]
