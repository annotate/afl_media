# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalogue_display', '0044_players_team'),
    ]

    operations = [
        migrations.CreateModel(
            name='HighlightSyntax',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('highlight', models.CharField(max_length=255, null=True, blank=True)),
                ('syntax', models.CharField(max_length=255, null=True, blank=True)),
            ],
        ),
    ]
