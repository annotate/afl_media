# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalogue_display', '0005_auto_20150511_0551'),
    ]

    operations = [
        migrations.AddField(
            model_name='filecatalogue',
            name='file_type',
            field=models.CharField(help_text=b'The type of file it is.', max_length=255, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='filecatalogue',
            name='status',
            field=models.CharField(default=b'new_file', max_length=255, choices=[(b'new_file', b'New'), (b'archived_file', b'Archived'), (b'ignored_file', b'Ignored'), (b'error_file', b'Error')]),
            preserve_default=True,
        ),
    ]
