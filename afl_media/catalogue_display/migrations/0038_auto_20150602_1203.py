# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalogue_display', '0037_remove_filecatalogue_file_type'),
    ]

    operations = [
        migrations.AlterField(
            model_name='directory',
            name='folder_section',
            field=models.CharField(default=b'sdna_folder', max_length=50, choices=[(b'top_level', b'Top level'), (b'sdna_folder', b'SDNA Folder'), (b'evs_logs', b'EVS log Path'), (b'avid_path', b'Avid Path')]),
        ),
        migrations.AlterField(
            model_name='filecatalogue',
            name='folder_section',
            field=models.CharField(default=b'sdna_folder', max_length=255, choices=[(b'sdna_folder', b'SDNA folder'), (b'avid_path', b'Avid Path'), (b'evs_folder', b'EVS folder')]),
        ),
    ]
