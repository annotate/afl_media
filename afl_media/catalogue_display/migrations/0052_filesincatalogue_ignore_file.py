# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalogue_display', '0051_auto_20150805_1420'),
    ]

    operations = [
        migrations.AddField(
            model_name='filesincatalogue',
            name='ignore_file',
            field=models.BooleanField(default=False),
        ),
    ]
