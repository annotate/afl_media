# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalogue_display', '0009_filecatalogue_matched_log_file'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='filecatalogue',
            name='file_extension',
        ),
        migrations.AlterField(
            model_name='filecatalogue',
            name='file_type',
            field=models.CharField(blank=True, max_length=255, null=True, choices=[(b'aiff', b'AIFF FILE'), (b'avid', b'Avid file'), (b'xml', b'XML File'), (b'log', b'Log File'), (b'm2v', b'M2V File')]),
            preserve_default=True,
        ),
    ]
