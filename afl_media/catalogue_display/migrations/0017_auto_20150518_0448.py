# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalogue_display', '0016_filecatalogue_folder_section'),
    ]

    operations = [
        migrations.AlterField(
            model_name='filecatalogue',
            name='file_type',
            field=models.CharField(blank=True, max_length=255, null=True, choices=[(b'aiff', b'AIFF FILE'), (b'mov', b'MOV FILE'), (b'avid', b'Avid file'), (b'xml', b'XML File'), (b'log', b'Log File'), (b'm2v', b'M2V File'), (b'no_extension', b'No extension'), (b'mxf', b'MXF File'), (b'par', b'PAR File'), (b'folder', b'FOLDER')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='filecatalogue',
            name='folder_section',
            field=models.CharField(default=b'metadata_folder', max_length=255, choices=[(b'metadata_folder', b'Metadata folder'), (b'one_folder', b'1 folder'), (b'evs_folder', b'EVS folder')]),
            preserve_default=True,
        ),
    ]
