# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalogue_display', '0002_auto_20150508_1602'),
    ]

    operations = [
        migrations.AlterField(
            model_name='filecatalogue',
            name='path',
            field=models.CharField(help_text=b'The path of the file', max_length=255, null=True, blank=True),
            preserve_default=True,
        ),
    ]
