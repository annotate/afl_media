# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalogue_display', '0018_filecatalogue_folder_link'),
    ]

    operations = [
        migrations.AddField(
            model_name='filecatalogue',
            name='parent_link',
            field=models.CharField(max_length=255, null=True, blank=True),
            preserve_default=True,
        ),
    ]
