from django.contrib import admin

from .models import *

class FileCatalogueAdmin(admin.ModelAdmin):
    model = FilesInCatalogue

    list_display = ('id', 'name',  'tape_id', 'clip_id', 'folder_section', 'has_matched_clip_ids', 'status')
    list_filter = ('matched_log_file', 'folder_section', 'status',)
    search_fields = ('id', 'name', 'clip_id', 'tape_id','full_path_text')

    raw_id_fields = ('matched_log_file', 'modified_by', 'directory')
    actions = ('update_status',)

    def update_status(self, request, queryset):

        for line in queryset:
            line.status = FilesInCatalogue.NEW_FILE
            line.save()

    update_status.short_description = "Set Status to New File"


class DirectoryAdmin(admin.ModelAdmin):
    model = Directories
    list_display = ('id', 'name', 'parent', 'folder_section')
    list_filter = ('name', 'folder_section', 'parent__name', 'folder_and_children_done')
    search_fields = ('id', 'name', 'full_path_text')

    raw_id_fields = ('parent', )

    actions = ('update_status',)

    def update_status(self, request, queryset):

        for line in queryset:
            line.folder_and_children_done = False
            line.save()

    update_status.short_description = "Set Directory to New"


class FileCatalogueSetAdmin(admin.ModelAdmin):
    model = FileCatalogueSet

    def has_add_permission(self, request):
        if self.get_queryset(request):
            return False
        return True


class FileCatalogueLogAdmin(admin.ModelAdmin):
    model = FileCatalogueLog
    list_display = ('id', 'status')
    readonly_fields = ('folders', 'file_logs')


class NameCorrectionsAdmin(admin.ModelAdmin):
    model = NameCorrections
    search_fields = ('evs_name', 'corrected_name')
    list_display = ('id', 'evs_name', 'corrected_name')


class PlayersAdmin(admin.ModelAdmin):
    model = NameCorrections
    search_fields = ('number', 'initial', 'last_name', 'first_name', 'team')
    list_display = ('id', 'number', 'initial', 'last_name', 'first_name', 'team')

class HighlightSyntaxAdmin(admin.ModelAdmin):
    model = HighlightSyntax
    search_fields = ('highlight', 'syntax')
    list_display = ('id', 'highlight', 'syntax')


# admin.site.register(FileCatalogueLog, FileCatalogueLogAdmin)
admin.site.register(FilesInCatalogue, FileCatalogueAdmin)
admin.site.register(Directories, DirectoryAdmin)
admin.site.register(FileCatalogueSet, FileCatalogueSetAdmin)
admin.site.register(NameCorrections, NameCorrectionsAdmin)
admin.site.register(HighlightSyntax, HighlightSyntaxAdmin)
admin.site.register(Players, PlayersAdmin)
admin.site.register(EVSUmpires)
admin.site.register(EVSCommentators)
admin.site.register(EVSWeather)

# admin.site.register(BucketDateList)