# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('contenttypes', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='WorkflowLogEntry',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name=b'Date created')),
                ('modified', models.DateTimeField(auto_now=True, verbose_name=b'Date last modified')),
                ('object_id', models.PositiveIntegerField()),
                ('workflow_name', models.CharField(max_length=255, null=True, blank=True)),
                ('old_state', models.CharField(max_length=255, null=True, blank=True)),
                ('old_state_display', models.CharField(max_length=255, null=True, blank=True)),
                ('new_state', models.CharField(max_length=255)),
                ('new_state_display', models.CharField(max_length=255)),
                ('new_state_class', models.CharField(max_length=255, null=True, blank=True)),
                ('comment', models.TextField(null=True, blank=True)),
                ('content_type', models.ForeignKey(to='contenttypes.ContentType')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, blank=True, to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
                'ordering': ('created', 'pk'),
            },
            bases=(models.Model,),
        ),
    ]
