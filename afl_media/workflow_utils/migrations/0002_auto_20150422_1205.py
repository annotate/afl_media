# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('workflow_utils', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='workflowlogentry',
            options={'ordering': ('created', 'pk'), 'verbose_name_plural': 'Workflow Log Entries'},
        ),
    ]
