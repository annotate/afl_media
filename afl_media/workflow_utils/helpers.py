from functools import wraps
import re
from django.utils.datastructures import SortedDict as OrderedDict
from django.core.exceptions import ValidationError

try:
    from django.utils import six
except ImportError:
    import six

__all__ = ["WorkflowStates", "WorkflowState", "Workflow", "sources"]


### Support Functionality (Not part of public API ###

class Labels(dict):
    def __getattribute__(self, name):
        result = dict.get(self, name, None)
        if result is not None:
            return result
        else:
            raise AttributeError("Label for field %s was not found." % name)
    def __setattr__(self, name, value):
        self[name] = value

### End Support Functionality ###


class WorkflowState(object):
    order = 0

    def __init__(self, label=None, order=None):
        self.value = None
        if order:
            self.order = order
        else:
            WorkflowState.order += 1
            self.order = WorkflowState.order
        self.label = label


class WorkflowStatesMeta(type):
    """
    Metaclass that writes the workflow states class.
    """
    name_clean = re.compile(r"_+")
    def __new__(cls, name, bases, attrs):
        class StaticProp(object):
            def __init__(self, value):
                self.value = value
            def __get__(self, obj, objtype):
                return self.value

        fields = {}
        labels = Labels()
        values = {}
        choices = []

        # Get all the fields from parent classes.
        parents = [b for b in bases if isinstance(b, WorkflowStatesMeta)]
        for kls in parents:
            for field_name in kls._fields:
                fields[field_name] = kls._fields[field_name]

        # Get all the fields from this class.
        for field_name in attrs:
            val = attrs[field_name]
            if isinstance(val, WorkflowState):
                fields[field_name] = val

        fields = OrderedDict(sorted(fields.items(), key=lambda x: x[1].order))

        for field_name in fields:
            val = fields[field_name]
            if isinstance(val, WorkflowState):
                if not val.label is None:
                    label = val.label
                else:
                    label = cls.name_clean.sub(" ", field_name)

                choices.append((field_name, label))
                attrs[field_name] = StaticProp(field_name)
                setattr(labels, field_name, label)
                values[val.value or label] = label
            else:
                choices.append((field_name, val.choices))

        attrs["choices"] = StaticProp(tuple(choices))
        attrs["labels"] = labels
        attrs["values"] = values
        attrs["_fields"] = fields

        return super(WorkflowStatesMeta, cls).__new__(cls, name, bases, attrs)


class WorkflowStates(six.with_metaclass(WorkflowStatesMeta)):
    order = 0
    choices = ()
    labels = Labels()
    values = {}

    @classmethod
    def validator(cls, value):
        if value not in cls.values:
            raise ValidationError('Select a valid choice. %(value)s is not '
                                  'one of the available choices.')


class Workflow(object):
    def __init__(self, obj):
        self.obj = obj


def sources(bound_method, *args):
    """
    Raises an exception if the workflow object is not in one of the states
    described by *args
    """
    if not hasattr(bound_method, '_django_fsm'):
        raise TypeError('%s method is not transition' % bound_method.im_func.__name__)

    meta = bound_method._django_fsm
    im_self = getattr(bound_method, 'im_self', getattr(bound_method, '__self__'))
    current_state = meta.field.get_state(im_self)

    return meta.has_transition(current_state) and (
        not check_conditions or meta.conditions_met(im_self, current_state))


class WorkflowException(Exception):
    pass


class InvalidTransitionException(WorkflowException):
    pass


def sources(*valid_states):
    """
    Raises an exception if the workflow object is not in one of the states
    described by *states
    """
    def inner(func):
        @wraps(func)
        def _check_status(*args, **kwargs):
            current_state = args[0].obj.processing_workflow_state

            if current_state not in valid_states:
                raise InvalidTransitionException("Tried to move out of '%s' using '%s'. Valid source states are %s" % (current_state, func.__name__, valid_states))


        return _check_status

    return inner


def lists_overlap(a, b):
    return bool(set(a) & set(b))
