from django.db import models
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.conf import settings
from django.contrib.auth.models import User


class WorkflowLogEntry(models.Model):
    created = models.DateTimeField(verbose_name="Date created", auto_now_add=True)
    modified = models.DateTimeField(verbose_name="Date last modified", auto_now=True)
    user = models.ForeignKey(User, null=True, blank=True, on_delete=models.PROTECT)

    content_type = models.ForeignKey(ContentType)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')
    workflow_name = models.CharField(max_length=255, null=True, blank=True)

    old_state = models.CharField(max_length=255, null=True, blank=True)
    old_state_display = models.CharField(max_length=255, null=True, blank=True)
    new_state = models.CharField(max_length=255)
    new_state_display = models.CharField(max_length=255)
    new_state_class = models.CharField(max_length=255, null=True, blank=True)
    comment = models.TextField(null=True, blank=True)

    class Meta:
        ordering = ('created', 'pk')
        verbose_name_plural = 'Workflow Log Entries'

    def __unicode__(self):
        return u'%s -> %s' % (self.old_state, self.new_state_display)
