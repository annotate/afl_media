from django.contrib.auth.decorators import login_required
from django_ajax.decorators import ajax
from .models import FileMoveProgram, FilePathDetails, FileProgramRun
import json
import datetime


@ajax
@login_required
def start_running(request):
    # if the request.user is anonymous then this view not proceed

    path_selection = FilePathDetails.objects.filter(status=FilePathDetails.PRODUCTION_STATUS).order_by('-pk')[0]
    new_program = FileMoveProgram(path_selection_for_program=path_selection, started=datetime.datetime.now())
    new_program.save()

    new_program.start_program(request)

    payload = {'success': True,
               'program_id': new_program.pk}

    return json.dumps(payload)
