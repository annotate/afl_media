from afl_media.generic_views import DetailLoggedInView, TemplateView, RedirectView
from .models import FileProgramRun, ProgramFileLineItem, FileMoveProgram
from django.http import HttpResponse
from django.shortcuts import redirect

import csv

def home_redirect(request):
    return redirect("/catalogue/display/")


class FileLogDetailView(DetailLoggedInView):
    template_name = "file_move/show_program_log.html"
    model = FileProgramRun
    context_object_name = "file_program_run"


class FileLogAdminStartView(RedirectView):

    pattern_name = 'show-program-log'

    def get_redirect_url(self, *args, **kwargs):
        if kwargs and kwargs['pk']:
            object = FileMoveProgram.objects.get(current_program_run__id=kwargs['pk'])
            object.start_program(self.request)

        return super(FileLogAdminStartView, self).get_redirect_url(*args, **kwargs)


class FileMoveHome(TemplateView):
    template_name = "file_move/index.html"

    def get_context_data(self, **kwargs):
        context = super(FileMoveHome, self).get_context_data(**kwargs)

        context['programs'] = FileMoveProgram.objects.filter(started__isnull=False).order_by('-pk')

        return context


class StartNewRun(TemplateView):
    template_name = "file_move/start_new_run.html"

    def get_context_data(self, **kwargs):
        context = super(StartNewRun, self).get_context_data(**kwargs)

        return context


class LogDetailsView(DetailLoggedInView):
    model = FileProgramRun
    template_name = "file_move/show_program_log.html"
    context_object_name = 'file_program_run'


class FileLogHistoryHome(TemplateView):
    template_name = "file_move/log_history.html"

    def get_context_data(self, **kwargs):
        context = super(FileLogHistoryHome, self).get_context_data(**kwargs)

        context['programs'] = FileProgramRun.objects.all().order_by('-pk')

        return context


def return_unmatched_mxf_files_csv(request, program_id):
    program = FileProgramRun.objects.get(pk=program_id)

    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="unmatched_mxf_files.csv"'

    writer = csv.writer(response)

    writer.writerow(['File name', 'File location', 'Status'])

    for line in program.program.get_unmatched_mxf_files():

        writer.writerow([line.initial_file, line.initial_file_location, line.get_status_display()])

    return response


def return_unmatched_ts_files_csv(request, program_id):
    program = FileProgramRun.objects.get(pk=program_id)

    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="unmatched_ts_files.csv"'

    writer = csv.writer(response)

    writer.writerow(['File name', 'File location', 'Status'])

    for line in program.program.get_unmatched_low_res_ts_files():

        writer.writerow([line.initial_file, line.initial_file_location, line.get_status_display()])

    return response


def return_completed_files_csv(request, program_id):
    program = FileProgramRun.objects.get(pk=program_id)

    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="unmatched_ts_files.csv"'

    writer = csv.writer(response)

    writer.writerow(['name', 'location', 'final', 'status'])


    for line in program.program.get_completed_files():

        writer.writerow([line.initial_file, line.initial_file_location, line.xml_files_moved_to, line.get_status_display()])

    return response