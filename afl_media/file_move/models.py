import datetime
from django.db import models
from django.contrib.auth.models import User
import os.path
from file_move_program import (start_initial_file_check, start_scan_for_ts_matching_files,
                               start_scan_for_ts_matching_files_high_res, start_scan_for_mxf_matching_files,
                               move_found_xml_files, return_unmatched_xml_files_csv, move_unlinked_ts_and_xml_files)


from .workflows import ProcessingWorkflowMixin
from .exceptions import FileMoveProcessingError

from utils.model_helpers import CreatedModifiedMixin


class ProgramDocumentation(CreatedModifiedMixin, models.Model):

    modified_by = models.ForeignKey(User, null=True, blank=True)
    title = models.CharField(max_length=255)
    help_document = models.FileField(upload_to="help_document")

    def __unicode__(self):
        return self.help_document.name

    class Meta:
        verbose_name_plural = "Program Documentation"


class SelectableYears(CreatedModifiedMixin, models.Model):

    modified_by = models.ForeignKey(User, null=True, blank=True)

    year = models.CharField(max_length=4, null=True, blank=True)
    available_for_clean_edit = models.BooleanField(default=True)

    class Meta:
        verbose_name_plural = 'Selectable Years'

    def __unicode__(self):
        return self.year


class FilePathDetails(CreatedModifiedMixin, models.Model):

    DRAFT_STATUS = 'draft'
    PRODUCTION_STATUS = 'production'

    STATUS_OPTIONS = (
        (DRAFT_STATUS, 'This is used in testing. '),
        (PRODUCTION_STATUS, 'Use this Path Setup for production and cron scheduling'),
    )

    modified_by = models.ForeignKey(User, null=True, blank=True)

    initial_scan_path = models.CharField(max_length=255, default="/", help_text="Set the initial scan path of the root folder of low resolution clips.")

    high_res_file_path = models.CharField(max_length=255, help_text="Set the file path for the root folder of high res clips on Nearline 2.")
    second_high_res_file_path = models.CharField(max_length=255, default="/", help_text="Set the file path for the root folder of high res clips on Nearline 3.")

    low_res_file_path = models.CharField(max_length=255, help_text="Set the file path of the root folder for the copy of the low resolution clips.")
    low_res_file_path_offline = models.CharField(max_length=255, default="/", help_text="Set the file path of the root folder for the copy of the low resolution clips before the age of files date.")

    missing_xml_log_file_path = models.CharField(max_length=255, null=True, blank=True, help_text="Set the file path for where the missing XML files should be saved.")

    age_of_files = models.IntegerField(help_text="Set the last year that we can process low res clips. ie 2013 will mean any file older be moved to the offline folder.")
    age_of_unlinked_files = models.IntegerField(default=14, help_text="Set the number of days that unlinked files can live on the system then move them to an unlinked folder.")

    move_current_year_files = models.BooleanField(default=True, help_text="If set, the current year folders will also be move otherwise ignored.")
    status = models.CharField(max_length=50, choices=STATUS_OPTIONS, default=DRAFT_STATUS, help_text="Set the status of this path setup to identify which to use for the overnight scheduled tasks.")

    class Meta:
        verbose_name_plural = 'Program File Path Details'

    def __unicode__(self):
        return 'Program File Path Details: #%s: (%s)' % (self.pk, self.initial_scan_path)


class FileMoveProgram(CreatedModifiedMixin, ProcessingWorkflowMixin, models.Model):

    PROGRAM_CREATED = 'created'
    STARTED = 'started'
    RUNNING = 'running'
    COMPLETED = 'completed'
    FAILED = 'failed'

    FILE_SCAN_STARTED = 'file_scan_started'
    FILE_SCAN_SUCCESS = 'file_scan_success'
    FILE_SCAN_SUCCESS_WITH_CSV = 'file_scan_success_with_log'

    FILE_MOVED_STARTED = 'file_move_started'
    FILE_MOVED_SUCCESS = 'file_move_success'

    STATUS_CHOICES = (
        (PROGRAM_CREATED, 'Program Created'),
        (STARTED, 'File Processing Started'),
        (RUNNING, 'File Processing In Progress'),
        (COMPLETED, 'File Processing Completed'),
        (FAILED, 'Error Encountered'),

        (FILE_SCAN_STARTED, 'Step 1: File Scan Started.'),
        (FILE_SCAN_SUCCESS, 'Step 1: File Scan Completed.'),
        (FILE_SCAN_SUCCESS_WITH_CSV, 'Step 1: File Scan Completed. CSV Logs Exported'),

        (FILE_MOVED_STARTED, 'Step 2: File Move Started.'),
        (FILE_MOVED_SUCCESS, 'Step 2: File Move Completed.'),

    )

    started_by = models.ForeignKey(User, null=True, blank=True)

    path_selection_for_program = models.ForeignKey(FilePathDetails, verbose_name="Path Selection", null=True, blank=True, help_text="Select the path options for this program.")

    status = models.CharField(max_length=50, default=PROGRAM_CREATED, choices=STATUS_CHOICES, help_text="The current status of the program.")
    status_description = models.CharField(max_length=255, null=True, blank=True)
    started = models.DateTimeField(null=True, blank=True, help_text="The date and time this program started.")
    completed = models.DateTimeField(null=True, blank=True, help_text="The date and time this program completed running.")
    failed = models.DateTimeField(null=True, blank=True, help_text="The date and time this program encountered an error.")
    files_checked = models.IntegerField(default=0, help_text="The number of files this program stepped through.")

    manually_created = models.BooleanField(default=False, help_text="This is set when a user creates this program to run in the admin tool.")
    scheduled_run = models.BooleanField(default=False, help_text="This is set when the program is created by the server.")

    initial_file_path_at_run = models.CharField(max_length=255, null=True, blank=True)
    low_res_file_path_at_run = models.CharField(max_length=255, null=True, blank=True)
    high_res_file_path_at_run = models.CharField(max_length=255, null=True, blank=True)
    second_high_res_file_path_at_run = models.CharField(max_length=255, null=True, blank=True)

    current_program_run = models.ForeignKey('FileProgramRun', null=True, blank=True)

    def __unicode__(self):
        return '%s - %s' % (self.started, self.status)

    def program_log_details(self):
        if self.current_program_run:
            return '<a href="/file/move/log/%s/" target="_blank">Show Latest Log Details (%s)</a>' % (self.current_program_run.pk, self.current_program_run.pk)
    program_log_details.allow_tags=True

    def return_program_log_file(self):
        file_name = 'file_move/logs/program_execution_%s.log' % self.pk
        if os.path.isfile(file_name):
            with open(file_name) as input_file:
                log_lines = []
                for i, line in enumerate(input_file):
                    log_lines.append(line)

            return log_lines

    def run_this_program(self, request):
        new_run = FileProgramRun.objects.create(program=self, date_run=datetime.datetime.now())
        self.current_program_run = new_run
        self.save()

        self.start_program(request)
        return True

    def start_program(self, request=None):

        self.status = self.STARTED
        self.started = datetime.datetime.now()
        self.save()

        user = None
        if request:
            user=request.user

        self.processing_workflow.request_file_validation_process(user=user)

    def set_fail_status(self):
        self.status = self.FAILED
        self.failed = datetime.datetime.now()
        self.save()

    def set_running_status(self):
        self.status = self.RUNNING
        self.save()

    def set_completed_status(self):
        self.status = self.COMPLETED
        self.completed = datetime.datetime.now()
        self.save()

    def set_state(self, state):
        self.status_description = state
        self.save()

    def set_status(self, status):
        self.status = status
        self.save()

    def move_unlinked_ts_and_xml_files(self):
        move_unlinked_ts_and_xml_files(self)

    def get_file_path_details(self):
        if not self.path_selection_for_program:
            try:
                file_path_details = FilePathDetails.objects.all().order_by('-pk')[0]
                return file_path_details
            except:
                return None
        else:
            return self.path_selection_for_program

    def check_scan_path_connection(self):
        file_path_details = self.get_file_path_details()

        if not file_path_details:
            self.set_fail_status()
            raise FileMoveProcessingError('\n'.join('File path details have not been set for initial scan'))
        elif not os.path.isdir(file_path_details.initial_scan_path):
            self.set_fail_status()
            raise FileMoveProcessingError('\n'.join('Initial scan file path does not exist'))
        elif not os.path.isdir(file_path_details.initial_scan_path):
            self.set_fail_status()
            raise FileMoveProcessingError('\n'.join('Initial scan file path does not exist'))
        else:
            self.initial_file_path_at_run = file_path_details.initial_scan_path
            self.save()

    def check_low_res_file_path(self):
        file_path_details = self.get_file_path_details()

        if not file_path_details:
            self.set_fail_status()
            raise FileMoveProcessingError('\n'.join('File path details have not been set for low res files'))
        elif not os.path.isdir(file_path_details.low_res_file_path):
            self.set_fail_status()
            raise FileMoveProcessingError('\n'.join('Low res file path does not exist'))
        elif not os.path.isdir(file_path_details.low_res_file_path):
            self.set_fail_status()
            raise FileMoveProcessingError('\n'.join('Scan file path does not exist'))
        else:
            self.low_res_file_path_at_run = file_path_details.low_res_file_path
            self.save()

    def check_high_res_file_path(self):
        file_path_details = self.get_file_path_details()
        if not file_path_details:
            self.set_fail_status()
            raise FileMoveProcessingError('\n'.join('File path details have not been set for high res files'))
        elif not os.path.isdir(file_path_details.high_res_file_path):
            self.set_fail_status()
            raise FileMoveProcessingError('\n'.join('High res file path does not exist'))

        else:
            self.high_res_file_path_at_run = file_path_details.high_res_file_path
            self.save()

    def check_second_high_res_file_path(self):
        file_path_details = self.get_file_path_details()

        if not file_path_details:
            self.set_fail_status()
            raise FileMoveProcessingError('\n'.join('File path details have not been set for second high res files'))
        elif not os.path.isdir(file_path_details.second_high_res_file_path):
            self.set_fail_status()
            raise FileMoveProcessingError('\n'.join('Second high res file path does not exist'))
        else:
            self.second_high_res_file_path_at_run = file_path_details.second_high_res_file_path
            self.save()

    def start_scan_of_all_files(self):
        self.set_running_status()
        file_path_details = self.get_file_path_details()
        return start_initial_file_check(self, file_path_details)

    def start_scan_for_mxf_matching_files(self):
        return start_scan_for_mxf_matching_files(self)

    def start_scan_for_ts_matching_files(self):
        return start_scan_for_ts_matching_files(self)

    def start_scan_for_ts_matching_files_high_res(self):
        return start_scan_for_ts_matching_files_high_res(self)

    def return_unmatched_xml_files_csv(self):
        return return_unmatched_xml_files_csv(self)

    def move_found_xml_files(self):
        return move_found_xml_files(self)

    def processing_complete(self):
        self.set_completed_status()

    def get_high_res_mxf_files(self):
        line_items = self.current_program_run.line_items.filter(file_type=ProgramFileLineItem.MXF_FILE_TYPE)
        return line_items

    def get_high_res_mxf_files_matched(self):
        line_items = self.current_program_run.line_items.filter(file_type=ProgramFileLineItem.MXF_FILE_TYPE, status=ProgramFileLineItem.MATCHED)
        return line_items

    def get_high_res_mxf_files_matched_count(self):
        return len(self.get_high_res_mxf_files_matched())

    def get_high_res_mxf_files_unmatched(self):
        line_items = self.current_program_run.line_items.filter(file_type=ProgramFileLineItem.MXF_FILE_TYPE).exclude(
                                                                status__in=[ProgramFileLineItem.MATCHED, ProgramFileLineItem.XML_GENERATION_REQUIRED])
        return line_items

    def get_high_res_mxf_files_unmatched_count(self):
        return len(self.get_high_res_mxf_files_unmatched())

    def get_all_low_res_ts_files(self):
        line_items = self.current_program_run.line_items.filter(file_type=ProgramFileLineItem.TS_FILE_TYPE)
        return line_items

    def get_low_res_ts_files(self):
        line_items = self.current_program_run.line_items.filter(file_type=ProgramFileLineItem.TS_FILE_TYPE,
                                                                status=ProgramFileLineItem.MATCHED)
        return line_items

    def get_all_mxf_files(self):
        line_items = self.current_program_run.line_items.filter(file_type=ProgramFileLineItem.MXF_FILE_TYPE)
        return line_items

    def get_unmatched_mxf_files(self):
        line_items = self.current_program_run.line_items.filter(file_type=ProgramFileLineItem.MXF_FILE_TYPE, status=ProgramFileLineItem.XML_GENERATION_REQUIRED_HIGH_RES)
        return line_items

    def get_unmatched_mxf_files_count(self):
        return len(self.get_unmatched_mxf_files())

    def get_unmatched_ts_files(self):
        line_items = self.current_program_run.line_items.filter(file_type=ProgramFileLineItem.TS_FILE_TYPE,
                                                                status=ProgramFileLineItem.XML_GENERATION_REQUIRED).exclude(status=ProgramFileLineItem.XML_GENERATION_REQUIRED_HIGH_RES)
        return line_items

    def get_unmatched_low_res_ts_files(self):
        line_items = self.current_program_run.line_items.filter(file_type=ProgramFileLineItem.TS_FILE_TYPE,
                                                                status=ProgramFileLineItem.XML_GENERATION_REQUIRED)
        return line_items

    def get_unmatched_high_res_ts_files(self):
        line_items = self.current_program_run.line_items.filter(file_type=ProgramFileLineItem.TS_FILE_TYPE,
                                                                status=ProgramFileLineItem.XML_GENERATION_REQUIRED_HIGH_RES)
        return line_items

    def get_unmatched_ts_files_count(self):
        return len(self.get_unmatched_low_res_ts_files())

    def get_completed_files(self):
        line_items = self.current_program_run.line_items.filter(file_type=ProgramFileLineItem.TS_FILE_TYPE,
                                                                status=ProgramFileLineItem.PROCESS_COMPLETE)
        return line_items

    def get_error_files(self):
        line_items = self.current_program_run.line_items.filter(file_type=ProgramFileLineItem.TS_FILE_TYPE,
                                                                status=ProgramFileLineItem.ERROR_MOVING)
        return line_items

    def get_completed_files_count(self):
        return len(self.get_completed_files())


class FileProgramRun(CreatedModifiedMixin, models.Model):

    program = models.ForeignKey(FileMoveProgram, help_text="The program this file check is set to.", related_name='program_run_item')
    date_run = models.DateTimeField(null=True, blank=True, help_text="The date and time this program started.")

    log_file = models.FileField(upload_to="log_files", null=True, blank=True)

    def __unicode__(self):
        return 'Program Run: (%s)' % self.pk

    def show_all_line_items(self):
        return True
    show_all_line_items.allow_tags = True



class ProgramFileLineItem(CreatedModifiedMixin, models.Model):

    SEARCHING_LOW_RES = 'searching_low_res'
    MATCHED_LOW_RES = 'matched_low_res'
    XML_GENERATION_REQUIRED = 'xml_generation_low_res'
    XML_GENERATION_REQUIRED_HIGH_RES = 'xml_generation_high_res'
    XML_GENERATION_REQUIRED_BOTH = 'xml_generation_missing_for_both'
    SEARCHING_HIGH_RES = 'searching_high_res'
    MXF_MATCHED_HIGH_RES = 'mxf_matched_high_res'
    MATCHED = 'matched'
    NOT_MATCHED = 'unmatched'
    ERROR_MOVING = 'error_moving_files'
    PROCESS_COMPLETE = 'completed_for_file'
    UNLINKED_PROCESS_COMPLETE = 'completed_for_file_unlinked'

    XML_GENERATION_REQUIRED_STATUSES = [XML_GENERATION_REQUIRED, XML_GENERATION_REQUIRED_HIGH_RES, XML_GENERATION_REQUIRED_BOTH]

    STATUS_CHOICES = (
        (SEARCHING_LOW_RES, 'File Process Initiated'),
        (MATCHED_LOW_RES, 'Found matching low res file'),
        (XML_GENERATION_REQUIRED, 'Low Res XML generation required'),
        (XML_GENERATION_REQUIRED_HIGH_RES, 'High Res XML generation required'),
        (XML_GENERATION_REQUIRED_BOTH, 'Low Res and High Res XML generation required'),
        (SEARCHING_HIGH_RES, 'Searching for matching high res file'),
        (MXF_MATCHED_HIGH_RES, 'Matched high res XML'),
        (MATCHED, 'Found matching files'),
        (NOT_MATCHED, 'Did not find matching file'),
        (ERROR_MOVING, 'Error in copying files to new location'),
        (PROCESS_COMPLETE, 'Complete for file'),
        (UNLINKED_PROCESS_COMPLETE, 'Complete for file - Moved to Unlinked Folder')
    )

    MXF_FILE_TYPE = 'mxf'
    TS_FILE_TYPE = 'ts'
    XML_FILE_TYPE = 'xml'

    FILE_TYPE_CHOICES = (
        (MXF_FILE_TYPE, 'File type .mxf'),
        (TS_FILE_TYPE, 'File type .ts'),
        (XML_FILE_TYPE, 'File type .xml')
    )

    LOW_RES = 'low_res'
    HIGH_RES = 'high_res'
    UNKNOWN = 'unknown'

    FILE_RES_CHOICES = (
        (LOW_RES, 'Low Res File Type'),
        (HIGH_RES, 'High Res File Type'),
        (UNKNOWN, 'Unknown File Type')
    )

    program = models.ForeignKey(FileProgramRun, help_text="The program this file check is set to.", related_name='line_items')

    initial_file = models.CharField(max_length=255, null=True, blank=True, help_text="The name of the file.")
    initial_file_location = models.CharField(max_length=255, null=True, blank=True, help_text="The location of the file.")
    full_initial_file_location = models.CharField(max_length=255, null=True, blank=True, help_text="The location of the file.")
    file_type = models.CharField(max_length=255, choices=FILE_TYPE_CHOICES, default=TS_FILE_TYPE)

    file_name_without_extensions = models.CharField(max_length=255, null=True, blank=True)

    asset_guid = models.CharField(max_length=255, null=True, blank=True, help_text="The AssetItemGUID value of the XML file.")
    file_res_type = models.CharField(max_length=255, choices=FILE_RES_CHOICES, default=UNKNOWN, help_text="The res of the file.")

    file_year = models.CharField(max_length=10, null=True, blank=True)

    ts_file = models.CharField(max_length=255, null=True, blank=True, help_text="The name of the ts file found to match the mxf.")
    ts_file_location = models.CharField(max_length=255, null=True, blank=True, help_text="The full location of the ts file found to match the mxf.")

    high_res_scan_path = models.CharField(max_length=255, null=True, blank=True, help_text="The name of the low res file found.")

    low_res_xml_file = models.CharField(max_length=255, null=True, blank=True, help_text="The name of the low res file found.")
    low_res_xml_file_location = models.CharField(max_length=255, null=True, blank=True, help_text="The full location of the low res file found.")

    high_res_xml_file = models.CharField(max_length=255, null=True, blank=True, help_text="The name of the high res file being checked.")
    high_res_xml_file_location = models.CharField(max_length=255, null=True, blank=True, help_text="The location of the high res file being checked.")

    xml_files_moved_to = models.CharField(max_length=255, null=True, blank=True, help_text="The path the high res file was moved to")

    status = models.CharField(max_length=50, default=SEARCHING_LOW_RES, choices=STATUS_CHOICES, null=True, blank=True, help_text="The status of the low res file being checked.")

    def __unicode__(self):
        return '%s - %s' % (self.pk, self.program)












