import time
from django.db import models
from django.contrib.contenttypes.fields import GenericRelation
import logging
import reversion

from afl_media.celery import app
from workflow_utils.helpers import WorkflowStates, WorkflowState, Workflow, sources
from workflow_utils.models import WorkflowLogEntry
from .exceptions import FileMoveProcessingError


import logging
import logging.handlers


class States(WorkflowStates):
    unprocessed = WorkflowState('Unprocessed')

    request_file_validation_process = WorkflowState("File validation process requested")

    check_scan_path_init = WorkflowState("Check Scan Path Location Initiated")
    check_scan_path_success = WorkflowState("Check Scan Path Location Successful")
    check_scan_path_fail = WorkflowState("Check Scan Path Location Failed")

    check_low_res_file_path_location_init = WorkflowState("Checking Low Res File Path Location Initiated")
    check_low_res_file_path_location_success = WorkflowState("Checking Low Res File Path Location Successful")
    check_low_res_file_path_location_fail = WorkflowState("Checking Low Res File Path Location Failed")

    check_high_res_file_path_location_init = WorkflowState("Check High Res File Path Location Initiated")
    check_high_res_file_path_location_success = WorkflowState("Check High Res File Path Location Successful")
    check_high_res_file_path_location_fail = WorkflowState("Check High Res File Path Location Failed")

    second_high_res_file_path_location_init = WorkflowState("Check Second High Res Path Location Initiated")
    second_high_res_file_path_location_success = WorkflowState("Check Second High Res Path Location Successful")
    second_high_res_file_path_location_fail = WorkflowState("Check Second High Res File Location Failed")

    scan_all_files_files_init = WorkflowState('Scanning All Files Initiated')
    scan_all_files_files_success = WorkflowState('Scanning All Files Successful')
    scan_all_files_files_fail = WorkflowState('Scanning All Files Failed')

    waiting_for_manual_high_res_xml_creation = WorkflowState('Waiting for High Res Manual XML Creation')
    
    high_res_file_processing_init = WorkflowState('.mxf File Processing Initiated')
    high_res_file_processing_success = WorkflowState('.mxf File Processing Successful')
    high_res_file_processing_fail = WorkflowState('.mxf File Processing Failed')
    
    scan_ts_low_res_files_init = WorkflowState('.ts Low Res Files Scanning Initiated')
    scan_ts_low_res_files_success = WorkflowState('.ts Low Res Files Scanning Successful')
    scan_ts_low_res_files_fail = WorkflowState('.ts Low Res Files Scanning Failed')

    waiting_for_manual_xml_creation = WorkflowState('Waiting for Manual XML Creation')

    ts_high_res_file_processing_init = WorkflowState('.ts High Res File Processing Initiated')
    ts_high_res_file_processing_success = WorkflowState('.ts High Res File Processing Successful')
    ts_high_res_file_processing_fail = WorkflowState('.ts High Res File Processing Failed')

    move_xml_files_init = WorkflowState('Move Found XML Files Initiated')
    move_xml_files_success = WorkflowState('Move Found XML Files Successful')
    move_xml_files_fail = WorkflowState('Move Found XML Files Failed')

    processing_complete = WorkflowState('Processing Complete')

    WAITING_STATES = (
        'scan_all_files_files_fail',
        'waiting_for_manual_high_res_xml_creation',
        'waiting_for_manual_xml_creation',
        'processing_complete',
    )

    ERROR_STATES = (
        'check_low_res_file_path_location_fail',
        'check_high_res_file_path_location_fail',
        'scan_all_files_files_fail',
        'scan_second_high_res_files_fail',
        'file_processing_fail',
        'scan_ts_low_res_files_fail',
        'ts_high_res_file_processing_fail',
        'move_xml_files_fail',
    )

    SUCCESS_STATES = (
        'processing_complete',
    )


class ProcessingWorkflowMixin(models.Model):
    PROCESSING_WORKFLOW_STREAM_ONLINE = 'online'
    PROCESSING_WORKFLOW_STREAM_OFFLINE = 'offline'

    PROCESSING_WORKFLOW_STREAM_CHOICES = (
        (PROCESSING_WORKFLOW_STREAM_ONLINE, 'Online'),
        (PROCESSING_WORKFLOW_STREAM_OFFLINE, 'Switched to Offline Workflow'),
    )

    processing_workflow_stream = models.CharField(max_length=50, choices=PROCESSING_WORKFLOW_STREAM_CHOICES, default=PROCESSING_WORKFLOW_STREAM_ONLINE)
    processing_workflow_state = models.CharField(max_length=50, choices=States.choices, default=States.unprocessed)
    processing_workflow_error = models.TextField(null=True, blank=True)
    processing_workflow_log_entries = GenericRelation(WorkflowLogEntry, related_query_name='file_move_as_processing')

    class Meta:
        abstract = True

    @property
    def processing_workflow(self):
        return ProcessingWorkflow(self)

    @property
    def processing_workflow_is_active(self):
        return self.processing_workflow_state not in States.WAITING_STATES

    @property
    def processing_workflow_moved_offline(self):
        return self.processing_workflow_stream == self.PROCESSING_WORKFLOW_STREAM_OFFLINE

    def logger(self):
        logging.basicConfig(level=logging.INFO,
                    format='%(asctime)s %(levelname)-8s %(message)s',
                    datefmt='%a, %d %b %Y %H:%M:%S',
                    filename='file_move/logs/program_execution_%s.log' % self.pk,
                    filemode='w')

        logger = logging.getLogger(__name__)
        return logger

    def move_processing_workflow_offline(self, comment=None, user=None):
        self.update_processing_workflow_stream(self.PROCESSING_WORKFLOW_STREAM_OFFLINE, comment=comment, user=user)

    def update_processing_workflow_stream(self, stream, comment=None, user=None):
        old_stream = self.processing_workflow_stream
        old_stream_display = self.get_processing_workflow_stream_display()
        self.processing_workflow_stream = stream
        new_stream_display = self.get_processing_workflow_stream_display()
        new_stream_class = 'stream_change'

        self.save()
            
        self.processing_workflow_log_entries.create(
            user=user,
            old_state=old_stream,
            old_state_display=old_stream_display,
            new_state=stream,
            new_state_display=new_stream_display,
            new_state_class=new_stream_class,
            comment=comment,
        )

    def update_processing_workflow_state(self, state, commit=True, comment=None, user=None):
        old_state = self.processing_workflow_state
        old_state_display = self.get_processing_workflow_state_display()
        self.processing_workflow_state = state
        new_state_display = self.get_processing_workflow_state_display()

        self.logger().info(' - %s ' % self.get_processing_workflow_state_display())

        if state in States.ERROR_STATES:
            new_state_class = 'error'
        elif state in States.SUCCESS_STATES:
            new_state_class = 'success'
        else:
            new_state_class = None

        if commit:
            self.save()
            
            self.processing_workflow_log_entries.create(
                user=user,
                old_state=old_state,
                old_state_display=old_state_display,
                new_state=state,
                new_state_display=new_state_display,
                new_state_class=new_state_class,
                comment=comment,
            )

    def log_processing_workflow_error(self, message):
        self.processing_workflow_error = message
        self.save()


class ProcessingWorkflow(Workflow):
    states = States

    @property
    def offline(self):
        return self.obj.processing_workflow_moved_offline

    def set_state(self, state, error=None, comment=None, user=None):
        self.obj.set_state(state)

        if error:
            if isinstance(error, FileMoveProcessingError):
                comment = error.message
            else:
                comment = "Unexpected internal error"

        self.obj.update_processing_workflow_state(state, comment=comment, user=user)

    def log_error(self, exception):

        if isinstance(exception, FileMoveProcessingError):
            # Exceptions of type FileMoveProcessingError should
            # be passed along to the user and not logged
            self.obj.processing_workflow_error = exception.message
        else:
            # All other exceptions should be hidden and the
            # stacktrace passed to Django's exception logging
            # framework.
            self.obj.processing_workflow_error = "Unexpected internal error"

        self.obj.save()

    def move_offline(self, comment=None, user=None):
        self.obj.move_processing_workflow_offline(comment=comment, user=user)

    def request_file_validation_process(self, user=None):
        self.set_state(States.request_file_validation_process, user=user)
        self.check_scan_path(user=user)

    @app.method_task
    @reversion.create_revision()
    def check_scan_path(self, user):
        self.set_state(States.check_scan_path_init, user=user)

        try:
            self.obj.check_scan_path_connection()
        except Exception, e:
            self.set_state(States.check_scan_path_fail, error=e)
            self.log_error(e)
            # process terminates here
        else:
            self.set_state(States.check_scan_path_success)
            self.check_low_res_file_path_location(user=user)

    @app.method_task
    @reversion.create_revision()
    def check_low_res_file_path_location(self, user=None):
        self.set_state(States.check_low_res_file_path_location_init, user=user)
        try:
            self.obj.check_low_res_file_path()
        except Exception, e:
            self.set_state(States.check_low_res_file_path_location_fail, error=e)
            self.log_error(e)
            # process terminates here
        else:
            self.set_state(States.check_low_res_file_path_location_success)
            self.check_high_res_file_path_location(user=user)

    @app.method_task
    @reversion.create_revision()
    def check_high_res_file_path_location(self, user=None):
        self.set_state(States.check_high_res_file_path_location_init, user=user)

        try:
            self.obj.check_high_res_file_path()
        except Exception, e:
            self.set_state(States.check_high_res_file_path_location_fail, error=e)
            self.log_error(e)
            # process terminates here
        else:
            self.set_state(States.check_high_res_file_path_location_success)
            self.second_high_res_file_path_location(user=user)

    @app.method_task
    @reversion.create_revision()
    def second_high_res_file_path_location(self, user=None):
        #Check second high res file path
        self.set_state(States.second_high_res_file_path_location_init, user=user)
        try:
            self.obj.check_second_high_res_file_path()
        except Exception, e:
            self.set_state(States.second_high_res_file_path_location_fail, error=e)
            self.log_error(e)
            # process terminates here
        else:
            self.set_state(States.second_high_res_file_path_location_success)
            self.scan_all_files_file_path_location(user=user)

    @app.method_task
    @reversion.create_revision()
    def scan_all_files_file_path_location(self, user=None):
        self.set_state(States.scan_all_files_files_init, user=user)

        try:
            self.obj.start_scan_of_all_files()
        except Exception, e:
            self.set_state(States.scan_all_files_files_fail, error=e)
            self.log_error(e)
            # process terminates here
        else:
            self.set_state(States.scan_all_files_files_success)
            self.scan_for_mxf_matching_files(user=user)

    @app.method_task
    @reversion.create_revision()
    def scan_for_mxf_matching_files(self, user=None):
        self.set_state(States.high_res_file_processing_init, user=user)
        self.obj.set_status('file_scan_started')

        try:
            success = self.obj.start_scan_for_mxf_matching_files()
        except Exception, e:
            self.set_state(States.high_res_file_processing_fail, error=e)
            self.log_error(e)
            # process terminates here
        else:
            if success:
                self.set_state(States.high_res_file_processing_success)
            else:
                self.set_state(States.waiting_for_manual_xml_creation)

        self.start_scan_for_ts_matching_files(user=user)

    @app.method_task
    @reversion.create_revision()
    def start_scan_for_ts_matching_files(self, user=None):
        self.set_state(States.scan_ts_low_res_files_init, user=user)
        self.obj.set_status('file_scan_started')

        try:
            success = self.obj.start_scan_for_ts_matching_files()
        except Exception, e:
            self.set_state(States.scan_ts_low_res_files_fail, error=e)
            self.log_error(e)
            # process terminates here
        else:
            if success:
                self.set_state(States.scan_ts_low_res_files_success)


            self.start_scan_for_ts_matching_files_high_res(user=user, low_res_success=success)

    @app.method_task
    @reversion.create_revision()
    def start_scan_for_ts_matching_files_high_res(self, user=None, low_res_success=None):
        self.set_state(States.ts_high_res_file_processing_init, user=user)
        self.obj.set_status('file_scan_started')

        try:
            success = self.obj.start_scan_for_ts_matching_files_high_res()
        except Exception, e:
            self.set_state(States.ts_high_res_file_processing_fail, error=e)
            self.log_error(e)
            # process terminates here
        else:
            self.obj.set_status('file_scan_success')
            self.obj.return_unmatched_xml_files_csv()

            if success and low_res_success:
                self.obj.set_status('file_scan_success_with_log')
                self.set_state(States.ts_high_res_file_processing_success)
            else:
                self.set_state(States.waiting_for_manual_xml_creation)

    @app.method_task
    @reversion.create_revision()
    def move_xml_files(self, user=None):
        self.set_state(States.move_xml_files_init)
        self.obj.set_status('file_move_started')

        try:
            success = self.obj.move_found_xml_files()
        except Exception, e:
            self.set_state(States.move_xml_files_fail, error=e)
            self.log_error(e)
            # process terminates here
        else:
            if success:
                self.obj.set_status('file_move_success')
                self.set_state(States.move_xml_files_success)
                self.finalise_processing(user=user)
            else:
                self.set_state(States.move_xml_files_fail)
                # process terminates here

    @app.method_task
    @reversion.create_revision()
    def finalise_processing(self, user=None):
        self.obj.move_unlinked_ts_and_xml_files()
        self.obj.processing_complete()
        self.set_state(States.processing_complete)



