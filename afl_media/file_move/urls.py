from django.conf.urls import patterns, url
from .views import FileLogDetailView, FileMoveHome, LogDetailsView, FileLogHistoryHome, StartNewRun, \
                   FileLogAdminStartView

urlpatterns = patterns('',
    url(r'^log/(?P<pk>\d+)/$', FileLogDetailView.as_view(), name='show-program-log'),
    url(r'^log/(?P<pk>\d+)/view/run/$', FileLogAdminStartView.as_view(), name='start-admin-program-log'),
    url(r'^log/view/(?P<pk>\d+)/$', LogDetailsView.as_view(), name='file-log-view'),
    url(r'^$', FileMoveHome.as_view(), name='file-move-home'),
    url(r'^log/history/$', FileLogHistoryHome.as_view(), name='file-log-history'),
    url(r'^start/new/run/$', StartNewRun.as_view(), name='start-new-run'),
    url(r'^start_running/$', 'file_move.ajax.start_running', name='start_running'),



    url(r'^log/csv/mxf/(?P<program_id>\d+)/$', 'file_move.views.return_unmatched_mxf_files_csv', name='csv-file-unmatched-mxf'),
    url(r'^log/csv/ts/(?P<program_id>\d+)/$', 'file_move.views.return_unmatched_ts_files_csv', name='csv-file-unmatched-ts'),
    url(r'^log/csv/completed/(?P<program_id>\d+)/$', 'file_move.views.return_completed_files_csv', name='csv-file-completed'),
)