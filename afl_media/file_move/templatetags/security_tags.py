from django import template



register = template.Library()


@register.filter(name='can_be_deleted_by')
def can_be_deleted_by(obj, user):
    """
    Tests to see if a user can delete an object

    Argument: The user to test
    """
    return obj.can_be_deleted_by(user)


@register.filter(name='can_be_edited_by')
def can_be_edited_by(obj, user):
    """
    Tests to see if a user can edit an object

    Argument: The user to test
    """
    return obj.can_be_edited_by(user)


@register.filter(name='can_add_sites_to')
def can_add_sites_to(user, organisation):
    """
    Tests to see if a user can add sites to a given organisation

    Argument: The organisation to test
    """
    return user.can_add_sites_to(organisation)


@register.filter(name='can_add_divisions_to')
def can_add_divisions_to(user, organisation):
    """
    Tests to see if a user can add divisions to a given organisation

    Argument: The organisation to test
    """
    return user.can_add_divisions_to(organisation)


@register.filter(name='can_add_users_to')
def can_add_users_to(user, organisation):
    """
    Tests to see if a user can add users to a given organisation

    Argument: The organisation to test
    """
    return user.can_add_users_to(organisation)
