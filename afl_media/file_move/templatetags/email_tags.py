import re
from django import template



register = template.Library()


@register.filter
def trim_email_subject(val):
    # Strips out a leading [tag]
    # i.e. "[RISE] Blarg" becomes "Blarg"
    return re.sub(r'\[[\w\s]*\]\s*', '', val)
