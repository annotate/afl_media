from django import template



register = template.Library()


@register.filter
def is_none(val):
    return val is None
