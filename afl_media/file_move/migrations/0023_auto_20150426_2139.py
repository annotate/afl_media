# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('file_move', '0022_filepathdetails_matching_file_path'),
    ]

    operations = [
        migrations.AddField(
            model_name='filemoveprogram',
            name='path_companation_selection',
            field=models.ForeignKey(blank=True, to='file_move.FilePathDetails', help_text=b'Select the path options for this program.', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='filelineitem',
            name='status',
            field=models.CharField(default=b'searching', choices=[(b'searching', b'Searching for matching file'), (b'matched', b'Found matching file'), (b'unmatched', b'Did not find matching file'), (b'error_in_move', b'Error in moving found files')], max_length=50, blank=True, help_text=b'The status of the low res file being checked.', null=True),
            preserve_default=True,
        ),
    ]
