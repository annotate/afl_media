# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('file_move', '0027_programfilelineitem_file_year'),
    ]

    operations = [
        migrations.AlterField(
            model_name='filemoveprogram',
            name='processing_workflow_state',
            field=models.CharField(default=b'unprocessed', max_length=50, choices=[(b'unprocessed', b'Unprocessed'), (b'request_file_validation_process', b'File validation process requested'), (b'check_low_res_file_path_location_init', b'Checking Low Res File Path Location Initiated'), (b'check_low_res_file_path_location_success', b'Checking Low Res File Path Location Successful'), (b'check_low_res_file_path_location_fail', b'Checking Low Res File Path Location Failed'), (b'check_high_res_file_path_location_init', b'Check High Res File Path Location Initiated'), (b'check_high_res_file_path_location_success', b'Check High Res File Path Location Successful'), (b'check_high_res_file_path_location_fail', b'Check High Res File Path Location Failed'), (b'second_high_res_file_path_location_init', b'Check Second High Res Path Location Initiated'), (b'second_high_res_file_path_location_success', b'Check Second High Res Path Location Successful'), (b'second_high_res_file_path_location_fail', b'Check Second High Res File Location Failed'), (b'scan_all_files_files_init', b'Scanning All Files Initiated'), (b'scan_all_files_files_success', b'Scanning All Files Successful'), (b'scan_all_files_files_fail', b'Scanning All Files Failed'), (b'waiting_for_manual_high_res_xml_creation', b'Waiting for High Res Manual XML Creation'), (b'high_res_xml_files_created_success', b'High Res XML files created successfully'), (b'high_res_file_processing_init', b'High Res File Processing Initiated'), (b'high_res_file_processing_success', b'High Res File Processing Successful'), (b'high_res_file_processing_fail', b'High Res File Processing Failed'), (b'scan_low_res_files_init', b'Scanning Low Res Files Initiated'), (b'scan_low_res_files_success', b'Scanning Low Res Files Successful'), (b'scan_low_res_files_fail', b'Scanning Low Res Files Failed'), (b'waiting_for_manual_low_res_xml_creation', b'Waiting for Low Res Manual XML Creation'), (b'low_res_xml_files_created_success', b'Low Res XML files created successfully'), (b'ts_high_res_file_processing_init', b'.ts High Res File Processing Initiated'), (b'ts_high_res_file_processing_success', b'.ts High Res File Processing Successful'), (b'ts_high_res_file_processing_fail', b'.ts High Res File Processing Failed'), (b'move_xml_files_init', b'Move Found XML Files Initiated'), (b'move_xml_files_success', b'Move Found XML Files Successful'), (b'move_xml_files_fail', b'Move Found XML Files Failed'), (b'processing_complete', b'Processing Complete')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='programfilelineitem',
            name='program',
            field=models.ForeignKey(related_name='line_items', to='file_move.FileProgramRun', help_text=b'The program this file check is set to.'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='programfilelineitem',
            name='status',
            field=models.CharField(default=b'searching_low_res', choices=[(b'searching_low_res', b'Searching for matching low res file'), (b'matched_low_res', b'Found matching low res file'), (b'xml_generation_low_res', b'XML generation required'), (b'searching_high_res', b'Searching for matching high res file'), (b'matched', b'Found matching files'), (b'unmatched', b'Did not find matching file'), (b'error_moving_files', b'Error in copying files to new location')], max_length=50, blank=True, help_text=b'The status of the low res file being checked.', null=True),
            preserve_default=True,
        ),
    ]
