# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('file_move', '0031_auto_20150505_1904'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='filepathdetails',
            name='matching_file_path',
        ),
        migrations.AlterField(
            model_name='filepathdetails',
            name='low_res_file_path',
            field=models.CharField(help_text=b'Set the file path of the root folder for the copy of the low resolution clips.', max_length=255),
            preserve_default=True,
        ),
    ]
