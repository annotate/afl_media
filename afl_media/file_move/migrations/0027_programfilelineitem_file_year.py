# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('file_move', '0026_auto_20150427_1518'),
    ]

    operations = [
        migrations.AddField(
            model_name='programfilelineitem',
            name='file_year',
            field=models.CharField(max_length=10, null=True, blank=True),
            preserve_default=True,
        ),
    ]
