# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('file_move', '0011_auto_20150421_1132'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='filelineitem',
            name='current_program_step',
        ),
        migrations.RemoveField(
            model_name='filemoveprogram',
            name='current_program_step',
        ),
        migrations.AddField(
            model_name='filemoveprogram',
            name='high_res_file_path_at_run',
            field=models.CharField(max_length=255, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='filemoveprogram',
            name='low_res_file_path_at_run',
            field=models.CharField(max_length=255, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='filemoveprogram',
            name='processing_workflow_error',
            field=models.TextField(null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='filemoveprogram',
            name='processing_workflow_state',
            field=models.CharField(default=b'unprocessed', max_length=50, choices=[(b'unprocessed', b'Unprocessed'), (b'check_low_res_file_path_location_initiated', b'Checking Low Res File Path Location Initiated'), (b'check_low_res_file_path_location_successful', b'Checking Low Res File Path Location Successful'), (b'check_low_res_file_path_location_failed', b'Checking Low Res File Path Location Failed'), (b'check_high_res_file_path_location_initiated', b'Checking High Res File Path Location Initiated'), (b'check_high_res_file_path_location_successful', b'Checking High Res File Path Location Successful'), (b'check_high_res_file_path_location_failed', b'Checking High Res File Path Location Failed'), (b'scan_high_res_requested', b'Scan High Res Files Requested'), (b'scan_high_res_files_initiated', b'Scanning High Res Files Initiated'), (b'scan_high_res_files_successful', b'Scanning High Res Files Successful'), (b'scan_high_res_files_failed', b'Scanning High Res Files Failed'), (b'waiting_for_manual_high_res_xml_creation', b'Waiting for High Res Manual XML Creation'), (b'high_res_xml_files_created_successfully', b'High Res XML files created successfully'), (b'high_res_file_processing_initiated', b'High Res File Processing Initiated'), (b'high_res_file_processing_successful', b'High Res File Processing Successful'), (b'high_res_file_processing_failed', b'High Res File Processing Failed'), (b'scan_low_res_files_requested', b'Scanning Low Res Files Initiated'), (b'scan_low_res_files_successful', b'Scanning Low Res Files Successful'), (b'scan_low_res_files_failed', b'Scanning Low Res Files Failed'), (b'waiting_for_manual_low_res_xml_creation', b'Waiting for Low Res Manual XML Creation'), (b'low_res_xml_files_created_successfully', b'Low Res XML files created successfully'), (b'low_res_file_processing_initiated', b'Low Res File Processing Initiated'), (b'low_res_file_processing_successful', b'Low Res File Processing Successful'), (b'low_res_file_processing_failed', b'Low Res File Processing Failed'), (b'scan_xml_files_initiated', b'Scan XML files For Matching Clip Initiated'), (b'scan_xml_files_successful', b'Scan XML files For Matching Clip Successful'), (b'scan_xml_files_failed', b'Scan XML files For Matching Clip Failed'), (b'processing_complete', b'Processing Complete')]),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='filemoveprogram',
            name='processing_workflow_stream',
            field=models.CharField(default=b'online', max_length=50, choices=[(b'online', b'Online'), (b'offline', b'Switched to Offline Workflow')]),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='filemoveprogram',
            name='second_high_res_file_path_at_run',
            field=models.CharField(max_length=255, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='filelineitem',
            name='program',
            field=models.ForeignKey(related_name='line_items', to='file_move.FileMoveProgram', help_text=b'The program this file check is set to.'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='filemoveprogram',
            name='status',
            field=models.CharField(default=b'created', help_text=b'The current status of the program.', max_length=50, choices=[(b'created', b'Program Created'), (b'started', b'File Processing Started'), (b'running', b'File Processing In Progress'), (b'completed', b'File Processing Completed'), (b'failed', b'Error Encountered')]),
            preserve_default=True,
        ),
    ]
