# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('file_move', '0023_auto_20150426_2139'),
    ]

    operations = [
        migrations.RenameField(
            model_name='filemoveprogram',
            old_name='path_companation_selection',
            new_name='path_selection_for_program',
        ),
    ]
