# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('file_move', '0041_auto_20150520_1032'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='programfilelineitem',
            name='high_res_mxf_file',
        ),
        migrations.RemoveField(
            model_name='programfilelineitem',
            name='high_res_mxf_file_location',
        ),
    ]
