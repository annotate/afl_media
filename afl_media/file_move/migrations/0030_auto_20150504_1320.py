# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('file_move', '0029_auto_20150429_1151'),
    ]

    operations = [
        migrations.AddField(
            model_name='filepathdetails',
            name='initial_scan_path',
            field=models.CharField(help_text=b'Set the initial scan path of the root folder of low resolution clips.', max_length=255, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='filepathdetails',
            name='age_of_files',
            field=models.IntegerField(help_text=b'Set the last year that we can process low res clips. ie 2013 will mean any file older will not be moved. ', max_length=4),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='filepathdetails',
            name='second_high_res_file_path',
            field=models.CharField(default=b'/', max_length=255, null=True, help_text=b'Set the file path for the root folder of high res clips on Nearline 3.', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='programfilelineitem',
            name='status',
            field=models.CharField(default=b'searching_low_res', choices=[(b'searching_low_res', b'Searching for matching low res file'), (b'matched_low_res', b'Found matching low res file'), (b'xml_generation_low_res', b'XML generation required'), (b'searching_high_res', b'Searching for matching high res file'), (b'mxf_matched_high_res', b'Matched high res MXF -> XML file'), (b'matched', b'Found matching files'), (b'unmatched', b'Did not find matching file'), (b'error_moving_files', b'Error in copying files to new location'), (b'completed_for_file', b'Complete for file')], max_length=50, blank=True, help_text=b'The status of the low res file being checked.', null=True),
            preserve_default=True,
        ),
    ]
