# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('file_move', '0045_auto_20150525_1557'),
    ]

    operations = [
        migrations.AddField(
            model_name='filepathdetails',
            name='move_current_year_files',
            field=models.BooleanField(default=True, help_text=b'If set, the current year folders will also be move otherwise ignored.'),
            preserve_default=True,
        ),
    ]
