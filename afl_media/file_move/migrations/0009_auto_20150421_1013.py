# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('file_move', '0008_auto_20150421_1010'),
    ]

    operations = [
        migrations.AlterField(
            model_name='filepathdetails',
            name='age_of_files',
            field=models.IntegerField(help_text=b'Set the earliest year that CleanEdit can access clips from. Eg. 1999. Any clip from this year on will have the low res files moved to the designated root folder, in a subfolder of that year.', max_length=4),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='filepathdetails',
            name='high_res_file_path',
            field=models.CharField(help_text=b'Set the file path for the root folder of high res clips on Nearline 2.', max_length=255),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='filepathdetails',
            name='low_res_file_path',
            field=models.CharField(help_text=b'Set the file path of the root folder of low resolution clips.', max_length=255),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='filepathdetails',
            name='second_high_res_file_path',
            field=models.CharField(help_text=b'Set the file path for the root folder of high res clips on Nearline 3.', max_length=255),
            preserve_default=True,
        ),
    ]
