# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import utils.model_helpers


class Migration(migrations.Migration):

    dependencies = [
        ('file_move', '0006_auto_20150420_1644'),
    ]

    operations = [
        migrations.AlterField(
            model_name='filepathdetails',
            name='high_res_file_path',
            field=utils.model_helpers.DirectoryField(null=True, upload_to=b'', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='filepathdetails',
            name='low_res_file_path',
            field=utils.model_helpers.DirectoryField(null=True, upload_to=b'', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='filepathdetails',
            name='second_high_res_file_path',
            field=utils.model_helpers.DirectoryField(null=True, upload_to=b'', blank=True),
            preserve_default=True,
        ),
    ]
