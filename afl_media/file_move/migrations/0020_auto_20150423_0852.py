# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('file_move', '0019_auto_20150423_0849'),
    ]

    operations = [
        migrations.AddField(
            model_name='filelineitem',
            name='low_res_file_location',
            field=models.CharField(help_text=b'The location of the low res file being checked.', max_length=255, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='filelineitem',
            name='matching_xml_file_location',
            field=models.CharField(help_text=b'The location of the matching XML file.', max_length=255, null=True, blank=True),
            preserve_default=True,
        ),
    ]
