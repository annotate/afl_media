# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('file_move', '0047_auto_20150602_1203'),
    ]

    operations = [
        migrations.AlterField(
            model_name='filemoveprogram',
            name='status',
            field=models.CharField(default=b'created', help_text=b'The current status of the program.', max_length=50, choices=[(b'created', b'Program Created'), (b'started', b'File Processing Started'), (b'running', b'File Processing In Progress'), (b'completed', b'File Processing Completed'), (b'failed', b'Error Encountered'), (b'file_scan_started', b'Step 1: File Scan Started.'), (b'file_scan_success', b'Step 1: File Scan Completed.'), (b'file_scan_success_with_log', b'Step 1: File Scan Completed. CSV Logs Exported'), (b'file_move_started', b'Step 2: File Move Started.'), (b'file_move_success', b'Step 2: File Move Completed.')]),
        ),
    ]
