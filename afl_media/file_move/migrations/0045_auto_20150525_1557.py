# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('file_move', '0044_filepathdetails_age_of_unlinked_files'),
    ]

    operations = [
        migrations.AlterField(
            model_name='programfilelineitem',
            name='status',
            field=models.CharField(default=b'searching_low_res', choices=[(b'searching_low_res', b'File Process Initiated'), (b'matched_low_res', b'Found matching low res file'), (b'xml_generation_low_res', b'Low Res XML generation required'), (b'xml_generation_high_res', b'High Res XML generation required'), (b'xml_generation_missing_for_both', b'Low Res and High Res XML generation required'), (b'searching_high_res', b'Searching for matching high res file'), (b'mxf_matched_high_res', b'Matched high res XML'), (b'matched', b'Found matching files'), (b'unmatched', b'Did not find matching file'), (b'error_moving_files', b'Error in copying files to new location'), (b'completed_for_file', b'Complete for file'), (b'completed_for_file_unlinked', b'Complete for file - Moved to Unlinked Folder')], max_length=50, blank=True, help_text=b'The status of the low res file being checked.', null=True),
            preserve_default=True,
        ),
    ]
