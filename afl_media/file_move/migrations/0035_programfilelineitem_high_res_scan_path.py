# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('file_move', '0034_programfilelineitem_full_initial_file_location'),
    ]

    operations = [
        migrations.AddField(
            model_name='programfilelineitem',
            name='high_res_scan_path',
            field=models.CharField(help_text=b'The name of the low res file found.', max_length=255, null=True, blank=True),
            preserve_default=True,
        ),
    ]
