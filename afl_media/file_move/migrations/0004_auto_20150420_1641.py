# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('file_move', '0003_programdocumentation_title'),
    ]

    operations = [
        migrations.AlterField(
            model_name='filemoveprogram',
            name='status',
            field=models.CharField(default=b'created', max_length=50, choices=[(b'created', b'Program Created'), (b'started', b'File Move Started'), (b'running', b'File Move In Progress'), (b'completed', b'File Move Completed'), (b'failed', b'Error Encountered')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='filepathdetails',
            name='high_res_file_path',
            field=models.FilePathField(max_length=255, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='filepathdetails',
            name='low_res_file_path',
            field=models.FilePathField(max_length=255, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='filepathdetails',
            name='second_high_res_file_path',
            field=models.FilePathField(max_length=255, null=True, blank=True),
            preserve_default=True,
        ),
    ]
