# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('file_move', '0015_auto_20150422_1323'),
    ]

    operations = [
        migrations.AddField(
            model_name='filemoveprogram',
            name='current_program_run',
            field=models.ForeignKey(blank=True, to='file_move.FileProgramRun', null=True),
            preserve_default=True,
        ),
    ]
