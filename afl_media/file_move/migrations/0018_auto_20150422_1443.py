# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('file_move', '0017_highresfilelineitem'),
    ]

    operations = [
        migrations.AddField(
            model_name='fileprogramrun',
            name='log_file',
            field=models.FileField(null=True, upload_to=b'log_files', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='filemoveprogram',
            name='processing_workflow_state',
            field=models.CharField(default=b'unprocessed', max_length=50, choices=[(b'unprocessed', b'Unprocessed'), (b'request_file_validation', b'File validation process requested'), (b'check_low_res_file_path_location_initiated', b'Checking Low Res File Path Location Initiated'), (b'check_low_res_file_path_location_successful', b'Checking Low Res File Path Location Successful'), (b'check_low_res_file_path_location_failed', b'Checking Low Res File Path Location Failed'), (b'check_high_res_file_path_location_initiated', b'Checking High Res File Path Location Initiated'), (b'check_high_res_file_path_location_successful', b'Checking High Res File Path Location Successful'), (b'check_high_res_file_path_location_failed', b'Checking High Res File Path Location Failed'), (b'scan_high_res_requested', b'Scan High Res Files Requested'), (b'scan_high_res_files_initiated', b'Scanning High Res Files Initiated'), (b'scan_high_res_files_successful', b'Scanning High Res Files Successful'), (b'scan_high_res_files_failed', b'Scanning High Res Files Failed'), (b'scan_second_high_res_files_initiated', b'Scanning High Res Files in Second Location Initiated'), (b'scan_second_high_res_files_successful', b'Scanning High Res Files in Second Location Successful'), (b'scan_second_high_res_files_failed', b'Scanning High Res Files in Second Location Failed'), (b'waiting_for_manual_high_res_xml_creation', b'Waiting for High Res Manual XML Creation'), (b'high_res_xml_files_created_successfully', b'High Res XML files created successfully'), (b'high_res_file_processing_initiated', b'High Res File Processing Initiated'), (b'high_res_file_processing_successful', b'High Res File Processing Successful'), (b'high_res_file_processing_failed', b'High Res File Processing Failed'), (b'scan_low_res_files_initiated', b'Scanning Low Res Files Initiated'), (b'scan_low_res_files_successful', b'Scanning Low Res Files Successful'), (b'scan_low_res_files_failed', b'Scanning Low Res Files Failed'), (b'waiting_for_manual_low_res_xml_creation', b'Waiting for Low Res Manual XML Creation'), (b'low_res_xml_files_created_successfully', b'Low Res XML files created successfully'), (b'low_res_file_processing_initiated', b'Low Res File Processing Initiated'), (b'low_res_file_processing_successful', b'Low Res File Processing Successful'), (b'low_res_file_processing_failed', b'Low Res File Processing Failed'), (b'scan_xml_files_initiated', b'Scan XML files For Matching Clip Initiated'), (b'scan_xml_files_successful', b'Scan XML files For Matching Clip Successful'), (b'scan_xml_files_failed', b'Scan XML files For Matching Clip Failed'), (b'processing_complete', b'Processing Complete')]),
            preserve_default=True,
        ),
    ]
