# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('file_move', '0035_programfilelineitem_high_res_scan_path'),
    ]

    operations = [
        migrations.AddField(
            model_name='filepathdetails',
            name='low_res_file_path_offline',
            field=models.CharField(default=b'/', help_text=b'Set the file path of the root folder for the copy of the low resolution clips before the age of files date.', max_length=255),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='filepathdetails',
            name='age_of_files',
            field=models.IntegerField(help_text=b'Set the last year that we can process low res clips. ie 2013 will mean any file older be moved to the offline folder.', max_length=4),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='filepathdetails',
            name='initial_scan_path',
            field=models.CharField(default=b'/', help_text=b'Set the initial scan path of the root folder of low resolution clips.', max_length=255),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='filepathdetails',
            name='second_high_res_file_path',
            field=models.CharField(default=b'/', help_text=b'Set the file path for the root folder of high res clips on Nearline 3.', max_length=255),
            preserve_default=True,
        ),
    ]
