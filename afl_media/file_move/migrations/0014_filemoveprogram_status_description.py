# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('file_move', '0013_auto_20150422_1155'),
    ]

    operations = [
        migrations.AddField(
            model_name='filemoveprogram',
            name='status_description',
            field=models.CharField(max_length=255, null=True, blank=True),
            preserve_default=True,
        ),
    ]
