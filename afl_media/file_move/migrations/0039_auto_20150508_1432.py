# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('file_move', '0038_filepathdetails_missing_xml_log_file_path'),
    ]

    operations = [
        migrations.AlterField(
            model_name='filemoveprogram',
            name='processing_workflow_state',
            field=models.CharField(default=b'unprocessed', max_length=50, choices=[(b'unprocessed', b'Unprocessed'), (b'request_file_validation_process', b'File validation process requested'), (b'check_scan_path_init', b'Check Scan Path Location Initiated'), (b'check_scan_path_success', b'Check Scan Path Location Successful'), (b'check_scan_path_fail', b'Check Scan Path Location Failed'), (b'check_low_res_file_path_location_init', b'Checking Low Res File Path Location Initiated'), (b'check_low_res_file_path_location_success', b'Checking Low Res File Path Location Successful'), (b'check_low_res_file_path_location_fail', b'Checking Low Res File Path Location Failed'), (b'check_high_res_file_path_location_init', b'Check High Res File Path Location Initiated'), (b'check_high_res_file_path_location_success', b'Check High Res File Path Location Successful'), (b'check_high_res_file_path_location_fail', b'Check High Res File Path Location Failed'), (b'second_high_res_file_path_location_init', b'Check Second High Res Path Location Initiated'), (b'second_high_res_file_path_location_success', b'Check Second High Res Path Location Successful'), (b'second_high_res_file_path_location_fail', b'Check Second High Res File Location Failed'), (b'scan_all_files_files_init', b'Scanning All Files Initiated'), (b'scan_all_files_files_success', b'Scanning All Files Successful'), (b'scan_all_files_files_fail', b'Scanning All Files Failed'), (b'waiting_for_manual_high_res_xml_creation', b'Waiting for High Res Manual XML Creation'), (b'high_res_file_processing_init', b'.mxf File Processing Initiated'), (b'high_res_file_processing_success', b'.mxf File Processing Successful'), (b'high_res_file_processing_fail', b'.mxf File Processing Failed'), (b'scan_ts_low_res_files_init', b'.ts Low Res Files Scanning Initiated'), (b'scan_ts_low_res_files_success', b'.ts Low Res Files Scanning Successful'), (b'scan_ts_low_res_files_fail', b'.ts Low Res Files Scanning Failed'), (b'waiting_for_manual_xml_creation', b'Waiting for Manual XML Creation'), (b'ts_high_res_file_processing_init', b'.ts High Res File Processing Initiated'), (b'ts_high_res_file_processing_success', b'.ts High Res File Processing Successful'), (b'ts_high_res_file_processing_fail', b'.ts High Res File Processing Failed'), (b'move_xml_files_init', b'Move Found XML Files Initiated'), (b'move_xml_files_success', b'Move Found XML Files Successful'), (b'move_xml_files_fail', b'Move Found XML Files Failed'), (b'processing_complete', b'Processing Complete')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='programfilelineitem',
            name='status',
            field=models.CharField(default=b'searching_low_res', choices=[(b'searching_low_res', b'Searching for matching low res file'), (b'matched_low_res', b'Found matching low res file'), (b'xml_generation_low_res', b'Low Res XML generation required'), (b'xml_generation_high_res', b'High Res XML generation required'), (b'xml_generation_missing_for_both', b'Low Res and High Res XML generation required'), (b'searching_high_res', b'Searching for matching high res file'), (b'mxf_matched_high_res', b'Matched high res MXF -> XML file'), (b'matched', b'Found matching files'), (b'unmatched', b'Did not find matching file'), (b'error_moving_files', b'Error in copying files to new location'), (b'completed_for_file', b'Complete for file')], max_length=50, blank=True, help_text=b'The status of the low res file being checked.', null=True),
            preserve_default=True,
        ),
    ]
