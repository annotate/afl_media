# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('file_move', '0037_filepathdetails_status'),
    ]

    operations = [
        migrations.AddField(
            model_name='filepathdetails',
            name='missing_xml_log_file_path',
            field=models.CharField(help_text=b'Set the file path for where the missing XML files should be saved.', max_length=255, null=True, blank=True),
            preserve_default=True,
        ),
    ]
