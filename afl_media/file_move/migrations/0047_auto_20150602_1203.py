# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('file_move', '0046_filepathdetails_move_current_year_files'),
    ]

    operations = [
        migrations.AlterField(
            model_name='filepathdetails',
            name='age_of_files',
            field=models.IntegerField(help_text=b'Set the last year that we can process low res clips. ie 2013 will mean any file older be moved to the offline folder.'),
        ),
        migrations.AlterField(
            model_name='filepathdetails',
            name='age_of_unlinked_files',
            field=models.IntegerField(default=14, help_text=b'Set the number of days that unlinked files can live on the system then move them to an unlinked folder.'),
        ),
    ]
