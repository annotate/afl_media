# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('file_move', '0043_programfilelineitem_file_name_without_extensions'),
    ]

    operations = [
        migrations.AddField(
            model_name='filepathdetails',
            name='age_of_unlinked_files',
            field=models.IntegerField(default=14, help_text=b'Set the number of days that unlinked files can live on the system then move them to an unlinked folder.', max_length=2),
            preserve_default=True,
        ),
    ]
