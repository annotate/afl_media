# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('file_move', '0002_auto_20150420_1634'),
    ]

    operations = [
        migrations.AddField(
            model_name='programdocumentation',
            name='title',
            field=models.CharField(default='doc', max_length=255),
            preserve_default=False,
        ),
    ]
