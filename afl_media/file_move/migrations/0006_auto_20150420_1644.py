# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('file_move', '0005_auto_20150420_1642'),
    ]

    operations = [
        migrations.AlterField(
            model_name='filepathdetails',
            name='high_res_file_path',
            field=models.FilePathField(recursive=True, max_length=255, allow_folders=True, blank=True, path=b'/', null=True, match=b'*.*'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='filepathdetails',
            name='low_res_file_path',
            field=models.FilePathField(recursive=True, max_length=255, allow_folders=True, blank=True, path=b'/', null=True, match=b'*.*'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='filepathdetails',
            name='second_high_res_file_path',
            field=models.FilePathField(recursive=True, max_length=255, allow_folders=True, blank=True, path=b'/', null=True, match=b'*.*'),
            preserve_default=True,
        ),
    ]
