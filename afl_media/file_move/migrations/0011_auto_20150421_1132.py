# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('file_move', '0010_auto_20150421_1047'),
    ]

    operations = [
        migrations.AddField(
            model_name='filelineitem',
            name='current_program_step',
            field=models.CharField(default=b'step_one_a_scanning_high_res_files', help_text=b'The current step the program was working on.', max_length=50, choices=[(b'step_one_a_scanning_high_res_files', b'Step One - Scanning high res folder for .mxf files'), (b'step_one_b_scanning_low_res_files_for_ts_files', b'Step One - Scanning low res files for all .ts files and matching .xml'), (b'step_one_waiting_on_manual_xml_creation', b'Step One - Waiting on manual .xml creation of missing files.')]),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='filemoveprogram',
            name='current_program_step',
            field=models.CharField(default=b'step_one_a_scanning_high_res_files', help_text=b'The current step the program is working on.', max_length=50, choices=[(b'step_one_a_scanning_high_res_files', b'Step One - Scanning high res folder for .mxf files'), (b'step_one_b_scanning_low_res_files_for_ts_files', b'Step One - Scanning low res files for all .ts files and matching .xml'), (b'step_one_waiting_on_manual_xml_creation', b'Step One - Waiting on manual .xml creation of missing files.')]),
            preserve_default=True,
        ),
    ]
