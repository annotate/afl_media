# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('file_move', '0021_auto_20150426_1855'),
    ]

    operations = [
        migrations.AddField(
            model_name='filepathdetails',
            name='matching_file_path',
            field=models.CharField(default='/', help_text=b'Set the file path for the root folder where the matching files should be moved to by year', max_length=255),
            preserve_default=False,
        ),
    ]
