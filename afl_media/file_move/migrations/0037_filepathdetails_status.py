# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('file_move', '0036_auto_20150506_1507'),
    ]

    operations = [
        migrations.AddField(
            model_name='filepathdetails',
            name='status',
            field=models.CharField(default=b'draft', help_text=b'Set the status of this path setup to identify which to use for the overnight scheduled tasks.', max_length=50, choices=[(b'draft', b'This is used in testing. '), (b'production', b'Use this Path Setup for production and cron scheduling')]),
            preserve_default=True,
        ),
    ]
