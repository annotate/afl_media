# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('file_move', '0033_auto_20150505_2143'),
    ]

    operations = [
        migrations.AddField(
            model_name='programfilelineitem',
            name='full_initial_file_location',
            field=models.CharField(help_text=b'The location of the file.', max_length=255, null=True, blank=True),
            preserve_default=True,
        ),
    ]
