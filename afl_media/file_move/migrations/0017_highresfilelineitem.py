# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('file_move', '0016_filemoveprogram_current_program_run'),
    ]

    operations = [
        migrations.CreateModel(
            name='HighResFileLineItem',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(help_text=b'The date and time at which the object was entered into the system', verbose_name=b'Date created', auto_now_add=True)),
                ('modified', models.DateTimeField(help_text=b'The date and time at which the object was last modified', verbose_name=b'Date last modified', auto_now=True)),
                ('high_res_file', models.CharField(help_text=b'The name of the high res file being checked.', max_length=255, null=True, blank=True)),
                ('matching_xml_file', models.CharField(help_text=b'The name of the matching XML file.', max_length=255, null=True, blank=True)),
                ('high_res_file_moved_to', models.CharField(help_text=b'The path the high res file was moved to', max_length=255, null=True, blank=True)),
                ('matching_xml_file_moved_to', models.CharField(help_text=b'The path the high res file was moved to.', max_length=255, null=True, blank=True)),
                ('status', models.CharField(default=b'searching', choices=[(b'searching', b'Searching for matching file'), (b'matched', b'Found matching file'), (b'unmatched', b'Did not find matching file')], max_length=50, blank=True, help_text=b'The status of the low res file being checked.', null=True)),
                ('program', models.ForeignKey(related_name='high_res_line_items', to='file_move.FileProgramRun', help_text=b'The program this file check is set to.')),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
    ]
