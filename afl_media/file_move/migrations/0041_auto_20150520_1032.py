# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('file_move', '0040_auto_20150511_0551'),
    ]

    operations = [
        migrations.AddField(
            model_name='programfilelineitem',
            name='high_res_mxf_file',
            field=models.CharField(help_text=b'The name of the high res mxf file.', max_length=255, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='programfilelineitem',
            name='high_res_mxf_file_location',
            field=models.CharField(help_text=b'The location of the high res mxf file.', max_length=255, null=True, blank=True),
            preserve_default=True,
        ),
    ]
