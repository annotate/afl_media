# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('file_move', '0025_auto_20150427_1437'),
    ]

    operations = [
        migrations.AddField(
            model_name='programfilelineitem',
            name='asset_guid',
            field=models.CharField(help_text=b'The AssetItemGUID value of the XML file.', max_length=255, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='programfilelineitem',
            name='file_res_type',
            field=models.CharField(default=b'unknown', help_text=b'The res of the file.', max_length=255, choices=[(b'low_res', b'Low Res File Type'), (b'high_res', b'High Res File Type'), (b'unknown', b'Unknown File Type')]),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='programfilelineitem',
            name='initial_file_location',
            field=models.CharField(help_text=b'The location of the file.', max_length=255, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='filemoveprogram',
            name='processing_workflow_state',
            field=models.CharField(default=b'unprocessed', max_length=50, choices=[(b'unprocessed', b'Unprocessed'), (b'request_file_validation_process', b'File validation process requested'), (b'check_low_res_file_path_location_init', b'Checking Low Res File Path Location Initiated'), (b'check_low_res_file_path_location_success', b'Checking Low Res File Path Location Successful'), (b'check_low_res_file_path_location_fail', b'Checking Low Res File Path Location Failed'), (b'check_high_res_file_path_location_init', b'Check High Res File Path Location Initiated'), (b'check_high_res_file_path_location_success', b'Check High Res File Path Location Successful'), (b'check_high_res_file_path_location_fail', b'Check High Res File Path Location Failed'), (b'second_high_res_file_path_location_init', b'Check Second High Res Path Location Initiated'), (b'second_high_res_file_path_location_success', b'Check Second High Res Path Location Successful'), (b'second_high_res_file_path_location_fail', b'Check Second High Res File Location Failed'), (b'scan_all_files_files_init', b'Scanning All Files Initiated'), (b'scan_all_files_files_success', b'Scanning All Files Successful'), (b'scan_all_files_files_fail', b'Scanning All Files Failed'), (b'waiting_for_manual_high_res_xml_creation', b'Waiting for High Res Manual XML Creation'), (b'high_res_xml_files_created_success', b'High Res XML files created successfully'), (b'high_res_file_processing_init', b'High Res File Processing Initiated'), (b'high_res_file_processing_success', b'High Res File Processing Successful'), (b'high_res_file_processing_fail', b'High Res File Processing Failed'), (b'scan_low_res_files_init', b'Scanning Low Res Files Initiated'), (b'scan_low_res_files_success', b'Scanning Low Res Files Successful'), (b'scan_low_res_files_fail', b'Scanning Low Res Files Failed'), (b'waiting_for_manual_low_res_xml_creation', b'Waiting for Low Res Manual XML Creation'), (b'low_res_xml_files_created_success', b'Low Res XML files created successfully'), (b'low_res_file_processing_init', b'Low Res File Processing Initiated'), (b'low_res_file_processing_success', b'Low Res File Processing Successful'), (b'low_res_file_processing_fail', b'Low Res File Processing Failed'), (b'scan_xml_files_init', b'Scan XML files For Matching Clip Initiated'), (b'scan_xml_files_success', b'Scan XML files For Matching Clip Successful'), (b'scan_xml_files_fail', b'Scan XML files For Matching Clip Failed'), (b'processing_complete', b'Processing Complete')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='programfilelineitem',
            name='file_type',
            field=models.CharField(default=b'ts', max_length=255, choices=[(b'mxf', b'File type .mxf'), (b'ts', b'File type .ts'), (b'xml', b'File type .xml')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='programfilelineitem',
            name='initial_file',
            field=models.CharField(help_text=b'The name of the file.', max_length=255, null=True, blank=True),
            preserve_default=True,
        ),
    ]
