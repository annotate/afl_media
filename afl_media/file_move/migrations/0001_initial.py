# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='FileLineItem',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(help_text=b'The date and time at which the object was entered into the system', verbose_name=b'Date created', auto_now_add=True)),
                ('modified', models.DateTimeField(help_text=b'The date and time at which the object was last modified', verbose_name=b'Date last modified', auto_now=True)),
                ('low_res_file', models.CharField(max_length=255, null=True, blank=True)),
                ('matching_xml_file', models.CharField(max_length=255, null=True, blank=True)),
                ('low_res_file_moved_to', models.CharField(max_length=255, null=True, blank=True)),
                ('matching_xml_file_moved_to', models.CharField(max_length=255, null=True, blank=True)),
                ('status', models.CharField(default=b'searching', max_length=50, null=True, blank=True, choices=[(b'searching', b'Searching for matching file'), (b'matched', b'Found matching file'), (b'unmatched', b'Did not find matching file')])),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='FileMoveProgram',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(help_text=b'The date and time at which the object was entered into the system', verbose_name=b'Date created', auto_now_add=True)),
                ('modified', models.DateTimeField(help_text=b'The date and time at which the object was last modified', verbose_name=b'Date last modified', auto_now=True)),
                ('status', models.CharField(default=b'started', max_length=50, choices=[(b'started', b'File Move Started'), (b'running', b'File Move In Progress'), (b'completed', b'File Move Completed'), (b'failed', b'Error Encountered')])),
                ('started', models.DateTimeField(null=True, blank=True)),
                ('completed', models.DateTimeField(null=True, blank=True)),
                ('failed', models.DateTimeField(null=True, blank=True)),
                ('files_checked', models.IntegerField(default=0)),
                ('started_by', models.ForeignKey(blank=True, to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='FilePathDetails',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(help_text=b'The date and time at which the object was entered into the system', verbose_name=b'Date created', auto_now_add=True)),
                ('modified', models.DateTimeField(help_text=b'The date and time at which the object was last modified', verbose_name=b'Date last modified', auto_now=True)),
                ('low_res_file_path', models.CharField(max_length=255, null=True, blank=True)),
                ('high_res_file_path', models.CharField(max_length=255, null=True, blank=True)),
                ('second_high_res_file_path', models.CharField(max_length=255, null=True, blank=True)),
                ('age_of_files', models.IntegerField(null=True, blank=True)),
                ('modified_by', models.ForeignKey(blank=True, to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='SelectableYears',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(help_text=b'The date and time at which the object was entered into the system', verbose_name=b'Date created', auto_now_add=True)),
                ('modified', models.DateTimeField(help_text=b'The date and time at which the object was last modified', verbose_name=b'Date last modified', auto_now=True)),
                ('year', models.CharField(max_length=4, null=True, blank=True)),
                ('available_for_clean_edit', models.BooleanField(default=True)),
                ('modified_by', models.ForeignKey(blank=True, to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='filelineitem',
            name='program',
            field=models.ForeignKey(to='file_move.FileMoveProgram'),
            preserve_default=True,
        ),
    ]
