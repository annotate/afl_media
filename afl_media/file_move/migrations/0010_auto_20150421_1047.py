# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('file_move', '0009_auto_20150421_1013'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='programdocumentation',
            options={'verbose_name_plural': 'Program Documentation'},
        ),
        migrations.AddField(
            model_name='filemoveprogram',
            name='manually_created',
            field=models.BooleanField(default=False, help_text=b'This is set when a user creates this program to run in the admin tool.'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='filemoveprogram',
            name='scheduled_run',
            field=models.BooleanField(default=False, help_text=b'This is set when the program is created by the server.'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='filelineitem',
            name='low_res_file',
            field=models.CharField(help_text=b'The name of the low res file being checked.', max_length=255, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='filelineitem',
            name='low_res_file_moved_to',
            field=models.CharField(help_text=b'The path the low res file was moved to', max_length=255, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='filelineitem',
            name='matching_xml_file',
            field=models.CharField(help_text=b'The name of the matching XML file.', max_length=255, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='filelineitem',
            name='matching_xml_file_moved_to',
            field=models.CharField(help_text=b'The path the high res file was moved to.', max_length=255, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='filelineitem',
            name='program',
            field=models.ForeignKey(help_text=b'The program this file check is set to.', to='file_move.FileMoveProgram'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='filelineitem',
            name='status',
            field=models.CharField(default=b'searching', choices=[(b'searching', b'Searching for matching file'), (b'matched', b'Found matching file'), (b'unmatched', b'Did not find matching file')], max_length=50, blank=True, help_text=b'The status of the low res file being checked.', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='filemoveprogram',
            name='completed',
            field=models.DateTimeField(help_text=b'The date and time this program completed running.', null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='filemoveprogram',
            name='failed',
            field=models.DateTimeField(help_text=b'The date and time this program encountered an error.', null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='filemoveprogram',
            name='files_checked',
            field=models.IntegerField(default=0, help_text=b'The number of files this program stepped through.'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='filemoveprogram',
            name='started',
            field=models.DateTimeField(help_text=b'The date and time this program started.', null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='filemoveprogram',
            name='status',
            field=models.CharField(default=b'created', help_text=b'The current status of the program.', max_length=50, choices=[(b'created', b'Program Created'), (b'started', b'File Move Started'), (b'running', b'File Move In Progress'), (b'completed', b'File Move Completed'), (b'failed', b'Error Encountered')]),
            preserve_default=True,
        ),
    ]
