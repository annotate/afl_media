# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('file_move', '0042_auto_20150520_1137'),
    ]

    operations = [
        migrations.AddField(
            model_name='programfilelineitem',
            name='file_name_without_extensions',
            field=models.CharField(max_length=255, null=True, blank=True),
            preserve_default=True,
        ),
    ]
