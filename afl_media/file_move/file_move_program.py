import os
import time
import csv
import xml.etree.ElementTree as ET
import shutil
import errno
import datetime


def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as exc: # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else: raise


def check_directory_as_year(directory):
    file_year = None
    sub_directories = directory.split("/")
    for sub in sub_directories:
        try:
            if len(sub) == 4:
                file_year = int(str(sub))
                return file_year
        except:
            pass

    return file_year

def get_file_name_without_extensions(file_name):

    if file_name:
        file_name = file_name.split(".")
        return file_name[0]
    return None

def start_initial_file_check(program, path_details):
    from .models import ProgramFileLineItem

    ProgramFileLineItem.objects.all().delete()

    #we only care about ts and xml in low res folders
    file_types = [".ts", ".xml"]

    #This one will be flat file location
    for fn in os.listdir(path_details.initial_scan_path):

        ext = os.path.splitext(fn)[-1].lower()
        asset_id = None

        if ext in file_types:
            initial_file_location = os.path.join(path_details.initial_scan_path, fn)
            if os.path.isfile(initial_file_location):
                file_type = None

                if ext == ".xml":
                    asset_id = get_asset_guid(initial_file_location)
                    file_type = ProgramFileLineItem.XML_FILE_TYPE
                elif ext == ".ts":
                    file_type = ProgramFileLineItem.TS_FILE_TYPE

                if file_type:
                    file_name = "%s" % fn
                    full_initial_file_location = initial_file_location.replace(file_name, "")
                    ProgramFileLineItem.objects.create(program=program.current_program_run,
                                                       initial_file=file_name,
                                                       full_initial_file_location=full_initial_file_location,
                                                       initial_file_location=initial_file_location,
                                                       file_name_without_extensions=get_file_name_without_extensions(file_name),
                                                       file_type=file_type,
                                                       asset_guid=asset_id,
                                                       file_res_type=ProgramFileLineItem.LOW_RES)


    #we only care about mxf and xml in high res folders
    file_types = [".mxf", ".xml"]
    for directory, sub_directories, files in os.walk(path_details.high_res_file_path):
        for fn in files:
            if os.path.isfile(os.path.join(directory, fn)):

                ext = os.path.splitext(fn)[-1].lower()
                asset_id = None
                if ext in file_types:

                    check_folder_year = check_directory_as_year(directory)
                    initial_file_location = os.path.join(directory, fn)

                    file_type = None
                    if ext == ".xml":
                        asset_id = get_asset_guid(initial_file_location)
                        file_type = ProgramFileLineItem.XML_FILE_TYPE
                    elif ext == ".mxf":
                        file_type = ProgramFileLineItem.MXF_FILE_TYPE

                    file_year = None
                    if check_folder_year:
                        file_year = int(check_folder_year)

                    if file_type:
                        file_name = "%s" % fn
                        full_initial_file_location = initial_file_location.replace(file_name, "")
                        ProgramFileLineItem.objects.create(program=program.current_program_run,
                                                           initial_file=file_name,
                                                           full_initial_file_location=full_initial_file_location,
                                                           initial_file_location=initial_file_location,
                                                           file_name_without_extensions=get_file_name_without_extensions(file_name),
                                                           file_type=file_type,
                                                           file_year=file_year,
                                                           asset_guid=asset_id,
                                                           high_res_scan_path=path_details.high_res_file_path,
                                                           file_res_type=ProgramFileLineItem.HIGH_RES)

    if not path_details.high_res_file_path == path_details.second_high_res_file_path:
        for directory, sub_directories, files in os.walk(path_details.second_high_res_file_path):

            for fn in files:
                if os.path.isfile(os.path.join(directory, fn)):

                    ext = os.path.splitext(fn)[-1].lower()
                    asset_id = None

                    if ext in file_types:
                        initial_file_location = os.path.join(directory, fn)
                        check_folder_year = check_directory_as_year(directory)

                        file_year = None
                        if check_folder_year:
                            file_year = int(check_folder_year)

                        file_type = None

                        if ext == ".xml":
                            asset_id = get_asset_guid(initial_file_location)
                            file_type = ProgramFileLineItem.XML_FILE_TYPE
                        elif ext == ".mxf":
                            file_type = ProgramFileLineItem.MXF_FILE_TYPE

                        if file_type:
                            file_name = "%s" % fn
                            full_initial_file_location = initial_file_location.replace(file_name, "")

                            ProgramFileLineItem.objects.create(program=program.current_program_run,
                                                               initial_file=file_name,
                                                               full_initial_file_location=full_initial_file_location,
                                                               initial_file_location=initial_file_location,
                                                               file_name_without_extensions=get_file_name_without_extensions(file_name),
                                                               file_type=file_type,
                                                               file_year=file_year,
                                                               asset_guid=asset_id,
                                                               high_res_scan_path=path_details.second_high_res_file_path,
                                                               file_res_type=ProgramFileLineItem.HIGH_RES)
    return True


def get_asset_guid(file_path_location):
    tree = ET.parse(file_path_location)
    root = tree.getroot()

    for child in root.iter('AssetItemGUID'):
        asset_id = child.text
        return asset_id


def start_scan_for_mxf_matching_files(program):
    from .models import ProgramFileLineItem

    all_files = ProgramFileLineItem.objects.filter(program=program.current_program_run,
                                                   file_type=ProgramFileLineItem.MXF_FILE_TYPE)
    success = True
    for afl_file in all_files:

        matching_name = afl_file.initial_file.lower()

        if '.evs' in matching_name:
            matching_name = matching_name.replace('.mxf', '.xml')
        else:
            matching_name = matching_name.replace('.mxf', '.evs.xml')

        try:
            matching_xml = ProgramFileLineItem.objects.get(program=program.current_program_run,
                                                           file_type=ProgramFileLineItem.XML_FILE_TYPE,
                                                           file_res_type=ProgramFileLineItem.HIGH_RES,
                                                           full_initial_file_location=afl_file.full_initial_file_location,
                                                           initial_file__iexact=matching_name)

            afl_file.high_res_xml_file = matching_xml.initial_file
            afl_file.status = ProgramFileLineItem.MXF_MATCHED_HIGH_RES
            afl_file.file_year = matching_xml.file_year
            afl_file.high_res_xml_file_location = matching_xml.initial_file_location
            afl_file.save()

            matching_xml.status = ProgramFileLineItem.MXF_MATCHED_HIGH_RES
            matching_xml.save()
        except:
            success = False
            afl_file.status = ProgramFileLineItem.XML_GENERATION_REQUIRED_HIGH_RES
            afl_file.save()

    return success


def start_scan_for_ts_matching_files(program):
    from .models import ProgramFileLineItem

    all_files = ProgramFileLineItem.objects.filter(program=program.current_program_run,
                                                   file_type=ProgramFileLineItem.TS_FILE_TYPE)
    success = True
    for afl_file in all_files:

        matching_name = afl_file.initial_file.lower()

        if '.evs' in matching_name:
            matching_name = matching_name.replace('.ts', '.xml')
        else:
            matching_name = matching_name.replace('.ts', '.evs.xml')

        try:

            matching_xml = ProgramFileLineItem.objects.get(program=program.current_program_run,
                                                           file_type=ProgramFileLineItem.XML_FILE_TYPE,
                                                           file_res_type=ProgramFileLineItem.LOW_RES,
                                                           full_initial_file_location=afl_file.full_initial_file_location,
                                                           initial_file__iexact=matching_name)

            afl_file.low_res_xml_file = matching_xml.initial_file
            afl_file.low_res_xml_file_location = matching_xml.initial_file_location

            if matching_xml.file_year:
                afl_file.file_year = matching_xml.file_year

            afl_file.status = ProgramFileLineItem.MATCHED_LOW_RES
            afl_file.high_res_scan_path = matching_xml.high_res_scan_path
            afl_file.save()
        except:
            success = False
            afl_file.status = ProgramFileLineItem.XML_GENERATION_REQUIRED
            afl_file.save()

    return success


def start_scan_for_ts_matching_files_high_res(program):
    from .models import ProgramFileLineItem

    all_files = ProgramFileLineItem.objects.filter(program=program.current_program_run, 
                                                   file_type=ProgramFileLineItem.TS_FILE_TYPE,
                                                   status=ProgramFileLineItem.MATCHED_LOW_RES)

    for afl_file in all_files:
        matching_name = afl_file.initial_file.lower()

        if '.evs' in matching_name:
            matching_name = matching_name.replace('.ts', '.xml')
        else:
            matching_name = matching_name.replace('.ts', '.evs.xml')

        try:
            matching_xml = ProgramFileLineItem.objects.get(program=program.current_program_run,
                                                           file_type=ProgramFileLineItem.XML_FILE_TYPE,
                                                           file_res_type=ProgramFileLineItem.HIGH_RES,
                                                           status=ProgramFileLineItem.MXF_MATCHED_HIGH_RES,
                                                           initial_file__iexact=matching_name,
                                                           file_year__isnull=False)

            afl_file.high_res_xml_file = matching_xml.initial_file
            afl_file.high_res_xml_file_location = matching_xml.initial_file_location
            afl_file.status = ProgramFileLineItem.MATCHED
            afl_file.high_res_scan_path = matching_xml.high_res_scan_path

            if matching_xml.file_year:
                afl_file.file_year = matching_xml.file_year

            afl_file.xml_files_moved_to = ''
            afl_file.save()
        except:
            afl_file.status = ProgramFileLineItem.XML_GENERATION_REQUIRED_HIGH_RES
            afl_file.save()

    return True


def check_or_create_folder(path, name):
    path_name = os.path.join(path, name)

    if not os.path.isdir(path_name):
        os.makedirs(path_name)

    return path_name


def move_found_xml_files(program):
    from .models import ProgramFileLineItem

    try:
        age_of_files = int(program.path_selection_for_program.age_of_files)
    except:
        age_of_files = 0

    program.set_status('file_move_started')

    all_files = ProgramFileLineItem.objects.filter(program=program.current_program_run,
                                                   file_type=ProgramFileLineItem.TS_FILE_TYPE,
                                                   status=ProgramFileLineItem.MATCHED)

    success = True
    for afl_file in all_files:

        #We set the age of files clean edit can access. If they're older then we push them to offline
        if not afl_file.file_year:
            file_year = 1
        else:
            try:
                file_year = int(afl_file.file_year)
            except:
                file_year = 1


        file_path_to_move_to = afl_file.program.program.path_selection_for_program.low_res_file_path_offline

        #Move files to this path:
        if file_year > age_of_files:
            #Do we include current year folders?
            if program.path_selection_for_program.move_current_year_files:
                #Yes
                file_path_to_move_to = afl_file.program.program.path_selection_for_program.low_res_file_path
            else:
                #No - skip current year folders
                continue

        #Location path of the high res files to copy
        location_of_high_res_file = afl_file.high_res_xml_file_location
        #Location of high res scan path
        high_res_scan_path = afl_file.high_res_scan_path

        #Replace the high res scan path with the new low res scan path
        new_matching_file_path = location_of_high_res_file.replace(high_res_scan_path, file_path_to_move_to)

        #Take out the file name
        new_matching_file_path = new_matching_file_path.replace(afl_file.high_res_xml_file, '')

        #Make the folder structure to the new Low Res file path
        mkdir_p(new_matching_file_path)
        copy_folder_location = new_matching_file_path

        if os.path.isfile(afl_file.low_res_xml_file_location) \
                and os.path.isfile(afl_file.initial_file_location):

            #Get file locations
            low_res_file_location = afl_file.low_res_xml_file_location
            ts_file_location = afl_file.initial_file_location

            #Check if the file exists, if not move it to the new low res location
            if not os.path.isfile(os.path.join(copy_folder_location, afl_file.low_res_xml_file)):
                try:
                    shutil.move(low_res_file_location, copy_folder_location)
                except:
                    afl_file.status = ProgramFileLineItem.ERROR_MOVING
                    afl_file.save()

            #Check if the file exists, if not move it to the new low res location
            if not os.path.isfile(os.path.join(copy_folder_location, afl_file.initial_file)):
                try:
                    shutil.move(ts_file_location, copy_folder_location)
                except:
                    afl_file.status = ProgramFileLineItem.ERROR_MOVING
                    afl_file.save()

            #Save the moved to location to verify if it has gone awry
            afl_file.xml_files_moved_to = copy_folder_location
            afl_file.status = ProgramFileLineItem.PROCESS_COMPLETE
            afl_file.save()

        else:
            success = False
            afl_file.status = ProgramFileLineItem.ERROR_MOVING
            afl_file.save()

    program.set_status('file_move_success')
    return success


def move_unlinked_ts_and_xml_files(program):
    from .models import ProgramFileLineItem

    try:
        age_of_files = int(program.path_selection_for_program.age_of_unlinked_files)
    except:
        raise EnvironmentError

    all_files = ProgramFileLineItem.objects.filter(program=program.current_program_run,
                                                   file_type=ProgramFileLineItem.TS_FILE_TYPE,
                                                   status=ProgramFileLineItem.XML_GENERATION_REQUIRED_HIGH_RES)

    success = True
    #Move unlinked files to this path if they're older than the age of files:
    file_path_to_move_to = os.path.join(program.path_selection_for_program.low_res_file_path_offline, 'Unlinked')
    mkdir_p(file_path_to_move_to)

    for afl_file in all_files:
        if os.path.isfile(afl_file.initial_file_location):
            # move_file = False
            # b = datetime.datetime.now()
            #
            # t = os.path.getmtime(afl_file.initial_file_location)
            # a = datetime.datetime.fromtimestamp(t)
            # total_days = (b-a).days
            # total_days = int(total_days)
            # if total_days >= age_of_files:
            #     move_file = True
            #
            # t = os.path.getctime(afl_file.initial_file_location)
            # a = datetime.datetime.fromtimestamp(t)
            # total_days = (b-a).days
            # total_days = int(total_days)
            # if total_days >= age_of_files:
            #     move_file = True

            move_file = True

            if move_file:
                # print 'Moving to unlinked folder'
                #move to unlinked folder
                if os.path.isfile(afl_file.low_res_xml_file_location) \
                    and os.path.isfile(afl_file.initial_file_location):

                    #Get file locations
                    low_res_file_location = afl_file.low_res_xml_file_location
                    ts_file_location = afl_file.initial_file_location

                    #Check if the file exists, if not move it to the new low res unlinked location
                    if not os.path.isfile(os.path.join(file_path_to_move_to, afl_file.low_res_xml_file)):
                        try:
                            shutil.move(low_res_file_location, file_path_to_move_to)
                        except:
                            afl_file.status = ProgramFileLineItem.ERROR_MOVING
                            afl_file.save()

                    #Check if the file exists, if not move it to the new low res unlinked location
                    if not os.path.isfile(os.path.join(file_path_to_move_to, afl_file.initial_file)):
                        try:
                            shutil.move(ts_file_location, file_path_to_move_to)
                        except:
                            afl_file.status = ProgramFileLineItem.ERROR_MOVING
                            afl_file.save()

                    #Save the moved to location to verify if it has gone awry
                    afl_file.xml_files_moved_to = file_path_to_move_to
                    afl_file.status = ProgramFileLineItem.UNLINKED_PROCESS_COMPLETE
                    afl_file.save()

    all_files = ProgramFileLineItem.objects.filter(program=program.current_program_run,
                                                   status__in=[ProgramFileLineItem.SEARCHING_LOW_RES,
                                                               ProgramFileLineItem.XML_GENERATION_REQUIRED,
                                                               ProgramFileLineItem.XML_GENERATION_REQUIRED_HIGH_RES],
                                                   full_initial_file_location=program.path_selection_for_program.initial_scan_path)

    success = True

    #Move unlinked files to this path if they're older than the age of files:
    file_path_to_move_to = os.path.join(program.path_selection_for_program.low_res_file_path_offline, 'Unlinked')
    mkdir_p(file_path_to_move_to)

    for afl_file in all_files:
        move_file = False
        if os.path.isfile(afl_file.initial_file_location):
            # print 'found file: ', afl_file.initial_file_location
            # b = datetime.datetime.now()
            #
            # t = os.path.getmtime(afl_file.initial_file_location)
            # a = datetime.datetime.fromtimestamp(t)
            # total_days = (b-a).days
            # total_days = int(total_days)
            #
            # if total_days >= age_of_files:
            #     move_file = True
            #
            # t = os.path.getctime(afl_file.initial_file_location)
            # a = datetime.datetime.fromtimestamp(t)
            # total_days = (b-a).days
            # total_days = int(total_days)
            #
            # if total_days >= age_of_files:
            #     move_file = True

            move_file = True

            if move_file:
                # print 'Moving to unlinked folder'
                #move to unlinked folder
                #Get file locations
                file_location = afl_file.initial_file_location

                #Check if the file exists, if not move it to the new low res unlinked location
                if not os.path.isfile(os.path.join(file_path_to_move_to, afl_file.initial_file)):
                    try:
                        shutil.move(file_location, file_path_to_move_to)
                    except:
                        afl_file.status = ProgramFileLineItem.ERROR_MOVING
                        afl_file.save()

                #Save the moved to location to verify if it has gone awry
                afl_file.xml_files_moved_to = file_path_to_move_to
                afl_file.status = ProgramFileLineItem.UNLINKED_PROCESS_COMPLETE
                afl_file.save()

    return success


def return_unmatched_xml_files_csv(program):
    program.completed = datetime.datetime.now()
    program.save()

    xml_path = program.path_selection_for_program.missing_xml_log_file_path

    if not os.path.isdir(program.path_selection_for_program.missing_xml_log_file_path):
        program.completed = None
        program.status = 'error'
        program.save()
    else:
        today_file = "unmatched_ts_xml_files_%s.csv" % datetime.date.today()

        if os.path.isfile(os.path.join(program.path_selection_for_program.missing_xml_log_file_path, today_file)):
            os.remove(os.path.join(program.path_selection_for_program.missing_xml_log_file_path, today_file))

        if str(xml_path)[-1:] == "/":
            file_name = '%s%s' % (xml_path, today_file)
        else:
            file_name = '%s/%s' % (xml_path, today_file)

        with open(file_name, 'w+') as csv_file:
            fieldnames = ['name', 'location', 'status']
            writer = csv.DictWriter(csv_file, fieldnames=fieldnames)

            writer.writeheader()
            all_files = program.get_unmatched_low_res_ts_files()

            for line in all_files:
                writer.writerow({'name': line.initial_file, 'location': line.initial_file_location, 'status':line.get_status_display()})

        today_file = "unmatched_mxf_xml_files_%s.csv" % datetime.date.today()
        if os.path.isfile(os.path.join(program.path_selection_for_program.missing_xml_log_file_path, today_file)):
            os.remove(os.path.join(program.path_selection_for_program.missing_xml_log_file_path, today_file))

        if str(xml_path)[-1:] == "/":
            file_name = '%s%s' % (xml_path, today_file)
        else:
            file_name = '%s/%s' % (xml_path, today_file)

        with open(file_name, 'w+') as csv_file:
            fieldnames = ['name', 'location', 'status']
            writer = csv.DictWriter(csv_file, fieldnames=fieldnames)

            writer.writeheader()

            all_files = program.get_unmatched_mxf_files()

            for line in all_files:
                writer.writerow({'name': line.initial_file, 'location': line.initial_file_location, 'status':line.get_status_display()})


        today_file = "completed_files_moved_%s.csv" % datetime.date.today()
        if os.path.isfile(os.path.join(program.path_selection_for_program.missing_xml_log_file_path, today_file)):
            os.remove(os.path.join(program.path_selection_for_program.missing_xml_log_file_path, today_file))

        if str(xml_path)[-1:] == "/":
            file_name = '%s%s' % (xml_path, today_file)
        else:
            file_name = '%s/%s' % (xml_path, today_file)

        with open(file_name, 'w+') as csv_file:
            fieldnames = ['name', 'location', 'final', 'status']
            writer = csv.DictWriter(csv_file, fieldnames=fieldnames)

            writer.writeheader()

            all_files = program.get_completed_files()

            for line in all_files:
                writer.writerow({'name': line.initial_file, 'location': line.initial_file_location,
                                 'final': line.xml_files_moved_to,
                                 'status':line.get_status_display()})


        today_file = "moving_error_files_moved_%s.csv" % datetime.date.today()
        if os.path.isfile(os.path.join(program.path_selection_for_program.missing_xml_log_file_path, today_file)):
            os.remove(os.path.join(program.path_selection_for_program.missing_xml_log_file_path, today_file))

        if str(xml_path)[-1:] == "/":
            file_name = '%s%s' % (xml_path, today_file)
        else:
            file_name = '%s/%s' % (xml_path, today_file)

        with open(file_name, 'w+') as csv_file:
            fieldnames = ['name', 'location', 'final', 'status']
            writer = csv.DictWriter(csv_file, fieldnames=fieldnames)

            writer.writeheader()

            all_files = program.get_error_files()

            for line in all_files:
                writer.writerow({'name': line.initial_file, 'location': line.initial_file_location,
                                 'final': line.xml_files_moved_to,
                                 'status':line.get_status_display()})




