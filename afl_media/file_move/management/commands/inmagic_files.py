import os, sys, csv, re, datetime
from random import randrange
from optparse import make_option
from pprint import pprint
from string import capwords
import xml.etree.ElementTree as ET
from django.core.management.base import BaseCommand, CommandError


import os
from catalogue_display.models import FilesInCatalogue, FileCatalogueLog, Directories
from django.conf import settings


class Command(BaseCommand):
    help = 'Runs a scheduled program for the AFL Low Res file move.'
    option_list = BaseCommand.option_list + (
        make_option(
            '--file_source',
            '-s',
            dest='file_source',
        ),
        make_option(
            '--commit',
            '-c',
            dest='commit',
        ),
    )

    def handle(self, *args, **options):
        file_source = options.get('file_source', None)
        commit = options.get('commit', False)

        if file_source:
            print 'file_source', file_source, file_source[:-4]

            all_files = FilesInCatalogue.objects.filter(name__icontains=file_source)
            for file in all_files:
                file_path = '%s%s/%s' % (settings.CATALOGUE_FILE_PATH, file.full_path_text, file.name)
                file_path = file_path.replace("//", "/")

                print file_path, 'get_tape_serial', get_tape_serial(file_path, 'file', 'tapeserial')




def get_tape_serial(file_path, block, chunk):

    try:
        tree = ET.parse(file_path)
        root = tree.getroot()
    except:
        return None

    for child in root.iter(block):
        text = child.attrib[chunk]
        print file_path, block, chunk, text

        return text



