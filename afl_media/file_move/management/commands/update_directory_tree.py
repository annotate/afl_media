import os, sys, csv, re, datetime
from random import randrange
from optparse import make_option
from pprint import pprint
from string import capwords
import xml.etree.ElementTree as ET
from django.core.management.base import BaseCommand, CommandError
from catalogue_display.models import FilesInCatalogue, FileCatalogueLog, Directories


class Command(BaseCommand):
    help = 'Runs a scheduled program for the AFL Low Res file move.'

    def handle(self, *args, **options):

        print 'UPDATING ALL DIRECTORIES TO PARENTAL UNITS'
        all_directories = Directories.objects.filter(folder_section=Directories.SDNA_FOLDER).order_by('-pk')

        i = 1

        for directory in all_directories:
            print directory.path

            parent_link = str(directory.folder_link).replace(directory.name, '')
            parent_link = parent_link[:len(parent_link)-1]

            print 'parent_link', parent_link

            path = parent_link.replace('/catalogue/display', '')

            print 'path', path

            crumbs = parent_link.split("/")
            crumb = crumbs[-1]

            print 'crumb ', crumb

            # directory = Directories.objects.filter(folder_link=parent_link, name=crumb, path=path, folder_section=Directories.SDNA_FOLDER)
            # print directory

            try:
                parent = Directories.objects.get(folder_link=parent_link, name=crumb, path=path, folder_section=Directories.SDNA_FOLDER)
                directory.parent = parent
                directory.save()
            except:
                pass

            # i = i + 1
            # if i == 2:
            #     return


        # all_other_files = FilesInCatalogue.objects.all()
        # for file in all_other_files:
        #     print file
        #     #Try to match .ts files and low files too.
        #     try:
        #         matching_name = file.initial_file.split('.')[0]
        #         matching_xml = FileCatalogueLog.objects.filter(name__contains=matching_name)
        #
        #         if matching_xml:
        #             matching_xml = matching_xml[0]
        #             file.matched_log_file = matching_xml
        #             file.save()
        #     except:
        #         pass

