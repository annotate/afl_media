import os, sys, csv, re, datetime
from random import randrange
from optparse import make_option
from pprint import pprint
from string import capwords
import xml.etree.ElementTree as ET
from django.core.management.base import BaseCommand, CommandError
from catalogue_display.models import FilesInCatalogue


def get_or_create_for_folder(directory, sub_directory, initial_path):
    return FilesInCatalogue.objects.get_or_create_for_folder(directory, sub_directory, initial_path)


def get_file_clip_id(file_path):

    try:
        tree = ET.parse(file_path)
        root = tree.getroot()
    except:
        return None

    clip_text_id = None
    for child in root.iter('clip'):
        try:
            clip_text_id = child.attrib['clipid']
        except:
            pass

    if not clip_text_id:
        for child in root.iter('filemobid'):
            try:
                clip_text_id = child.text
            except:
                pass

    return clip_text_id


def get_file_tape_id(file_path):

    try:
        tree = ET.parse(file_path)
        root = tree.getroot()
    except:
        return None

    tape_id = None
    for action in root.iter('actions'):
        for child in action:
            try:
                tape_id = child.attrib['tapeid']
            except:
                pass

    if not tape_id:
        for action in root.iter('files'):
            for child in action:
                try:
                    tape_id = child.attrib['tapeserial']
                except:
                    pass

    return tape_id


class Command(BaseCommand):
    help = 'Runs a scheduled program for the AFL Low Res file move.'

    def handle(self, *args, **options):
        all_files = FilesInCatalogue.objects.all()

        i = 0
        for file in all_files:
            file_name = os.path.join(file.full_path_text, file.name)
            i = i +1
            # print i, ' - ', file.pk, file_name

            if os.path.exists(file_name):

                clip_id = get_file_clip_id(file.full_path_text)
                tape_id = get_file_tape_id(file.full_path_text)

                if "/metadata/" in str(file.full_path_text):
                    file.folder_section = FilesInCatalogue.SDNA_FOLDER
                if "/1/" in str(file.full_path_text):
                    file.folder_section = FilesInCatalogue.AVID_PATH

                file.clip_id = clip_id
                file.tape_id = tape_id
                file.save()

                if file.folder_section == FilesInCatalogue.SDNA_FOLDER:
                    log = FilesInCatalogue.objects.filter(name__iexact=file.name, folder_section=FilesInCatalogue.EVS_FOLDER)
                    if not log and not file.status == FilesInCatalogue.ARCHIVED_FILE:
                        file.status = FilesInCatalogue.IGNORED_FILE

                        file.save()

                if file.folder_section == FilesInCatalogue.EVS_FOLDER:
                    log = FilesInCatalogue.objects.filter(name__iexact=file.name, folder_section=FilesInCatalogue.SDNA_FOLDER)
                    if not log and not file.status == FilesInCatalogue.ARCHIVED_FILE:
                        file.status = FilesInCatalogue.IGNORED_FILE
                        file.save()


                    # if i == 10:
                    #     break

