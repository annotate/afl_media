import os, sys, csv, re, datetime
from random import randrange
from optparse import make_option
from pprint import pprint
from string import capwords
import xml.etree.ElementTree as ET
from django.core.management.base import BaseCommand, CommandError


import os
from catalogue_display.models import FilesInCatalogue, Directories



class Command(BaseCommand):
    help = 'Runs a scheduled program for the AFL Low Res file move.'
    option_list = BaseCommand.option_list + (
        make_option(
            '--path',
            '-p',
            dest='path',
            help="If set, this will update the path to the db"
        ),
        make_option(
            '--commit',
            '-c',
            dest='commit',
        ),
        make_option(
            '--quiet',
            '-q',
            dest='quiet',
        ),
    )

    def handle(self, *args, **options):

        ignore_list = [".DS_Store", ".git", ".idea", None, "", '.gitignore', '.Trash-1001', '.sdna-mounted-dir']

        directories = Directories.objects.all()
        # print 'has children'
        for directory in directories:
            # print 'directory',  directory
            dir_path = directory.full_path_text
            # print 'dir_path', dir_path

            if os.path.isdir(dir_path):
                for fname in os.listdir(dir_path):
                    # print 'file ', fname
                    if os.path.isfile(os.path.join(dir_path, fname)) and fname not in ignore_list:
                        child_file, child_created = FilesInCatalogue.objects.get_or_create(directory=directory,
                                                                                        full_path_text=dir_path,
                                                                                        name=fname,
                                                                                        folder_section=directory.folder_section)

                        child_file.clip_id = get_file_clip_id(os.path.join(dir_path, fname))
                        child_file.tape_id = get_file_tape_id(os.path.join(dir_path, fname))

                        child_file.save()
                        if child_created:
                            child_file.status = FilesInCatalogue.NEW_FILE
                            child_file.save()

        print 'Completed file snapshot ', datetime.datetime.now()

def get_file_tape_id(file_path):

    try:
        tree = ET.parse(file_path)
        root = tree.getroot()
    except:
        return None

    tape_id = None
    for action in root.iter('actions'):
        for child in action:
            try:
                tape_id = child.attrib['tapeid']
            except:
                pass

    if not tape_id:
        for action in root.iter('files'):
            for child in action:
                try:
                    tape_id = child.attrib['tapeserial']
                except:
                    pass

    return tape_id

def get_file_clip_id(file_path):

    try:
        tree = ET.parse(file_path)
        root = tree.getroot()
    except:
        return None

    clip_text_id = None
    for child in root.iter('clip'):
        clip_text_id = child.attrib['clipid']
        return clip_text_id

    if not clip_text_id:
        pass



def get_folder_or_file_section(path):
    section = None
    if "/1/" in path:
        return FilesInCatalogue.AVID_PATH
    if "/metadata/" in path:
        return FilesInCatalogue.SDNA_FOLDER

    if not section:
        if "/1" in path:
            return FilesInCatalogue.AVID_PATH
        if "/metadata" in path:
            return FilesInCatalogue.SDNA_FOLDER

    return FilesInCatalogue.SDNA_FOLDER


def save_directory_chunk(parent, path, fname):
    ignore_list = [".DS_Store", ".git", ".idea", None, "", '.gitignore', '.Trash-1001', '.sdna-mounted-dir']

    if fname not in ignore_list:
        if os.path.isdir(path):
            Directories.objects.get_or_create(parent=parent,
                                            full_path_text=path,
                                            name=fname,
                                            folder_section=get_folder_or_file_section(path))

        if os.path.isfile(path) and fname not in ignore_list:
            child_file, child_created = FilesInCatalogue.objects.get_or_create(directory=parent,
                                                                          full_path_text=path,
                                                                          name=fname,
                                                                          folder_section=get_folder_or_file_section(path))
            if child_created:
                child_file.status = FilesInCatalogue.NEW_FILE
                child_file.save()

