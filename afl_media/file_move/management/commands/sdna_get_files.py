import os, sys, csv, re, datetime
from random import randrange
from optparse import make_option
from pprint import pprint
from string import capwords
import xml.etree.ElementTree as ET
from django.core.management.base import BaseCommand, CommandError
from itertools import izip

import os
from catalogue_display.models import FilesInCatalogue, Directories, FileCatalogueLog


def checkPath(path):

    print 'is directory:', os.path.isdir(path), path

    for dirname, dirnames, filenames in os.walk(path):
        for filename in filenames:
            print filename



class Command(BaseCommand):
    help = 'Runs a scheduled program for the AFL Low Res file move.'
    option_list = BaseCommand.option_list + (
        make_option(
            '--path',
            '-p',
            dest='path',
            help="If set, this will update the path to the db"
        ),
        make_option(
            '--commit',
            '-c',
            dest='commit',
        ),
        make_option(
            '--quiet',
            '-q',
            dest='quiet',
        ),
    )

    def handle(self, *args, **options):
        quiet = options.get('quiet', False)
        # if not quiet: print 'threading a pathway'

        ignore_list = [".DS_Store", ".git", ".idea", None, "", '.gitignore', '.Trash-1001', '.sdna-mounted-dir']

        paths = ['/home/user/MediaSort/file_mounts/sdna_to_inmagic/FILE_CATALOGUE_DATA/ARCHIVED_PROJECTS/1/']
        #         '/home/user/MediaSort/file_mounts/sdna_to_inmagic/FILE_CATALOGUE_DATA/BACKUP_EVEN_INTERPLAY/',
        #         '/home/user/MediaSort/file_mounts/sdna_to_inmagic/FILE_CATALOGUE_DATA/COMPILES/',
        #         '/home/user/MediaSort/file_mounts/sdna_to_inmagic/FILE_CATALOGUE_DATA/MEDIA_MANAGEMENT/',
        #         '/home/user/MediaSort/file_mounts/sdna_to_inmagic/FILE_CATALOGUE_DATA/ARCHIVED_PROJECTS/',
        #         '/home/user/MediaSort/file_mounts/sdna_to_inmagic/FILE_CATALOGUE_DATA/BACKUP_EVEN_INTERPLAY_1/',
        #         '/home/user/MediaSort/file_mounts/sdna_to_inmagic/FILE_CATALOGUE_DATA/COMPONENTS/',
        #         '/home/user/MediaSort/file_mounts/sdna_to_inmagic/FILE_CATALOGUE_DATA/MOTION_GRAPHICS/',
        #         '/home/user/MediaSort/file_mounts/sdna_to_inmagic/FILE_CATALOGUE_DATA/BACKUP_EVS/',
        #         '/home/user/MediaSort/file_mounts/sdna_to_inmagic/FILE_CATALOGUE_DATA/DATABASES_AND_EVS_LOGS/',
        #         '/home/user/MediaSort/file_mounts/sdna_to_inmagic/FILE_CATALOGUE_DATA/Orphan Clips/',
        #         '/home/user/MediaSort/file_mounts/sdna_to_inmagic/FILE_CATALOGUE_DATA/AUDIO_POST/',
        #         '/home/user/MediaSort/file_mounts/sdna_to_inmagic/FILE_CATALOGUE_DATA/BACKUP_INTERPLAY_EVEN/',
        #         '/home/user/MediaSort/file_mounts/sdna_to_inmagic/FILE_CATALOGUE_DATA/EVS_ARCHIVE/',
        #         '/home/user/MediaSort/file_mounts/sdna_to_inmagic/FILE_CATALOGUE_DATA/OUTPUT_MASTERS/',
        #         '/home/user/MediaSort/file_mounts/sdna_to_inmagic/FILE_CATALOGUE_DATA/AVID_PROJECTS/',
        #         '/home/user/MediaSort/file_mounts/sdna_to_inmagic/FILE_CATALOGUE_DATA/BACKUP_INTERPLAY_ODD/',
        #         '/home/user/MediaSort/file_mounts/sdna_to_inmagic/FILE_CATALOGUE_DATA/EVS_LOW_RES/',
        #         '/home/user/MediaSort/file_mounts/sdna_to_inmagic/FILE_CATALOGUE_DATA/PROJECTS/',
        #         '/home/user/MediaSort/file_mounts/sdna_to_inmagic/FILE_CATALOGUE_DATA/BACKUP_DELL/',
        #         '/home/user/MediaSort/file_mounts/sdna_to_inmagic/FILE_CATALOGUE_DATA/BACKUP_ODD_INTERPLAY/',
        #         '/home/user/MediaSort/file_mounts/sdna_to_inmagic/FILE_CATALOGUE_DATA/FOOTBALL_MATCHES/',
        #         '/home/user/MediaSort/file_mounts/sdna_to_inmagic/FILE_CATALOGUE_DATA/PROJECTS_ON_HOLD/']


        commit = options.get('commit', None)
        if commit:
            FilesInCatalogue.objects.all().delete()
            Directories.objects.all().delete()

        for section in paths:
            path_folder_name = section.split("/")[-2]
            parent, created = Directories.objects.filter(full_path_text=section,
                                                         name=path_folder_name,
                                                         folder_section=Directories.AVID_PATH)

            main_parent = parent
            print 'Beginning snapshot ', datetime.datetime.now(), 'path: ', section

            for path, dirs, files in os.walk(section):
                folder_name = path.split("/")[-1]
                print 'folder_name', folder_name

                if folder_name not in ignore_list:
                    parent_folder_name = path.split("/")[-2]

                    directory_parent = None
                    if parent_folder_name and path_folder_name not in ignore_list:

                        directory_parent = None
                        possible_parents = Directories.objects.filter(name=parent_folder_name, folder_section=get_folder_or_file_section(path))
                        for parent in possible_parents:

                            if section in parent.full_path_text:
                                directory_parent = parent
                                break

                    # print 'directory_parent', directory_parent
                    parent, created = Directories.objects.get_or_create(full_path_text=path,
                                                                       name=folder_name,
                                                                       parent=directory_parent,
                                                                       folder_section=get_folder_or_file_section(path))

            print 'getting files'
            directories = Directories.objects.filter(full_path_text__icontains=path_folder_name)
            # print 'has children'
            for directory in directories:
                # print 'directory',  directory
                dir_path = directory.full_path_text
                # print 'dir_path', dir_path

                if os.path.isdir(dir_path):
                    print 'is directory'
                    for fname in os.listdir(dir_path):
                        # print 'file ', fname
                        if os.path.isfile(os.path.join(dir_path, fname)) and fname not in ignore_list:
                            child_file, child_created = FilesInCatalogue.objects.get_or_create(directory=directory,
                                                                                            full_path_text=dir_path,
                                                                                            name=fname,
                                                                                            folder_section=directory.folder_section)

                            if get_file_clip_id(os.path.join(dir_path, fname)):
                                child_file.clip_id = get_file_clip_id(os.path.join(dir_path, fname))

                            elif get_file_clip_via_source(os.path.join(dir_path, fname)):
                                child_file.clip_id = get_file_clip_via_source(os.path.join(dir_path, fname))

                            child_file.tape_id = get_file_tape_id(os.path.join(dir_path, fname))

                            child_file.save()
                            if child_created:
                                child_file.status = FilesInCatalogue.NEW_FILE
                                child_file.save()

            print 'Completed snapshot ', datetime.datetime.now()


def get_file_tape_id(file_path):

    try:
        tree = ET.parse(file_path)
        root = tree.getroot()
    except:
        return None

    tape_id = None
    for action in root.iter('actions'):
        for child in action:
            try:
                tape_id = child.attrib['tapeid']
            except:
                pass

    if not tape_id:
        for action in root.iter('files'):
            for child in action:
                try:
                    tape_id = child.attrib['tapeserial']
                except:
                    pass

    return tape_id

def get_file_clip_id(file_path):

    try:
        tree = ET.parse(file_path)
        root = tree.getroot()
    except:
        return None

    clip_text_id = None
    for child in root.iter('clip'):
        clip_text_id = child.attrib['clipid']
        return clip_text_id

    if not clip_text_id:
        pass


def get_file_clip_via_source(file_path):
        try:
            tree = ET.parse(file_path)
            root = tree.getroot()
        except:
            return None

        clip_text_id = None
        for child in root:
            if child.tag == "sourcemobid":
                if child.text:
                    clip_text_id = child.text

        if clip_text_id:
            return clip_text_id


def get_folder_or_file_section(path):
    section = None
    if "/1/" in path:
        return FilesInCatalogue.AVID_PATH
    if "/metadata/" in path:
        return FilesInCatalogue.SDNA_FOLDER

    if not section:
        if "/1" in path:
            return FilesInCatalogue.AVID_PATH
        if "/metadata" in path:
            return FilesInCatalogue.SDNA_FOLDER

    return FilesInCatalogue.SDNA_FOLDER


def save_directory_chunk(parent, path, fname):
    ignore_list = [".DS_Store", ".git", ".idea", None, "", '.gitignore', '.Trash-1001', '.sdna-mounted-dir']

    if fname not in ignore_list:
        if os.path.isdir(path):
            Directories.objects.get_or_create(parent=parent,
                                            full_path_text=path,
                                            name=fname,
                                            folder_section=get_folder_or_file_section(path))

        if os.path.isfile(path) and fname not in ignore_list:
            child_file, child_created = FilesInCatalogue.objects.get_or_create(directory=parent,
                                                                          full_path_text=path,
                                                                          name=fname,
                                                                          folder_section=get_folder_or_file_section(path))
            if child_created:
                child_file.status = FilesInCatalogue.NEW_FILE
                child_file.save()

