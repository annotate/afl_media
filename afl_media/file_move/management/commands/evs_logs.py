import os, sys, csv, re, datetime
from random import randrange
from optparse import make_option
from pprint import pprint
from string import capwords
import xml.etree.ElementTree as ET
from django.core.management.base import BaseCommand, CommandError
import shutil

import os
from catalogue_display.models import FilesInCatalogue, FileCatalogueLog, Directories
from django.conf import settings


def get_file_tape_id(file_path):

    try:
        tree = ET.parse(file_path)
        root = tree.getroot()
    except:
        return None

    tape_id = None
    for action in root.iter('actions'):
        for child in action:
            try:
                tape_id = child.attrib['tapeid']
            except:
                pass

    if not tape_id:
        for action in root.iter('files'):
            for child in action:
                try:
                    tape_id = child.attrib['tapeserial']
                except:
                    pass

    return tape_id



def get_or_create_for_folder(directory, sub_directory, initial_path):
    return FilesInCatalogue.objects.get_or_create_for_folder(directory, sub_directory, initial_path)


def get_file_clip_id(file_path):

    try:
        tree = ET.parse(file_path)
        root = tree.getroot()
    except:
        return None

    clip_text_id = None
    for child in root.iter('clip'):
        clip_text_id = child.attrib['clipid']
        return clip_text_id

    if not clip_text_id:
        pass


class Command(BaseCommand):
    help = 'Runs a scheduled program for the AFL Low Res file move.'
    option_list = BaseCommand.option_list + (
        make_option(
            '--commit',
            '-c',
            dest='commit',
        ),
        make_option(
            '--evs_commit',
            '-b',
            dest='evs_commit',
        ),
    )
    def handle(self, *args, **options):
        commit = options.get('commit', None)
        evs_commit = options.get('evs_commit', None)

        if settings.EVS_LOG_WALK:
            print 'DIRECTORY - Beginning initial EVS log walk'

            parent_directory, created = Directories.objects.get_or_create(folder_section=Directories.EVS_LOG_PATH,
                                                                        parent__isnull=True,
                                                                        name="New")

            print 'parent_directory', parent_directory, parent_directory.pk
            for fn in os.listdir(settings.EVS_LOG_WALK):
                print fn
                if fn not in FilesInCatalogue.ignore_files and os.path.isfile(os.path.join(settings.EVS_LOG_WALK, fn)):
                    clip_id = get_file_clip_id(os.path.join(settings.EVS_LOG_WALK, fn))
                    tape_id = get_file_tape_id(os.path.join(settings.EVS_LOG_WALK, fn))
                    file, created = FilesInCatalogue.objects.get_or_create(name=fn,
                                                                        directory=parent_directory,
                                                                        full_path_text=settings.EVS_LOG_WALK,
                                                                        clip_id=clip_id,
                                                                        tape_id=tape_id,
                                                                        folder_section=FilesInCatalogue.EVS_FOLDER)
                    if created:
                        file.status = FilesInCatalogue.NEW_FILE
                        file.save()


        if commit:
            print 'UPDATING ALL FILES FOR EVS CHECK'
            all_files = FilesInCatalogue.objects.filter(folder_section=FilesInCatalogue.EVS_FOLDER).exclude(status=FilesInCatalogue.ARCHIVED_FILE)
            for file in all_files:
                print file.name
                if file.name not in FilesInCatalogue.ignore_files and os.path.isfile(os.path.join(settings.EVS_LOG_WALK, file.name)):
                    files = FilesInCatalogue.objects.filter(name__iexact=file.name).exclude(pk=file.pk)
                    if not files:
                        file.status = FilesInCatalogue.IGNORED_FILE
                    else:
                        file.status = FilesInCatalogue.NEW_FILE
                    file.save()




