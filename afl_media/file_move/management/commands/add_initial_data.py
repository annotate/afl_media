from django.core.management.base import BaseCommand, CommandError
from optparse import make_option
from catalogue_display.models import Players, HighlightSyntax, NameCorrections

import csv


def process_string(s):
    if s:
        try:
            return unicode(s)
        except:
            return None
    else:
        return None


class Command(BaseCommand):
    help = 'This will add some of the initial data'

    option_list = BaseCommand.option_list + (
            make_option(
                '--csv',
                '-i',
                action="store",
                dest='csv',
                type='string',
                help="A CSV file to read from"
            ),
            make_option(
                '--type',
                '-t',
                action="store",
                dest='type',
                type='string',
                help="The type of data being imported"
            ),
            make_option(
                '--commit',
                '-c',
                action="store_true",
                dest='commit',
                default=False,
                help="If set, the changes in this script will be committed to the database."
            ),
        )

    def handle(self, *args, **options):
        commit = options.get('commit', False)
        csv_filename = options.get('csv', None)
        type = options.get('type', None)

        if not csv_filename:
            print 'Please specify a CSV file with the --csv argument'
            return

        if not type:
            print 'Please specify a type file with the --type argument'
            return

        csv_file = open(csv_filename, 'rU')
        csv_reader = csv.reader(csv_file)

        print 'type selected', type

        if commit:

            print '--------------------------------------------'

            if type == "player":
                Players.objects.all().delete()

                for index, row in enumerate(csv_reader):
                    print index
                    print row
                    print ''

                    # Ignore the header row
                    if index == 0:
                        continue

                    (
                        number, initial, team, last_name, first_name
                    ) = row


                    # ==================================
                    # Pre-process all the inputs (making sure empty strings become None, etc)
                    # ==================================
                    number = process_string(number)
                    initial = process_string(initial)
                    last_name = process_string(last_name)
                    first_name = process_string(first_name)
                    team = process_string(team)



                    # ==================================
                    # Process Television
                    # ==================================

                    player = Players.objects.create(number=number, team=team, initial=initial, last_name=last_name,
                                                    first_name=first_name)

                    print 'saved player ', player.last_name

                print 'Import of Players Complete'

            if type == "light":
                HighlightSyntax.objects.all().delete()

                for index, row in enumerate(csv_reader):
                    print index
                    print row
                    print ''

                    # Ignore the header row
                    if index == 0:
                        continue

                    (
                        highlight, syntax
                    ) = row


                    # ==================================
                    # Pre-process all the inputs (making sure empty strings become None, etc)
                    # ==================================
                    highlight = process_string(highlight)
                    syntax = process_string(syntax)




                    # ==================================
                    # Process Television
                    # ==================================

                    player = HighlightSyntax.objects.create(highlight=highlight, syntax=syntax)

                    print 'saved syntax ', player.syntax

                print 'Import of Highlights Complete'


            if type == "names":
                NameCorrections.objects.all().delete()

                for index, row in enumerate(csv_reader):
                    print index
                    print row
                    print ''

                    # Ignore the header row
                    if index == 0:
                        continue

                    (
                        evs_name, corrected_name
                    ) = row


                    # ==================================
                    # Pre-process all the inputs (making sure empty strings become None, etc)
                    # ==================================
                    evs_name = process_string(evs_name)
                    corrected_name = process_string(corrected_name)




                    # ==================================
                    # Process Television
                    # ==================================

                    player = NameCorrections.objects.create(evs_name=evs_name, corrected_name=corrected_name)

                    print 'saved corrected_name ', player.corrected_name

                print 'Import of corrected_name Complete'