import os, sys, csv, re, datetime
from random import randrange
from optparse import make_option
from pprint import pprint
from string import capwords
from django.core.management.base import BaseCommand, CommandError

from file_move.models import *


class Command(BaseCommand):
    help = 'Runs a scheduled program for the AFL Low Res file move.'

    option_list = BaseCommand.option_list + (
        make_option(
            '--program_id',
            '-p',
            dest='program_id',
            help="If set, this will find a program run based on this ID."
        ),
        make_option(
            '--run',
            '-r',
            dest='run',
            action="store_true",
            default=False,
            help="If set, this will create and run a new program based on production status details in the database."
        ),
        make_option(
            '--commit',
            '-c',
            action="store_true",
            dest='commit',
            default=False,
            help="If set, the changes in this script will be committed to the database."
        ),
    )

    def handle(self, *args, **options):
        program_id = options.get('program_id', None)
        run = options.get('run', None)
        commit = options.get('commit', False)

        if program_id:
            print 'Selected program ID to run: ', program_id
            program = FileMoveProgram.objects.get(pk=program_id)
            if commit:
                print "Running program: %s at %s" % (program, datetime.datetime.now())
                program.start_program()
                print "Successfully completed program: %s at %s" % (program, datetime.datetime.now())
            else:
                print "Would have run this program: %s at %s" % (program, datetime.datetime.now())

            print 'Exit'

        if run:
            print 'Beginning file scheduled task'

            file_path_details = FilePathDetails.objects.filter(status=FilePathDetails.PRODUCTION_STATUS)
            if file_path_details:
                file_path_details = file_path_details[0]
                print 'Selected path details: ', file_path_details

                file_move_program = FileMoveProgram(path_selection_for_program=file_path_details, started=datetime.datetime.now(),
                                                    scheduled_run=True, initial_file_path_at_run=file_path_details.initial_scan_path,
                                                    low_res_file_path_at_run=file_path_details.low_res_file_path,
                                                    high_res_file_path_at_run=file_path_details.high_res_file_path,
                                                    second_high_res_file_path_at_run=file_path_details.second_high_res_file_path,
                                                    status=FileMoveProgram.STARTED)

                file_move_program.save()
                print 'Created new program: ', file_move_program

                print 'Beginning file process'
                file_move_program.start_program()
                print 'File processing complete. Status: ', file_move_program.get_status_display()

            else:
                print 'There are no file path details available for scheduled tasks'
                return False


            print 'Exit'
