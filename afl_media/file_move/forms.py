from django import forms
from .models import FileMoveProgram


class FileMoveProgramAddForm(forms.ModelForm):

    class Meta:
        model = FileMoveProgram
        fields = ('path_selection_for_program', 'started_by', 'status', 'started', 'completed', 'failed',
                  'files_checked', 'manually_created', 'path_selection_for_program')

    def save(self, *args, **kwargs):
        file_move_program = super(FileMoveProgramAddForm, self).save(*args, **kwargs)

        file_move_program.manually_created = True
        file_move_program.save()

        return file_move_program
