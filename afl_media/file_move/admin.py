from django.contrib import admin
from django.contrib import messages
from django.shortcuts import redirect
import datetime

from .models import SelectableYears, FileMoveProgram, FilePathDetails, ProgramDocumentation, \
                    FileProgramRun, ProgramFileLineItem
from .forms import FileMoveProgramAddForm
from workflow_utils.models import WorkflowLogEntry


from django.contrib.auth.models import Group
from django.contrib.sites.models import Site

admin.site.unregister(Group)
admin.site.unregister(Site)


class ProgramDocumentationAdmin(admin.ModelAdmin):
    model = ProgramDocumentation
    raw_id_fields = ('modified_by',)


class SelectableYearsAdmin(admin.ModelAdmin):
    model = SelectableYears
    raw_id_fields = ('modified_by',)


class FileProgramRunAdmin(admin.StackedInline):
    model = FileProgramRun
    extra = 0

    def get_queryset(self, request):
        qs = super(FileProgramRunAdmin, self).get_queryset(request)

        return qs.order_by('-id')

    def has_delete_permission(self, request, obj=None):
        return False


class FileMoveProgramAdmin(admin.ModelAdmin):
    model = FileMoveProgram
    form = FileMoveProgramAddForm

    list_display = ('id', 'created', 'started_by', 'status', 'program_log_details')
    raw_id_fields = ('started_by',)
    readonly_fields = ('manually_created', 'scheduled_run')

    actions = ('blank_actions', 'run_selected_program_now', 'move_found_xml_files', 'blank_action_two', 'create_csv_files',)

    inlines = (FileProgramRunAdmin,)

    def blank_actions(modeladmin, request, queryset):
        pass

    blank_actions.short_description = "           "

    def blank_action_two(modeladmin, request, queryset):
        pass

    blank_action_two.short_description = "           "

    def run_selected_program_now(modeladmin, request, queryset):
        for item in queryset:
            new_run = FileProgramRun.objects.create(program=item, date_run=datetime.datetime.now())
            item.current_program_run = new_run
            item.save()

            return redirect('start-admin-program-log', item.current_program_run.pk)

    run_selected_program_now.short_description = "Run Step 1: Scan All Files."

    def move_unlinked_ts_and_xml_files(modeladmin, request, queryset):
        for item in queryset:
            item.move_unlinked_ts_and_xml_files()

        messages.success(request, "Checked and moved unlinked files successfully.")
    move_unlinked_ts_and_xml_files.short_description = "Move unlinked ts and xml files"

    def create_csv_files(self, request, queryset):
        for item in queryset:
            item.return_unmatched_xml_files_csv()
            messages.info(request, 'The program step has completed running.')
            break

    create_csv_files.short_description = "Step 1.1 - Recreate missing XMl csv files"

    def move_found_xml_files(self, request, queryset):
        for item in queryset:
            item.move_found_xml_files()
            item.move_unlinked_ts_and_xml_files()
            messages.info(request, 'The program step has completed running.')
            break

    move_found_xml_files.short_description = "Run Step 2: Move All Files."



class FilePathDetailsAdmin(admin.ModelAdmin):
    model = FilePathDetails

    list_display = ('id', 'created', 'low_res_file_path', 'age_of_files', 'status')
    search_fields = ('low_res_file_path', 'high_res_file_path', 'second_high_res_file_path', 'age_of_files',)
    list_filter = ('age_of_files',)

    raw_id_fields = ('modified_by',)


class ProgramFileLineItemAdmin(admin.ModelAdmin):
    model = ProgramFileLineItem
    list_display = ('id', 'program', 'initial_file', 'file_type', 'file_res_type', 'file_year', 'status')
    list_filter = ('file_type', 'file_year', 'program', 'status', 'file_res_type')
    search_fields = ('id', 'initial_file', )

    actions = ('update_status',)

    def update_status(self, request, queryset):

        all_files = queryset.filter(file_type=ProgramFileLineItem.MXF_FILE_TYPE).exclude(status=ProgramFileLineItem.MATCHED)
        for line in all_files:
            line.status = ProgramFileLineItem.MATCHED
            line.save()

    update_status.short_description = "Check Status"


admin.site.register(FileMoveProgram, FileMoveProgramAdmin)
admin.site.register(SelectableYears, SelectableYearsAdmin)
admin.site.register(FilePathDetails, FilePathDetailsAdmin)
admin.site.register(ProgramDocumentation, ProgramDocumentationAdmin)
admin.site.register(ProgramFileLineItem, ProgramFileLineItemAdmin)


