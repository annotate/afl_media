from __future__ import absolute_import

import time
from celery import Celery
from celery.contrib.methods import task_method

from django.conf import settings
from .settings.utils import add_settings_to_environ


add_settings_to_environ()


class NicerCelery(Celery):
    @property
    def method_task(self):
        return self.task(filter=task_method)


app = NicerCelery('afl_media')


# Using a string here means the worker will not have to
# pickle the object when using Windows.
app.config_from_object('django.conf:settings')
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)


@app.task(bind=True)
def debug_task(self):
    print 'Doing a thing!'
    time.sleep(5)
    print 'Thing complete!'
