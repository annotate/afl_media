from django.conf.urls import patterns, include, url
from django.contrib import admin

from django.contrib.auth.forms import AuthenticationForm
from dajaxice.core import dajaxice_autodiscover, dajaxice_config

dajaxice_autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'afl_media.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(dajaxice_config.dajaxice_url, include('dajaxice.urls')),

    url(r'^$', 'file_move.views.home_redirect', name='home-index'),
    url(r'^grappelli/', include('grappelli.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^login/', include(admin.site.urls)),
    url(r'^file/move/', include('file_move.urls')),
    url(r'^catalogue/', include('catalogue_display.urls')),

    url(r'^get_root_structure/$', 'catalogue_display.ajax.get_root_structure', name='get_root_structure'),
    url(r'^get_evs_tree_structure/$', 'catalogue_display.ajax.get_evs_tree_structure', name='get_evs_tree_structure'),

    url(r'^get_node_children/(?P<directory_id>\d+)/$', 'catalogue_display.ajax.get_node_children', name='get_node_children'),
    url(r'^get_node_children_structure/(?P<directory_id>\d+)/$', 'catalogue_display.ajax.get_node_children_structure', name='get_node_children_structure'),

    url(r'^get_directory_files/(?P<directory_id>\d+)/$', 'catalogue_display.ajax.get_directory_files', name='get_directory_files'),

    url(r'^save_evs_log/(?P<file_id>\d+)/$', 'catalogue_display.ajax.save_evs_log', name='save_evs_log'),


    url(r'^add_folder_to_bucket/$', 'catalogue_display.ajax.add_folder_to_bucket', name='add_folder_to_bucket'),
    url(r'^add_project_to_bucket/$', 'catalogue_display.ajax.add_project_to_bucket', name='add_project_to_bucket'),

    url(r'^add_files_to_bucket/$', 'catalogue_display.ajax.add_files_to_bucket', name='add_files_to_bucket'),
    url(r'^remove_folder_from_bucket/(?P<folder_id>\d+)/$', 'catalogue_display.ajax.remove_folder_from_bucket', name='remove_folder_from_bucket'),
    url(r'^remove_project_from_bucket/(?P<folder_id>\d+)/$', 'catalogue_display.ajax.remove_project_from_bucket', name='remove_project_from_bucket'),
    url(r'^remove_file_from_bucket/(?P<file_id>\d+)/$', 'catalogue_display.ajax.remove_file_from_bucket', name='remove_file_from_bucket'),
    url(r'^remove_all_from_bucket/$', 'catalogue_display.ajax.remove_all_from_bucket', name='remove_all_from_bucket'),

    url(r'^logout/$', 'django.contrib.auth.views.logout', {'next_page': '/login/'}, name="logout"),

)
