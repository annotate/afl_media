"""
This file was generated with the customdashboard management command and
contains the class for the main dashboard.

To activate your index dashboard add the following to your settings.py::
    GRAPPELLI_INDEX_DASHBOARD = 'afl_media.dashboard.CustomIndexDashboard'
"""

from django.utils.translation import ugettext_lazy as _
from django.core.urlresolvers import reverse

from grappelli.dashboard import modules, Dashboard
from grappelli.dashboard.utils import get_admin_site_name


class CustomIndexDashboard(Dashboard):
    """
    Custom index dashboard for www.
    """
    
    def init_with_context(self, context):
        site_name = get_admin_site_name(context)

        # append a group for "Administration" & "Applications"

        # append an app list module for "Applications"
        self.children.append(modules.ModelList(
            title='Administration',
            column=1,
            models=('django.contrib.*',),
        ))


        # append an app list module for "Applications"
        self.children.append(modules.ModelList(
            title='File Move Program',
            column=1,
            models=('file_move.models.*',),
            exclude=('file_move.models.ProgramDocumentation', 'file_move.models.SelectableYears', 'file_move.models.ProgramFileLineItem')
        ))

        # append an app list module for "Applications"
        self.children.append(modules.ModelList(
            title='SDNA File Catalogue',
            column=1,
            models=('catalogue_display.models.*', 'mptt.models.*'),
            exclude=('catalogue_display.models.HighlightSyntax', 'catalogue_display.models.Players', 'catalogue_display.models.NameCorrections',
                    'catalogue_display.models.FileCatalogueSet')
        ))

        # append an app list module for "Applications"
        self.children.append(modules.ModelList(
            title='DMP file Setup',
            column=1,
            models=('catalogue_display.models.Players', 'catalogue_display.models.NameCorrections',
                    'catalogue_display.models.EVSUmpires', 'catalogue_display.models.EVSWeather', 'catalogue_display.models.EVSCommentators',
                    'catalogue_display.models.HighlightSyntax'),
        ))

        self.children.append(modules.LinkList(
            title='Links',
            column=2,
            children=(
                {
                    'title': 'SDNA to InMagic',
                    'url': '/catalogue/display/',
                    'external': True,
                },

            )
        ))
        # # append a recent actions module
        # self.children.append(modules.RecentActions(
        #     _('Recent Actions'),
        #     limit=5,
        #     collapsible=False,
        #     column=3,
        # ))



