DEFAULT_SETTINGS = 'afl_media.settings.local'
# DEFAULT_SETTINGS = 'afl_media.settings.linux'

SETTINGS_MAPPING = {
    'local': 'afl_media.settings.local',
    'linux': 'afl_media.settings.linux',

}
