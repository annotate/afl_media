import sys
from .base import *

TEMPLATE_DEBUG = DEBUG

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'afl_media',                      # Or path to database file if using sqlite3.
        'USER': 'root',                      # Not used with sqlite3.
        'PASSWORD': '1234',                  # Not used with sqlite3.
        'HOST': '',                      # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '',                      # Set to empty string for default. Not used with sqlite3.
    }
}

BASE_EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

INSTALLED_APPS += ()

INTERNAL_IPS = ('127.0.0.1',)

MIDDLEWARE_CLASSES += ()

SECRET_KEY = '1234567890'

CELERY_RESULT_BACKEND = 'redis://localhost:6379/0'
CELERY_TASK_SERIALIZER = 'pickle'
CELERY_ACCEPT_CONTENT = ['pickle',]
logging = 'DEBUG'

BROKER_URL = 'redis://localhost:6379/0'

if 'test' in sys.argv:
    BROKER_BACKEND = 'memory'
    BROKER_URL = None
    CELERY_RESULT_BACKEND = 'cache'
    CELERY_CACHE_BACKEND = 'memory'
    #BROKER_URL = 'memory'


# #Catalogue display details
# EVS_LOG_WALK = '/Users/martajaniak/Sites/django/annotate/afl_media/documents/SDNA/EVS LOGS/NEW/'
# EVS_LOG_WALK_DONE = '/Users/martajaniak/Sites/django/annotate/afl_media/documents/SDNA/EVS LOGS/DONE/'

THREADED_TREE_IGNORE_LIST =   [".DS_Store", ".git", ".idea", None, "", '.gitignore', '.Trash-1001', '.sdna-mounted-dir',
                               'BACKUP_DELL', 'BACKUP_INTERPLAY_EVEN', 'MEDIA_MANAGEMENT',
                               'BACKUP_INTERPLAY_ODD', 'MOTION_GRAPHICS', 'ARCHIVED_PROJECTS.7z',
                               'BACKUP_ODD_INTERPLAY', 'Orphan Clips', 'AUDIO_POST', 'BACKUP_DELL',
                               'DATABASES_AND_EVS_LOGS', 'BACKUP_EVEN_INTERPLAY',
                               'BACKUP_EVEN_INTERPLAY_1', 'EVS_LOW_RES', 'BACKUP_EVS']

EVS_LOG_WALK = '/home/user/MediaSort/file_mounts/sdna_to_inmagic/EVS_LOGS/NEW/'
EVS_LOG_WALK_DONE = '/home/user/MediaSort/file_mounts/sdna_to_inmagic/InMagic_DMP_FILES/DONE/'